<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content" class="">
        <div class="default-padding">
            <form id="" method="post" accept-charset="utf-8">

                <!-- EDIT FILTERS GROUP -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">FILTERS GROUP</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="item-holder">
                                <div class="span4 item-name">Group Name:</div>
                                <div class="span14 child">
                                    <span>Spam Capture</span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span4 item-name">Priority:</div>
                                <div class="span3 child">
                                    <input type="text" value="0" />
                                </div>
                                <div class="span3 text-center">(0 is highest priority)</div>
                                <!--<div class="span2"><?php echo get_input_button("default-btn save full-width", "append", "Append"); ?></div>-->
                                <div class="clearfix"></div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <!-- Filters table -->
                 <div class="content-holder">
                    <div class="table-container">
                        <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                            <table id="ticketlist" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>FILTERS</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <input type="checkbox" /> 10

                                        </td>
                                        <td>
                                            <strong>If:</strong>
                                            <ul>
                                                <li>
                                                    "from" <b>contains</b> "thevarguy"
                                                    &nbsp;<strong>or</strong>
                                                </li>
                                                <li>
                                                    "from" <b>contains</b> "seo"
                                                </li>
                                            </ul>
                                            <strong>Then set:</strong>
                                            <ul>
                                                <li>Assigned to user: "spamCollector"</li>
                                                <li>Priority to: "Junk"</li>
                                                <li>Mark as spam</li>
                                                <li>Stop filters processing for incoming mail after this filter</li>
                                                <li>Automatically close ticket</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "filters-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <input type="checkbox" /> 11
                                        </td>
                                        <td>
                                            <strong>If:</strong>
                                            <ul>
                                                <li>
                                                    "subject" <b>contains</b> "@deleteuser:"
                                                </li>
                                            </ul>
                                            <strong>Then set:</strong>
                                            <ul>
                                                <li>Assigned to user: "timets"</li>
                                                <li>Priority to: "Normal"</li>
                                                <li>Enquiry Type to: "Internal: User deletion"</li>
                                                <li>Response target to: "0 days 12:00:00"</li>
                                                <li>Stop filters processing for incoming mail after this filter</li>
                                                <li>Disable AutoReply</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "filters-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" /> 29
                                        </td>
                                        <td>
                                            <strong>If:</strong>
                                            <ul>
                                                <li>"cc" <b>contains</b> "support@logicalware.com"</li>
                                            </ul>
                                            <strong>Then set:</strong>
                                            <ul>
                                                <li>Assigned to queue: "Support"</li>
                                                <li>Disable AutoReply</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "filters-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <input type="checkbox" /> 30
                                        </td>
                                        <td>
                                            <strong>If:</strong>
                                            <ul>
                                                <li>"subject" <b>contains</b> "DO NOT REPLY"</li>
                                            </ul>
                                            <strong>Then set:</strong>
                                            <ul>
                                                <li>Assigned to queue: "Support"</li>
                                                <li>Disable AutoReply</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "filters-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </form>
        </div>

    </div>
    <?php
    $btn_array = array();

    array_push($btn_array, get_input_button("footer-btn save", "save", "Save"));

    echo get_footer($btn_array);
    ?>
</div>
<?php include('views/footer.php'); ?>