<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">FILTERS</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="item-holder">
                                <div class="span4 item-name">
                                    <span>Filter list by group:</span>
                                </div>
                                <div class="span4">
                                    <select name="group_id" id="group_id">
                                        <option value="0">Not assigned</option>
                                        <option value="-1" selected="selected">All</option>
                                        <option value="8">Spam Capture</option>
                                        <option value="9">System</option>
                                    </select>
                                </div>
                                <div class="span2">
                                    <?php echo get_input_button("default-btn full-width", "filter", "Apply Filter") ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="content-holder">
                    <div class="table-container">
                        <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                            <table id="ticketlist" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>FILTER</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>10</td>
                                        <td>
                                            <strong>If:</strong>
                                            <ul>
                                                <li>
                                                    "from" <b>contains</b> "thevarguy"
                                                    &nbsp;<strong>or</strong>
                                                </li>
                                                <li>
                                                    "from" <b>contains</b> "seo"
                                                </li>
                                            </ul>
                                            <strong>Then set:</strong>
                                            <ul>
                                                <li>Assigned to user: "spamCollector"</li>
                                                <li>Priority to: "Junk"</li>
                                                <li>Mark as spam</li>
                                                <li>Stop filters processing for incoming mail after this filter</li>
                                                <li>Automatically close ticket</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "filters-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            11
                                        </td>
                                        <td>
                                            <strong>If:</strong>
                                            <ul>
                                                <li>
                                                    "subject" <b>contains</b> "@deleteuser:"
                                                </li>
                                            </ul>
                                            <strong>Then set:</strong>
                                            <ul>
                                                <li>Assigned to user: "timets"</li>
                                                <li>Priority to: "Normal"</li>
                                                <li>Enquiry Type to: "Internal: User deletion"</li>
                                                <li>Response target to: "0 days 12:00:00"</li>
                                                <li>Stop filters processing for incoming mail after this filter</li>
                                                <li>Disable AutoReply</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "filters-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            29
                                        </td>
                                        <td>
                                            <strong>If:</strong>
                                            <ul>
                                                <li>"cc" <b>contains</b> "support@logicalware.com"</li>
                                            </ul>
                                            <strong>Then set:</strong>
                                            <ul>
                                                <li>Assigned to queue: "Support"</li>
                                                <li>Disable AutoReply</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "filters-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            30
                                        </td>
                                        <td>
                                            <strong>If:</strong>
                                            <ul>
                                                <li>"subject" <b>contains</b> "DO NOT REPLY"</li>
                                            </ul>
                                            <strong>Then set:</strong>
                                            <ul>
                                                <li>Assigned to queue: "Support"</li>
                                                <li>Disable AutoReply</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "filters-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_button("footer-btn save", "save", "Add New Filter", "filters-add_form"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>