<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <!-- Accounts -->
                <!-- 
                    //TODO: remove empty div 
                -->
                <div class="content-holder">
                    <div class="">
                        <div class="layout-helper">
                            <div class="span4 settings-title">                                <!--<i class="fa-user fa margin-right"></i>-->
                                Accounts
                            </div>
                            <div class="span20">Set up accounts so your helpdesk can receive emails</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="content-holder">
                    <div class="table-container">
                        <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                            <table id="ticketlist" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ACCOUNT</th>
                                        <th>ASSSIGNED</th>
                                        <th>PRIORITY</th>
                                        <th>ACTION</th>
                                        <th>STATUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            billing@logicalware.com
                                        </td>
                                        <td nowrap="nowrap">
                                            Billing &amp; Invoicing (Queue)
                                        </td>
                                        <td>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "accounts-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                        <td>
                                            <span>OK</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            2
                                        </td>
                                        <td>
                                            enquiries@logicalware.com
                                        </td>
                                        <td nowrap="nowrap">
                                            Sales &amp; Enquiries (Queue)
                                        </td>
                                        <td>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "accounts-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                        <td>
                                            <span>OK</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            3
                                        </td>
                                        <td>
                                            lwtest@my.logicalware.net
                                        </td>
                                        <td nowrap="nowrap">
                                            logicalware  (User)
                                        </td>
                                        <td>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "accounts-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                        <td>
                                            <span>OK</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            4
                                        </td>
                                        <td>
                                            sales@logicalware.com
                                        </td>
                                        <td nowrap="nowrap">
                                            Sales &amp; Enquiries (Queue)
                                        </td>
                                        <td>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "accounts-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                        <td>
                                            <span>OK</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            5
                                        </td>
                                        <td>
                                            support@logicalware.com
                                        </td>
                                        <td nowrap="nowrap">
                                            Support (Queue)
                                        </td>
                                        <td>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "accounts-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                        <td>
                                            <span>OK</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            6
                                        </td>
                                        <td>
                                            trials@logicalware.com
                                        </td>
                                        <td nowrap="nowrap">
                                            Free Trials (Queue)
                                        </td>
                                        <td>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "accounts-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                        <td>
                                            <span>OK</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            7
                                        </td>
                                        <td>
                                            unknown@logicalware.com
                                        </td>
                                        <td nowrap="nowrap">
                                            Wrong email address (Queue)
                                        </td>
                                        <td>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                            <div class="priority-mark"></div>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "accounts-edit_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                        <td>
                                            <span>OK</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_button('footer-btn create', 'new-account', "Add New Account","accounts-add_form"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>