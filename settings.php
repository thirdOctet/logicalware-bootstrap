<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <i class="fa-cogs fa ic-backdrop ic-faded"></i>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <!-- settings info -->
                <div class="content-holder">
                    <div class="span2">
                        <div class="settings-default">
                            <!-- <i class="fa fa-info-circle fa-lg"></i> -->
                             <div class="ticket-helper info-btn">
                                <div class="display-inline-block">
                                    <span class="info">i</span>
                                </div>
                             </div>
                        </div>
                    </div>
                    <div class="span22">
                        Some of the options available in the Settings are permanent and cannot be undone. Please check before all actions are executed.
                    </div>
                    <div class="clearfix"></div>
                </div>

                <!-- info left -->
                <div class="item-holder text-center">

                    <?php /*
                    <div class="span12">

                        <!-- Accounts -->
                        <div class="content-holder">
                            <a class="settings-links" href="accounts">
                                <div class="layout-helper">
                                    <div class="span6 settings-title">Accounts</div>
                                    <div class="span18">Set up accounts so your helpdesk can receive emails</div>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </div>

                        <!-- Queues -->
                        <div class="content-holder">
                            <a class="settings-links" href="queues">
                                <div class="layout-helper">
                                    <div class="span6 settings-title">Queues</div>
                                    <div class="span18">Set up queues to store tickets until ready for processing</div>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </div>


                        <!-- Templates -->
                        <div class="content-holder">
                            <a class="settings-links" href="template">
                                <div class="layout-helper">
                                    <div class="span6 settings-title">Templates</div>
                                    <div class="span18">Set up templates for your users to use when replying to emails</div>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </div>


                        <!-- Tags -->
                        <div class="content-holder">
                            <a class="settings-links" href="ticket_tag">
                                <div class="layout-helper">
                                    <div class="span6 settings-title">Tags</div>
                                    <div class="span18">Set up ticket tags to tag your incoming emails</div>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </div>

                        <!-- KB -->
                        <div class="content-holder">
                            <a class="settings-links" href="kb">
                                <div class="layout-helper">
                                    <div class="span6 settings-title">KB</div>
                                    <div class="span18">Set up knowledge base topics for your users to use when replying to emails</div>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </div>

                        <!-- System -->
                        <div class="content-holder">
                            <a class="settings-links" href="system">
                                <div class="layout-helper">
                                    <div class="span6 settings-title">System</div>
                                    <div class="span18">Advanced System Settings</div>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </div>

                    </div>

                    <!-- Info right -->
                    <div class="span12">

                        <!-- Users -->
                        <div class="content-holder">
                            <a class="settings-links" href="users">
                                <div class="layout-helper">
                                    <div class="span6 settings-title">Users</div>
                                    <div class="span18">Set up users to process tickets</div>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </div>

                        <!-- Groups -->
                        <div class="content-holder">
                            <a class="settings-links" href="groups">
                                <div class="layout-helper">
                                    <div class="span6 settings-title">Groups</div>
                                    <div class="span18">Set up groups to organise your users</div>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </div>

                        <!-- filters -->
                        <div class="content-holder">
                            <a class="settings-links" href="filters">
                                <div class="layout-helper">
                                    <div class="span6 settings-title">Filters</div>
                                    <div class="span18">Set up filters to capture and manage specific incoming emails</div>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </div>

                        <!-- Categories -->
                        <div class="content-holder">
                            <a class="settings-links" href="category0">
                                <div class="layout-helper">
                                    <div class="span6 settings-title">Categories</div>
                                    <div class="span18">Set up categories to categorise your incoming emails</div>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </div>

                        <!-- Customers -->
                        <div class="content-holder">
                            <a class="settings-links" href="customers">
                                <div class="layout-helper">
                                    <div class="span6 settings-title">Customers</div>
                                    <div class="span18">Set up customers so they can review their own tickets</div>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </div>

                        <!-- Access Groups -->
                        <div class="content-holder">
                            <a class="settings-links" href="token">
                                <div class="layout-helper">
                                    <div class="span6 settings-title">Access Groups</div>
                                    <div class="span18">Access Groups</div>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </div>

                    </div>
                    */ ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php
echo get_footer();
?>
</div>
<?php include('views/footer.php'); ?>