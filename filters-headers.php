<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>    
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">

                <!-- NEW HEADER -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">NEW HEADER</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">lorem ipsum dolor sit amet</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">

                            <div class="item-holder">
                                <div class="span4 item-name">New Header:</div>
                                <div class="span14 child">
                                    <input type="text" />
                                </div>
                                <div class="span2"><?php echo get_input_button("default-btn save", "add-header", "Add") ?></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="content-holder">
                    <div class="table-container">
                        <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                            <table id="ticketlist" class="table table-striped">                                
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>HEADER</th>
                                        <th>ACTION</th>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>cc</td>                                        
                                        <td><?php echo get_button("default-btn delete", "", "Delete", ""); ?></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>From</td>                                        
                                        <td><?php echo get_button("default-btn delete", "", "Delete", ""); ?></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Subject</td>                                        
                                        <td><?php echo get_button("default-btn delete", "", "Delete", ""); ?></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>To</td>                                        
                                        <td><?php echo get_button("default-btn delete", "", "Delete", ""); ?></td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>X-Priority</td>                                        
                                        <td><?php echo get_button("default-btn delete", "", "Delete", ""); ?></td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>X-Spam-Flag</td>                                        
                                        <td><?php echo get_button("default-btn delete", "", "Delete", ""); ?></td>
                                    </tr>

                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div> 
        </div>            
    </div>
</div>
<?php
$btn_array = array();

//array_push($btn_array, get_button("footer-btn save", "save", "Add New Filter", "filters-add_form"));

echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>