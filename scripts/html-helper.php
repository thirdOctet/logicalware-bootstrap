<?php

function get_button($class, $id, $text, $link) {
    $html = "";
    $html .= "<a id='$id'";
    $html .= "class='$class' ";
    $html .= "href='$link'>";
    $html .= $text;
    $html .= "</a>";
    return $html;
}

/**
 * Generate input button
 *
 * @param String $class
 * @param String $id
 * @param String $value
 * @param String $name
 * @param String $type default set to button
 * @return String html representation of input button
 */
function get_input_button($class, $id, $value, $name = "", $type = "button") {
    $html = "";
    $html .= "<input type='$type' ";
    $html .= "id='$id'";
    $html .= "class='$class' ";
    $html .= "name='$name' ";
    $html .= "value='$value' />";
    return $html;
}

function get_footer($arrayOfButtons = array(), $page = '') {
    $html = "";
    $html .= "<div id='footer'>";
    $html .= "<div id='footeralignment' class='" . $page . "'>";
    if (count($arrayOfButtons) > 0) {
        foreach ($arrayOfButtons as $button) {
            $html.= $button;
        }
    }
    $html .= "</div>";
    $html .= "</div>";
    return $html;
}
?>