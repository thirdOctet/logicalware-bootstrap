<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>    
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">                  

                <!-- ADD FILTER -->
                <div class="content-holder">
                    <div class="ticket-holder">
                        <div class="ticket-header">
                            <span class="assigned align-left default-padding padding-top-bottom">EDIT FILTER</span>
                            <span class="ticket-arrow-assigned align-left"></span>
                            <div class="clearfix"></div>    
                        </div>
                        <div class="ticket-content default-padding show">

                            <!-- template name -->
                            <div class="item-holder add-filter">
                                <div class="span9">
                                    <span class="item-name">Conditions:</span>
                                    <span class="item-input">
                                        <select id="header_name_1" name="header_name_1">
                                            <option value="cc">cc</option>
                                            <option value="from" selected="selected">From</option>
                                            <option value="subject">Subject</option>
                                            <option value="to">To</option>
                                            <option value="x-priority">X-Priority</option>
                                            <option value="x-spam-flag">X-Spam-Flag</option>
                                            <option value="body">Message Body</option>
                                            <option value="account">Account</option>
                                            <option value="has_attachment">Has attachment</option>
                                        </select>
                                    </span>
                                </div>

                                <div class="span9">
                                    <span class="item-name">Contains:</span>
                                    <span class="item-input">
                                        <input type="text" />
                                    </span>
                                </div>

                                <div class="span1">
                                    <span class="item-name"></span>
                                    <span class="item-input text-right">
                                        <?php echo get_input_button("default-btn remove-row delete", "delete", "-"); ?>
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder add-filter">
                                <div class="span9">
                                    <span class="item-name">Conditions:</span>
                                    <span class="item-input">
                                        <select id="header_name_1" name="header_name_1">
                                            <option value="cc">cc</option>
                                            <option value="from" selected="selected">From</option>
                                            <option value="subject">Subject</option>
                                            <option value="to">To</option>
                                            <option value="x-priority">X-Priority</option>
                                            <option value="x-spam-flag">X-Spam-Flag</option>
                                            <option value="body">Message Body</option>
                                            <option value="account">Account</option>
                                            <option value="has_attachment">Has attachment</option>
                                        </select>
                                    </span>
                                </div>

                                <div class="span9">
                                    <span class="item-name">Contains:</span>
                                    <span class="item-input">
                                        <input type="text" value="thevarguy" />
                                    </span>
                                </div>

                                <div class="span1">
                                    <span class="item-name"></span>
                                    <span class="item-input text-right">
                                        <?php echo get_input_button("default-btn remove-row delete", "delete", "-"); ?>
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder add-filter">
                                <div class="span9">
                                    <span class="item-name">Conditions:</span>
                                    <span class="item-input">
                                        <select id="header_name_1" name="header_name_1">
                                            <option value="cc">cc</option>
                                            <option value="from" selected="selected">From</option>
                                            <option value="subject">Subject</option>
                                            <option value="to">To</option>
                                            <option value="x-priority">X-Priority</option>
                                            <option value="x-spam-flag">X-Spam-Flag</option>
                                            <option value="body">Message Body</option>
                                            <option value="account">Account</option>
                                            <option value="has_attachment">Has attachment</option>
                                        </select>
                                    </span>
                                </div>

                                <div class="span9">
                                    <span class="item-name">Contains:</span>
                                    <span class="item-input">
                                        <input type="text" value="seo" />
                                    </span>
                                </div>

                                <div class="span1">
                                    <span class="item-name"></span>
                                    <span class="item-input text-right">
                                        <?php echo get_input_button("default-btn remove-row delete", "delete", "-"); ?>
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="offset18 span2">                                   
                                    <span class="item-input text-right">
                                        <?php echo get_input_button("default-btn save new-condition full-width", "save", "New Condition"); ?>
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>                                

                    </div>                    
                </div>

                <!-- THEN SET -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">THEN SET</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left  padding-top-bottom">lorem ipsum dolor sit amet</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">                           

                            <!-- ASSIGN -->
                            <div class="item-holder">
                                <div class="span4 item-name">Assign to:</div>
                                <div class="span4">                                        
                                    <select id="assign_user" name="assign_user">
                                        <option value="">User</option>
                                        <option value="david">david</option>
                                        <option value="galtsev">galtsev</option>
                                        <option value="igor">igor</option>
                                        <option value="jonathan">jonathan</option>
                                        <option value="logicalware">logicalware</option>
                                        <option value="spamCollector" selected="">spamCollector</option>
                                        <option value="timets">timets</option>
                                        <option value="will">will</option>
                                    </select>                                       
                                </div>
                                <div class="span1 text-center">
                                    <span>or</span>
                                </div>
                                <div class="span4">
                                    <select id="assign_group" name="assign_group">
                                        <option value="" selected="">Group</option>
                                        <option value="Billing &amp; Invoicing">Billing &amp; Invoicing</option>
                                        <option value="Free Trials">Free Trials</option>
                                        <option value="Sales &amp; Enquiries">Sales &amp; Enquiries</option>
                                        <option value="Support">Support</option>
                                    </select>
                                </div>
                                <div class="span1 text-center">
                                    <span>or</span>
                                </div>
                                <div class="span4">
                                    <select id="assign_queue" name="assign_queue">
                                        <option value="" selected="">Queue</option>
                                        <option value="Billing &amp; Invoicing">Billing &amp; Invoicing</option>
                                        <option value="Free Trials">Free Trials</option>
                                        <option value="Sales &amp; Enquiries">Sales &amp; Enquiries</option>
                                        <option value="Support">Support</option>
                                        <option value="TRIALS">TRIALS</option>
                                        <option value="Wrong email address">Wrong email address</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <!-- ACCESS GROUP -->
                            <div class="item-holder">
                                <div class="span4 item-name">Default access group:</div>
                                <div class="span14">
                                    <select id="default_token" name="default_token">
                                        <option value="#auth">Any authorized user</option>
                                        <option value="u1">test</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <!-- PRIORITY -->
                            <div class="item-holder">
                                <div class="span4 item-name">Default Priority:</div>
                                <div class="span14">
                                    <select id="default_priority" name="default_priority">
                                        <option value="5">Critical</option>
                                        <option value="4">High</option>
                                        <option selected="selected" value="3">Normal</option>
                                        <option value="2">Low</option>
                                        <option value="1">Junk</option></select>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <!-- CATEGORY 0 -->                         
                            <div class="item-holder">
                                <div class="span4 item-name">Category 0:</div>
                                <div class="span14">
                                    <select id="default_category0" name="default_category0">
                                        <option selected="selected" value="">Not set</option>
                                        <option value="-">-</option>
                                        <option value="Billing">Billing</option>
                                        <option value="Billing &amp; Invoicing">Billing &amp; Invoicing</option>
                                        <option value="Free Trial">Free Trial</option>
                                        <option value="Free Trial: Prospect Enquiries">Free Trial: Prospect Enquiries</option>
                                        <option value="Free Trial: Website Notification">Free Trial: Website Notification</option>
                                        <option value="Ignore">Ignore</option>
                                        <option value="Internal: Instance deletion">Internal: Instance deletion</option>
                                        <option value="Internal: Test">Internal: Test</option>
                                        <option value="Internal: User deletion">Internal: User deletion</option>
                                        <option value="Junk/Spam">Junk/Spam</option>
                                        <option value="Sales &amp; Enquiries">Sales &amp; Enquiries</option>
                                        <option value="Support">Support</option>
                                        <option value="Zuzana">Zuzana</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <!-- CATEGORY 1 -->
                            <div class="item-holder">
                                <div class="span4 item-name">Category 1:</div>
                                <div class="span14">
                                    <select id="default_category1" name="default_category1">
                                        <option selected="selected" value="">Not set</option>
                                        <option value="-">-</option>
                                        <option value="1: Acknowledged">1: Acknowledged</option>
                                        <option value="2: In Process">2: In Process</option>
                                        <option value="3: Complete">3: Complete</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <!-- CATEGORY 2 -->
                            <div class="item-holder">
                                <div class="span4 item-name">Category 2:</div>
                                <div class="span14">
                                    <select id="default_category1" name="default_category1">
                                        <option selected="selected" value="">Not set</option>
                                        <option value="-">-</option>
                                        <option value="1: Acknowledged">1: Acknowledged</option>
                                        <option value="2: In Process">2: In Process</option>
                                        <option value="3: Complete">3: Complete</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <!-- RESPONSE TARGET -->
                            <div class="item-holder">
                                <div class="span4 item-name">Response Target:</div>
                                <div class="span14">
                                    <select id="response_target" name="response_target">
                                        <option selected="selected" value="0">Not assigned</option>
                                        <option value="900">0 days 00:15:00</option>
                                        <option value="1800">0 days 00:30:00</option>
                                        <option value="2700">0 days 00:45:00</option>
                                        <option value="3600">0 days 01:00:00</option>
                                        <option value="3600">0 days 01:00:00</option>
                                        <option value="7200">0 days 02:00:00</option>
                                        <option value="10800">0 days 03:00:00</option>
                                        <option value="14400">0 days 04:00:00</option>
                                        <option value="18000">0 days 05:00:00</option>
                                        <option value="21600">0 days 06:00:00</option>
                                        <option value="25200">0 days 07:00:00</option>
                                        <option value="28800">0 days 08:00:00</option>
                                        <option value="32400">0 days 09:00:00</option>
                                        <option value="36000">0 days 10:00:00</option>
                                        <option value="39600">0 days 11:00:00</option>
                                        <option value="43200">0 days 12:00:00</option>
                                        <option value="54000">0 days 15:00:00</option>
                                        <option value="64800">0 days 18:00:00</option>
                                        <option value="75600">0 days 21:00:00</option>
                                        <option value="86400">1 days 00:00:00</option>
                                        <option value="172800">2 days 00:00:00</option>
                                        <option value="259200">3 days 00:00:00</option>
                                        <option value="345600">4 days 00:00:00</option>
                                        <option value="432000">5 days 00:00:00</option>
                                        <option value="518400">6 days 00:00:00</option>
                                        <option value="604800">7 days 00:00:00</option>
                                        <option value="691200">8 days 00:00:00</option>
                                        <option value="777600">9 days 00:00:00</option>
                                        <option value="864000">10 days 00:00:00</option>
                                        <option value="950400">11 days 00:00:00</option>
                                        <option value="1036800">12 days 00:00:00</option>
                                        <option value="1123200">13 days 00:00:00</option>
                                        <option value="1209600">14 days 00:00:00</option>
                                        <option value="1296000">15 days 00:00:00</option>
                                        <option value="1382400">16 days 00:00:00</option>
                                        <option value="1468800">17 days 00:00:00</option>
                                        <option value="1555200">18 days 00:00:00</option>
                                        <option value="1641600">19 days 00:00:00</option>
                                        <option value="1728000">20 days 00:00:00</option>
                                        <option value="1814400">21 days 00:00:00</option>
                                        <option value="1900800">22 days 00:00:00</option>
                                        <option value="1987200">23 days 00:00:00</option>
                                        <option value="2073600">24 days 00:00:00</option>
                                        <option value="2160000">25 days 00:00:00</option>
                                        <option value="2246400">26 days 00:00:00</option>
                                        <option value="2332800">27 days 00:00:00</option>
                                        <option value="2419200">28 days 00:00:00</option>
                                        <option value="2505600">29 days 00:00:00</option>
                                        <option value="2592000">30 days 00:00:00</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <!-- MARK AS SPAM -->
                            <div class="item-holder">
                                <div class="span4 item-name">Mark as spam:</div>
                                <div class="span14">
                                    <div class="item-input margin-bottom-5px">
                                        <select id="status" name="status">
                                            <option value="" selected="">No</option>
                                            <option value="Spam">Yes</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <!-- AUTO REPLY TEMPLATE -->
                            <div class="item-holder">
                                <div class="span4 item-name">Auto Reply Template:</div>
                                <div class="span14">
                                    <div class="item-input margin-bottom-5px">
                                        <div class="span16">
                                            <input type="text" name="" value=""/>
                                        </div>
                                        <div class="span4">
                                            <?php echo get_input_button("default-btn full-width", "templateBtn", "Select") ?>
                                        </div>
                                        <div class="span4">
                                            <?php echo get_input_button("default-btn full-width delete", "clearBtn", "Clear") ?>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div id="template" class="margin-bottom-5px hide">
                                            <div class="">
                                                <div>
                                                    <span class="right-arrow"></span>
                                                    <span>Auto Responses</span>
                                                </div>
                                                <ul class="hide list">
                                                    <li>
                                                        <span class="block"></span>
                                                        <span class="template-response">Auto response:support</span>
                                                    </li>
                                                </ul>
                                                <div><span class="right-arrow"></span>
                                                    <span class="template-response">Contract Modifications</span></div>
                                                <ul class="hide list">        
                                                    <li><span class="block"></span>
                                                        <span class="template-response">Contract - Termination</span>
                                                    </li>
                                                    <li><span class="block"></span><span class="template-response">Contract - User Decrease</span></li>
                                                    <li><span class="block"></span>
                                                        <span class="template-response">Contract - User Increase</span>
                                                    </li>
                                                </ul>
                                                <div><span class="block"></span><span class="template-response">Auto response: Old Employees</span></div>
                                                <div><span class="block"></span><span class="template-response">Users - Delete</span></div>
                                                <div><span class="block"></span><span class="template-response">Cite all previous messages</span></div>
                                                <div><span class="block"></span><span class="template-response">Cite last message</span></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="item-holder">
                                <div class="span4 item-name">Stop Processing:</div>
                                <div class="span14">
                                    <div class="item-input margin-bottom-5px">
                                        <input type="checkbox" name="" value="" checked=""/>
                                        <span>Stop further filter processing once message matches the filter condition</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="item-holder">
                                <div class="span4 item-name">Close ticket:</div>
                                <div class="span14">
                                    <div class="item-input margin-bottom-5px">
                                        <input type="checkbox" name="" value="" checked=""/>
                                        <span>Automatically close created ticket once message matches the filter condition</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>


                            <div class="item-holder">
                                <div class="span4 item-name">Filters Group:</div>
                                <div class="span14">
                                    <select id="group_id" name="group_id">
                                        <option value="0">No assigned</option>
                                        <option value="8">Spam Capture</option>
                                        <option value="9">System</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="item-holder">
                                <div class="span4 item-name">Disable account signature:</div>
                                <div class="span14">
                                    <div class="item-input margin-bottom-5px">
                                        <input type="checkbox" name="" value=""/>                                            
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="item-holder">
                                <div class="span4 item-name">Disable AutoReply:</div>
                                <div class="span14">
                                    <div class="item-input margin-bottom-5px">
                                        <input type="checkbox" name="" value=""/>                                            
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>                                
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>               
        </div>
    </div>
    <div class="clearfix"></div>
</div> 

</div>
</div>
<?php
$btn_array = array();

array_push($btn_array, get_input_button("footer-btn save", "save", "Save"));

echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>