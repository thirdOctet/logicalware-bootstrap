<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="nsearch">
    <?php include('views/search.php'); ?>
    <?php // include('views/inner-menu-rbuilder.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="content-holder">
                    <div class="span12">
                        <div class="span4 item-name">Search Method:</div>
                        <div class="span18">
                            <div id="settings-default">
                                <!--                            <div class="ticket-helper info-btn">
                                                                <div class="display-inline-block">
                                                                    <span class="info">i</span>
                                                                </div>
                                                            </div>                                    -->
                                <span class="margin-left margin-right"></span>
                                <input class="" type="radio" name="search" checked="" value="basic"/><span class="margin-right">Basic</span>
                                <input class="" type="radio" name="search" value="advanced"/><span class="margin-right">Advanced</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="basic" class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">SEARCH</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Select ticket attributes to search for</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">

                        <!-- col 1 -->
                        <div class="span12">
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Search by date:</span>
                                </div>
                                <div class="span18">
                                    <span class="margin-right">
                                        <input type="radio" name="date" checked=""/><span>Open</span>
                                    </span>
                                    <span>
                                        <input type="radio" name="date" /><span>Closed</span>
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Date From:</span>
                                </div>
                                <div class="span18">
                                    <input type="text" class="datepicker date-plus-img" value="<?php echo date('y-m-d'); ?>"/>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Date To:</span>
                                </div>
                                <div class="span18">
                                    <input type="text" class="datepicker date-plus-img" value="<?php echo date('y-m-d'); ?>"/>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>From (name):</span>
                                </div>
                                <div class="span18">
                                    <input type="text" />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>From (email):</span>
                                </div>
                                <div class="span18">
                                    <input type="text" />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Note:</span>
                                </div>
                                <div class="span18">
                                    <input type="text" />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Sort By:</span>
                                </div>
                                <div class="span18">
                                    <select id="sort_by" name="sort_by">
                                        <option value="id">Ticket ID</option>
                                        <option value="date_opened">Date (re)opened</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Sort Order:</span>
                                </div>
                                <div class="span18">
                                    <span class="margin-right">
                                        <input type="radio" name="sort" checked=""/><span>Ascending</span>
                                    </span>
                                    <span>
                                        <input type="radio" name="sort" /><span>Descending</span>
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <!-- col 2 -->
                        <div class="offset1 span11">
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Account:</span>
                                </div>
                                <div class="span18">
                                    <div class="span2">
                                        <input type="checkbox" />
                                    </div>
                                    <div class="span22">
                                        <div class="item-input hide">
                                            <span class="select">Select:</span>
                                            <span class="select-all">All</span>
                                            <span class="select-none">None</span>
                                            <select id="account_id" multiple="multiple" name="account_id" size="7">
                                                <option value="billing@logicalware.com">billing@logicalware.com</option>
                                                <option value="enquiries@logicalware.com">enquiries@logicalware.com</option>
                                                <option value="lwtest@my.logicalware.net">lwtest@my.logicalware.net</option>
                                                <option value="sales@logicalware.com">sales@logicalware.com</option>
                                                <option value="support@logicalware.com">support@logicalware.com</option>
                                                <option value="trials@logicalware.com">trials@logicalware.com</option>
                                                <option value="unknown@logicalware.com">unknown@logicalware.com</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Tag:</span>
                                </div>
                                <div class="span18">
                                    <div class="span2">
                                        <input type="checkbox" />
                                    </div>
                                    <div class="span22">
                                        <div class="item-input hide">
                                            <span class="select">Select:</span>
                                            <span class="select-all">All</span>
                                            <span class="select-none">None</span>
                                            <select id="tag" multiple="multiple" name="tag" size="0"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Assigned to user:</span>
                                </div>
                                <div class="span18">
                                    <div class="span2">
                                        <input type="checkbox" />
                                    </div>
                                    <div class="span22">
                                        <div class="item-input hide">
                                            <span class="select">Select:</span>
                                            <span class="select-all">All</span>
                                            <span class="select-none">None</span>
                                            <select id="assigned" multiple="multiple" name="assigned" size="8">
                                                <option value="david">david</option>
                                                <option value="galtsev">galtsev</option>
                                                <option value="igor">igor</option>
                                                <option value="jonathan">jonathan</option>
                                                <option value="logicalware">logicalware</option>
                                                <option value="spamCollector">spamCollector</option>
                                                <option value="timets">timets</option>
                                                <option value="will">will</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Status:</span>
                                </div>
                                <div class="span18">
                                    <div class="span2">
                                        <input type="checkbox" />
                                    </div>
                                    <div class="span22">
                                        <div class="item-input hide">
                                            <span class="select">Select:</span>
                                            <span class="select-all">All</span>
                                            <span class="select-none">None</span>
                                            <select id="status" multiple="multiple" name="status" size="5">
                                                <option value="Open">Open</option>
                                                <option value="Hold">Hold</option>
                                                <option value="Closed">Closed</option>
                                                <option value="Archived">Archived</option>
                                                <option value="Spam">Spam</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Show Overdue Tickets:</span>
                                </div>
                                <div class="span18">
                                    <div class="span2">
                                        <input type="checkbox" />
                                    </div>
                                    <div class="span22">
                                        <div class="item-input hide"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Priority:</span>
                                </div>
                                <div class="span18">
                                    <div class="span2">
                                        <input type="checkbox" />
                                    </div>
                                    <div class="span22">
                                        <div class="item-input hide">
                                            <span class="select">Select:</span>
                                            <span class="select-all">All</span>
                                            <span class="select-none">None</span>
                                            <select id="priority" multiple="multiple" name="priority" size="5">
                                                <option value="5">Critical</option>
                                                <option value="4">High</option>
                                                <option value="3">Normal</option>
                                                <option value="2">Low</option>
                                                <option value="1">Junk</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Category0:</span>
                                </div>
                                <div class="span18">
                                    <div class="span2">
                                        <input type="checkbox" />
                                    </div>
                                    <div class="span22">
                                        <div class="item-input hide">
                                            <span class="select">Select:</span>
                                            <span class="select-all">All</span>
                                            <span class="select-none">None</span>
                                            <select id="category0" multiple="multiple" name="category0" size="12">
                                                <option value="-">-</option>
                                                <option value="Billing">Billing</option>
                                                <option value="Billing &amp; Invoicing">Billing &amp; Invoicing</option>
                                                <option value="Free Trial">Free Trial</option>
                                                <option value="Free Trial: Prospect Enquiries">Free Trial: Prospect Enquiries</option>
                                                <option value="Free Trial: Website Notification">Free Trial: Website Notification</option>
                                                <option value="Ignore">Ignore</option>
                                                <option value="Internal: Instance deletion">Internal: Instance deletion</option>
                                                <option value="Internal: Test">Internal: Test</option>
                                                <option value="Internal: User deletion">Internal: User deletion</option>
                                                <option value="Junk/Spam">Junk/Spam</option>
                                                <option value="Sales &amp; Enquiries">Sales &amp; Enquiries</option>
                                                <option value="Support">Support</option>
                                                <option value="Zuzana">Zuzana</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Category1:</span>
                                </div>
                                <div class="span18">
                                    <div class="span2">
                                        <input type="checkbox" />
                                    </div>
                                    <div class="span22">
                                        <div class="item-input hide">
                                            <span class="select">Select:</span>
                                            <span class="select-all">All</span>
                                            <span class="select-none">None</span>
                                            <select id="category1" multiple="multiple" name="category1" size="4">
                                                <option value="-">-</option>
                                                <option value="1: Acknowledged">1: Acknowledged</option>
                                                <option value="2: In Process">2: In Process</option>
                                                <option value="3: Complete">3: Complete</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>

                <div id="advanced" class="content-holder hide">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">SEARCH</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Enter text to search for</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="span12">
                            <div class="item-holder">
                                <div class="span6 item-name">
                                    <span>Full Text Search:</span>
                                </div>
                                <div class="span18">
                                    <input type="text" />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $btn_array = array();
    array_push($btn_array, get_input_button("default-btn save", "save", "Search"));
    echo get_footer($btn_array);
    ?>
</div>
<?php include('views/footer.php'); ?>