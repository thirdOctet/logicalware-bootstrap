<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">WORKTIME CALENDAR</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Set your working hours</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="" id="thiscalendar"></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="content-holder">
                        <div id="change_day">
                            <span for="current_day">Time interval for Day: </span>
                            <span id="current_day">8</span>
                            <span for="time_from">From</span>
                            <input type="text"value="" name="time_from" id="time_from">
                            <span for="time_to">to</span>
                            <input type="text" value="" name="time_to" id="time_to">
                            <input type="button" value="Apply" name="apply">
                            <input type="button" value="Clear" name="clear">
                            <input type="button" value="Cancel">
                            <p id="form_error" class="error" style="display: none;"></p>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_input_button("footer-btn save", "save", "Save"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>