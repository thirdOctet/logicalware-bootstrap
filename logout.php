<?php include('views/header.php'); ?>
<div id='login-background'>
    <div id='logincontainer'>
        <div id='login-top'>
            <img id="logo" src='img/logo-logicalware.png' />
        </div>
        <div id='login-form-container'>
            <div class="content-holder">
                <div class="default-padding">
                    <span class="display-inline-block">Thank you for logging out. <br><br> To log in click <a class="display-inline-block" href="login">here</a></span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('views/footer.php'); ?>