<?php
include('views/header.php');
include('views/sidebar.php');
?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="content-holder">
                            <a class="settings-links" href="queues">
                                <div class="layout-helper">
                                    <div class="span6 settings-title">Queues</div>
                                    <div class="span18">Set up queues to store tickets until ready for processing</div>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </div>
                <div class="content-holder">
                    <div class="table-container">
                        <table id="ticketlist" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>FULL NAME</th>
                                    <th>USERNAME</th>
                                    <th>ADMINISTRATOR</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        1
                                    </td>
                                    <td>
                                        David Lough
                                    </td>
                                    <td>
                                        David
                                    </td>
                                    <td>
                                        <i class="fa fa-check fa-2x tick"></i>
                                    </td>
                                    <td>
                                        <?php echo get_button("default-btn", "", "Edit", "users-edit_form"); ?>
                                        <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>galtsev</td>
                                    <td>galtsev</td>
                                    <td>
                                        <!-- <i class="fa fa-times fa-2x"></i> -->
                                        <i class="fa fa-times fa-2x"></i>
                                    </td>
                                    <td>
                                        <?php echo get_button("default-btn", "", "Edit", "users-edit_form"); ?>
                                        <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>igor</td>
                                    <td>igor</td>
                                    <td>
                                        <i class="fa fa-check fa-2x tick"></i>
                                    </td>
                                    <td>
                                        <?php echo get_button("default-btn", "", "Edit", "users-edit_form"); ?>
                                        <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Jonathan Hussey</td>
                                    <td>jonathan</td>
                                    <td>
                                        <i class="fa fa-check fa-2x tick"></i>
                                    </td>
                                    <td>
                                        <?php echo get_button("default-btn", "", "Edit", "users-edit_form"); ?>
                                        <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>logicalware</td>
                                    <td>logicalware</td>
                                    <td>
                                        <i class="fa fa-check fa-2x tick"></i>
                                    </td>
                                    <td>
                                        <?php echo get_button("default-btn", "", "Edit", "users-edit_form"); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Spam Collector</td>
                                    <td>spamCollector</td>
                                    <td>
                                        <i class="fa fa-times fa-2x"></i>
                                    </td>
                                    <td>
                                        <?php echo get_button("default-btn", "", "Edit", "users-edit_form"); ?>
                                        <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_button('footer-btn create', 'new-user', "Add New User", "users-add_form"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>