<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">


                <!--  -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">ADD NEW ARTICLE</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Add new article</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">

                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Title:</span>
                            </div>
                            <div class="span14">
                                <input type="text" value=""/>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Description:</span>
                            </div>
                            <div class="span14">
                                <textarea rows="5"></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Keywords:</span>
                                <span>
                                    <div class="ticket-helper info-btn info">
                                        <div class="display-inline-block">
                                            <span class="info">i</span>
                                        </div>
                                    </div>

                                </span>
                            </div>
                            <div class="span14">
                                <input type="text" value=""/>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Body:</span>
                            </div>
                            <div class="span20">
                                <textarea id="email-reply"></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();

array_push($btn_array, get_input_button("footer-btn save", "save", "Save"));

echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>