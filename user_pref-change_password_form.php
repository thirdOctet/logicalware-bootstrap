<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">PASSWORD</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Set &amp; confirm your new password</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <form accept-charset="utf-8" action="user_pref/change_password" method="post">
                         <div class="item-holder">
                            <div class="span4 item-name">New Password:</div>
                            <div class="span14">
                                <div class="item-input">
                                    <input type="password" name="passwd"/>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="item-holder">
                            <div class="span4 item-name">Confirm New Password:</div>
                            <div class="span14">
                                <div class="item-input">
                                    <input type="password" name="passwd_confirm"/>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_input_button("footer-btn save", "change-password", "Change Password","submit"));
echo get_footer($btn_array);
?>
</form>
</div>
<?php include('views/footer.php'); ?>