<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">CUSTOMERS</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="table-container">
                            <div class="table-container">
                                <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                                    <table id="ticketlist" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>NAME</th>
                                                <th>EMAIL</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Jonathan</td>
                                                <td>jonathan@logicalware.com</td>
                                                <td><?php echo get_button("default-btn delete", "", "Delete", ""); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Jonathan</td>
                                                <td>jonathan@logicalware.com</td>
                                                <td><?php echo get_button("default-btn delete", "", "Delete", ""); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_button("footer-btn save", "save", "Add New Customer", "customers-add_form"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>