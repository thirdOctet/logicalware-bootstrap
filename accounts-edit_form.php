<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content" class="">
        <div class="default-padding">
            <form id="" method="post" accept-charset="utf-8">

                <!-- ADD NEW ACCOUNT -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">ADD NEW ACCOUNT</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">lorem ipsum dolor sit amet</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="span18">
                                <div class="item-holder">
                                    <div class="span6 item-name">Email Address:</div>
                                    <div class="span18 child">
                                        <input type="text" value="" />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Assign to:</div>
                                    <div class="span5">
                                        <select id="assign_user" name="assign_user">
                                            <option value="">User</option>
                                            <option value="david">david</option>
                                            <option value="galtsev">galtsev</option>
                                            <option value="igor">igor</option>
                                            <option value="jonathan">jonathan</option>
                                            <option value="logicalware">logicalware</option>
                                            <option value="spamCollector">spamCollector</option>
                                            <option value="timets">timets</option>
                                            <option value="will">will</option>
                                        </select>                                       
                                    </div>
                                    <div class="span1 text-center">
                                        <span>or</span>
                                    </div>
                                    <div class="span5">
                                        <select id="assign_group" name="assign_group">
                                            <option value="">Group</option>
                                            <option value="Billing &amp; Invoicing">Billing &amp; Invoicing</option>
                                            <option value="Free Trials">Free Trials</option>
                                            <option value="Sales &amp; Enquiries">Sales &amp; Enquiries</option>
                                            <option value="Support">Support</option>
                                        </select>
                                    </div>
                                    <div class="span1 text-center">
                                        <span>or</span>
                                    </div>
                                    <div class="span5">
                                        <select id="assign_queue" name="assign_queue">
                                            <option value="">Queue</option>
                                            <option value="Billing &amp; Invoicing">Billing &amp; Invoicing</option>
                                            <option value="Free Trials">Free Trials</option>
                                            <option value="Sales &amp; Enquiries">Sales &amp; Enquiries</option>
                                            <option value="Support">Support</option>
                                            <option value="TRIALS">TRIALS</option>
                                            <option value="Wrong email address">Wrong email address</option>
                                        </select>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Notification: <span>*</span></div>
                                    <div class="span18 child">
                                        <div class="item-input margin-bottom-5px">
                                            <input type="checkbox" name="" value=""/>
                                            <span>Notify assigned user by email when new messages arrive</span>
                                        </div>
                                        <div class="item-input margin-bottom-5px">
                                            <input type="checkbox" name="" value=""/>
                                            <span>Notify other group members when new messages arrive</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>                            
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <!-- MAIL SOURCE -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">MAIL SOURCE</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Lorem ipsum dolor sit amet</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="span18">
                                <div class="item-holder">
                                    <div class="span6 item-name">Routing #1:</div>
                                    <div class="span18 child">
                                        <input type="radio" name="routing" value="user" checked="true"/>
                                        <span>Incoming mail forwarded to Account</span>
                                        <span class="help"> [help]</span>
                                        <div class="help-info margin-top hide">
                                            <p>This is the most efficient routing option as emails will arrive into your helpdesk almost instantly.
                                                To set this up you will need to set up forwarding on your email server for the email address (as above) that you wish to manage with this account.
                                                To get emails into this account, you should forward emails to:
                                            </p>                                            
                                            <p class="login-template">
                                                <span class="system">mm2-</span>
                                                <span class="systemname">systemname</span>
                                                <span>+</span>
                                                <span class="email">support=company.com</span>
                                                <span>@my.logicalware.net</span>                                            
                                            </p>                                            
                                            <div class="item-holder">
                                                <div class="span6 email bold-font">support=company.com</div>
                                                <div class="span18">is the email address of this account which you are currently setting up (with no @ symbol)</div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="item-holder">
                                                <div class="span6 systemname bold-font">systemname</div>
                                                <div class="span18">
                                                    <span>is taken from your browsers’ URL</span>
                                                    <span class="bold-font">https://dc2.logicalware.net/</span>
                                                    <span class="systemname bold-font">systemname</span>
                                                    <span class="bold-font">/mail</span></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Routing #2:</div>
                                    <div class="span18 child">
                                        <input type="radio" name="routing" value="user" />
                                        <span>Get from server (fill in details below)</span>
                                        <span class="help">[help]</span>
                                        <div class="help-info margin-top hide">
                                            <p>This is where we login to your email server every 10 minutes and collect any new emails.
To set this up you will need to provide us with the address, username, password, and server type of your email server.
Your current email client (outlook, thunderbird, etc), Internet Service Provider (ISP) or IT department will be able to provide you with these settings.</p>

                                        </div>

                                    </div>                                    
                                    <div class="clearfix"></div>
                                </div>                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <!-- OPTIONS -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">OPTIONS</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left  padding-top-bottom">lorem ipsum dolor sit amet</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="span18">
                                <div class="item-holder">
                                    <div class="span6 item-name">Default access group:</div>
                                    <div class="span18 child">
                                        <select id="default_token" name="default_token">
                                            <option value="#auth">Any authorized user</option>
                                            <option value="u1">test</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Default Priority:</div>
                                    <div class="span18 child">
                                        <select id="default_priority" name="default_priority">
                                            <option value="5">Critical</option>
                                            <option value="4">High</option>
                                            <option selected="selected" value="3">Normal</option>
                                            <option value="2">Low</option>
                                            <option value="1">Junk</option></select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Default Template:</div>
                                    <div class="span18 child">
                                        <select id="default_template" name="default_template">
                                            <option value="">Not assigned</option>
                                            <option value="cite_last">Cite Last Message</option>
                                            <option value="Auto response: Old Employees">Auto response: Old Employees</option>
                                            <option value="Auto response: Support">Auto response: Support</option>
                                            <option value="Contract - Termination">Contract - Termination</option>
                                            <option value="Contract - User Decrease">Contract - User Decrease</option>
                                            <option value="Contract - User Increase">Contract - User Increase</option>
                                            <option value="Users - Delete">Users - Delete</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Dedicated Templates Group:</div>
                                    <div class="span18 child">
                                        <select id="template_group" name="template_group">
                                            <option value="">Not assigned</option>
                                            <option value="Auto Responses">Auto Responses </option>
                                            <option value="Contract Modifications">Contract Modifications </option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Category 0:</div>
                                    <div class="span18 child">
                                        <select id="default_category0" name="default_category0">
                                            <option selected="selected" value="">Not set</option>
                                            <option value="-">-</option>
                                            <option value="Billing">Billing</option>
                                            <option value="Billing &amp; Invoicing">Billing &amp; Invoicing</option>
                                            <option value="Free Trial">Free Trial</option>
                                            <option value="Free Trial: Prospect Enquiries">Free Trial: Prospect Enquiries</option>
                                            <option value="Free Trial: Website Notification">Free Trial: Website Notification</option>
                                            <option value="Ignore">Ignore</option>
                                            <option value="Internal: Instance deletion">Internal: Instance deletion</option>
                                            <option value="Internal: Test">Internal: Test</option>
                                            <option value="Internal: User deletion">Internal: User deletion</option>
                                            <option value="Junk/Spam">Junk/Spam</option>
                                            <option value="Sales &amp; Enquiries">Sales &amp; Enquiries</option>
                                            <option value="Support">Support</option>
                                            <option value="Zuzana">Zuzana</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Category 1:</div>
                                    <div class="span18 child">
                                        <select id="default_category1" name="default_category1">
                                            <option selected="selected" value="">Not set</option>
                                            <option value="-">-</option>
                                            <option value="1: Acknowledged">1: Acknowledged</option>
                                            <option value="2: In Process">2: In Process</option>
                                            <option value="3: Complete">3: Complete</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Category 2:</div>
                                    <div class="span18 child">
                                        <select id="default_category1" name="default_category1">
                                            <option selected="selected" value="">Not set</option>
                                            <option value="-">-</option>
                                            <option value="1: Acknowledged">1: Acknowledged</option>
                                            <option value="2: In Process">2: In Process</option>
                                            <option value="3: Complete">3: Complete</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Response Target:</div>
                                    <div class="span18 child">
                                        <select id="response_target" name="response_target">
                                            <option selected="selected" value="0">Not assigned</option>
                                            <option value="900">0 days 00:15:00</option>
                                            <option value="1800">0 days 00:30:00</option>
                                            <option value="2700">0 days 00:45:00</option>
                                            <option value="3600">0 days 01:00:00</option>
                                            <option value="3600">0 days 01:00:00</option>
                                            <option value="7200">0 days 02:00:00</option>
                                            <option value="10800">0 days 03:00:00</option>
                                            <option value="14400">0 days 04:00:00</option>
                                            <option value="18000">0 days 05:00:00</option>
                                            <option value="21600">0 days 06:00:00</option>
                                            <option value="25200">0 days 07:00:00</option>
                                            <option value="28800">0 days 08:00:00</option>
                                            <option value="32400">0 days 09:00:00</option>
                                            <option value="36000">0 days 10:00:00</option>
                                            <option value="39600">0 days 11:00:00</option>
                                            <option value="43200">0 days 12:00:00</option>
                                            <option value="54000">0 days 15:00:00</option>
                                            <option value="64800">0 days 18:00:00</option>
                                            <option value="75600">0 days 21:00:00</option>
                                            <option value="86400">1 days 00:00:00</option>
                                            <option value="172800">2 days 00:00:00</option>
                                            <option value="259200">3 days 00:00:00</option>
                                            <option value="345600">4 days 00:00:00</option>
                                            <option value="432000">5 days 00:00:00</option>
                                            <option value="518400">6 days 00:00:00</option>
                                            <option value="604800">7 days 00:00:00</option>
                                            <option value="691200">8 days 00:00:00</option>
                                            <option value="777600">9 days 00:00:00</option>
                                            <option value="864000">10 days 00:00:00</option>
                                            <option value="950400">11 days 00:00:00</option>
                                            <option value="1036800">12 days 00:00:00</option>
                                            <option value="1123200">13 days 00:00:00</option>
                                            <option value="1209600">14 days 00:00:00</option>
                                            <option value="1296000">15 days 00:00:00</option>
                                            <option value="1382400">16 days 00:00:00</option>
                                            <option value="1468800">17 days 00:00:00</option>
                                            <option value="1555200">18 days 00:00:00</option>
                                            <option value="1641600">19 days 00:00:00</option>
                                            <option value="1728000">20 days 00:00:00</option>
                                            <option value="1814400">21 days 00:00:00</option>
                                            <option value="1900800">22 days 00:00:00</option>
                                            <option value="1987200">23 days 00:00:00</option>
                                            <option value="2073600">24 days 00:00:00</option>
                                            <option value="2160000">25 days 00:00:00</option>
                                            <option value="2246400">26 days 00:00:00</option>
                                            <option value="2332800">27 days 00:00:00</option>
                                            <option value="2419200">28 days 00:00:00</option>
                                            <option value="2505600">29 days 00:00:00</option>
                                            <option value="2592000">30 days 00:00:00</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">TID Subject:</div>
                                    <div class="span18 child">
                                        <div class="item-input margin-bottom-5px">
                                            <input type="checkbox" name="" value=""/>
                                            <span>Add ticket ID to Subject</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Auto Reply:</div>
                                    <div class="span18 child">
                                        <div class="item-input margin-bottom-5px">
                                            <input type="checkbox" name="" value=""/>
                                            <span>Send Auto Reply to every ticket automatically</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">AutoReply Template:</div>
                                    <div class="span18 child">
                                        <select id="template_name" name="template_name">
                                            <option value="">Not assigned</option>
                                            <option value="Auto response: Old Employees">Auto response: Old Employees</option>
                                            <option value="Auto response: Support">Auto response: Support</option>
                                            <option value="Contract - Termination">Contract - Termination</option>
                                            <option value="Contract - User Decrease">Contract - User Decrease</option>
                                            <option value="Contract - User Increase">Contract - User Increase</option>
                                            <option value="Users - Delete">Users - Delete</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Signature:</div>
                                    <div class="span18 child">
                                        <div class="item-input margin-bottom-5px">
                                            <input type="checkbox" name="" value=""/>
                                            <span>Use Signature</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <!-- SET TEMPLATE RESPONSE FOR USERS -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">SIGNATURE</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left  padding-top-bottom">Set the users email signature</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="item-holder">
                                <textarea id="email-reply"></textarea>                        
                            </div>
                        </div>
                    </div>
                </div>        
            </form>            
        </div>

    </div>
    <?php
    $btn_array = array();

    array_push($btn_array, get_input_button("footer-btn save", "save", "Save"));

    echo get_footer($btn_array);
    ?>
</div>
<?php include('views/footer.php'); ?>