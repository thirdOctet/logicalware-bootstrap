<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-home.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class='row-fluid'>
                <div class='span12'>
                    <div class="content-holder">
                        <div class="table-container">
                            <table id="" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ACCOUNTS FOR JONATHAN</th>
                                        <th>OPEN</th>
                                        <th>OVERDUE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    for ($i = 0; $i < 10; $i++) {
                                        echo '<tr>';
                                        echo '<td><a href="nticket">billing@logicalware.com</a></td>';
                                        echo '<td><a href="nticket">1</a></td>';
                                        echo '<td><a href="nticket">0</a></td>';
                                        echo '</tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class='span12'>
                    <div class="content-holder">
                        <div class="table-container">
                            <table id="" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>QUEUES FOR JONATHAN</th>
                                        <th>OPEN</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    for ($i = 0; $i < 10; $i++) {
                                        echo '<tr>';
                                        echo '<td><a href="nticket">billing@logicalware.com</a></td>';
                                        echo '<td><a href="nticket">1</a></td>';
                                        echo '<td><a class="status default-btn" id="">Get Ticket</a></td>';
                                        echo '</tr >';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class='clearfix'></div>
            </div>
        </div>
    </div>
</div>
<?php

//    include('scripts/html-helper.php');

    $btn_array = array();

    array_push($btn_array, get_button("footer-btn create", "create", "Create Ticket", "create_ticket"));

    echo get_footer($btn_array);



?>
</div>
<?php include('views/footer.php'); ?>