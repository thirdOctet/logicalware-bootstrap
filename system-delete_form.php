<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">TICKETS</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Select which tickets to delete</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">

                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Account:</span>
                            </div>
                            <div class="span14">
                                <select id="account_id" name="account_id">
                                    <option value="">Any</option>
                                    <option value="lwtest@my.logicalware.net">lwtest@my.logicalware.net</option>
                                    <option value="sales@logicalware.com">sales@logicalware.com</option>
                                    <option value="enquiries@logicalware.com">enquiries@logicalware.com</option>
                                    <option value="support@logicalware.com">support@logicalware.com</option>
                                    <option value="trials@logicalware.com">trials@logicalware.com</option>
                                    <option value="billing@logicalware.com">billing@logicalware.com</option><option value="unknown@logicalware.com">unknown@logicalware.com</option></select>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>User:</span>
                            </div>
                            <div class="span14">
                                <select id="assigned" name="assigned"><option value="">Any</option>
                                    <option value="david">david</option>
                                    <option value="galtsev">galtsev</option>
                                    <option value="igor">igor</option>
                                    <option value="jonathan">jonathan</option>
                                    <option value="logicalware">logicalware</option>
                                    <option value="spamCollector">spamCollector</option>
                                    <option value="timets">timets</option>
                                    <option value="will">will</option></select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Delete tickets older than:</span>
                            </div>
                            <div class="span14">
                                <input type="text" class="datepicker date-plus-img" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>State:</span>
                            </div>
                            <div class="span14">
                                <div class="item-input">
                                    <input type="checkbox" checked="" /><span>Closed</span>
                                    <input type="checkbox" checked="" /> <span>Spam</span>
                                    <input type="checkbox" checked="" /> <span>Archive</span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Enquiry Type:</span>
                            </div>
                            <div class="span14">
                                <select id="category0" name="category0"><option selected="selected" value="">Not set</option>
                                    <option value="-">-</option>
                                    <option value="Billing">Billing</option>
                                    <option value="Billing &amp; Invoicing">Billing &amp; Invoicing</option>
                                    <option value="Free Trial">Free Trial</option>
                                    <option value="Free Trial: Prospect Enquiries">Free Trial: Prospect Enquiries</option>
                                    <option value="Free Trial: Website Notification">Free Trial: Website Notification</option>
                                    <option value="Ignore">Ignore</option>
                                    <option value="Internal: Instance deletion">Internal: Instance deletion</option>
                                    <option value="Internal: Test">Internal: Test</option>
                                    <option value="Internal: User deletion">Internal: User deletion</option>
                                    <option value="Junk/Spam">Junk/Spam</option>
                                    <option value="Sales &amp; Enquiries">Sales &amp; Enquiries</option>
                                    <option value="Support">Support</option>
                                    <option value="Zuzana">Zuzana</option></select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Progress:</span>
                            </div>
                            <div class="span14">
                                <select id="category1" name="category1"><option selected="selected" value="">Not set</option>
                                    <option value="-">-</option>
                                    <option value="1: Acknowledged">1: Acknowledged</option>
                                    <option value="2: In Process">2: In Process</option>
                                    <option value="3: Complete">3: Complete</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">OPTIONS</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Choose to download deleted tickets</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Download deleted tickets:</span>
                            </div>
                            <div class="span14">
                                <input type="checkbox" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Email to notify when download ready:</span>
                            </div>
                            <div class="span14">
                                <input type="text" value="jonathan@logicalware.com" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">HISTORY</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Previous delete requests</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">

                        <div class="table-container">
                            <div class="table-container">
                                <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                                    <table id="ticketlist" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>FROM</th>
                                                <th>EXPORT DATA</th>
                                                <th>NOTIFY TO</th>
                                                <th>FINISHED</th>
                                                <th>FILENAME</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>20.05.13 @ 10:26</td>
                                                <td><i class="fa fa-times fa-2x"></i></td>
                                                <td>dlough@logicalware.com</td>
                                                <td>20.05.13 @ 21:15</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>20.05.13 @ 10:26</td>
                                                <td><i class="fa fa-times fa-2x"></i></td>
                                                <td>dlough@logicalware.com</td>
                                                <td>20.05.13 @ 21:15</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>20.05.13 @ 10:26</td>
                                                <td><i class="fa fa-times fa-2x"></i></td>
                                                <td>dlough@logicalware.com</td>
                                                <td>20.05.13 @ 21:15</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>20.05.13 @ 10:26</td>
                                                <td><i class="fa fa-times fa-2x"></i></td>
                                                <td>dlough@logicalware.com</td>
                                                <td>20.05.13 @ 21:15</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>20.05.13 @ 10:26</td>
                                                <td><i class="fa fa-times fa-2x"></i></td>
                                                <td>dlough@logicalware.com</td>
                                                <td>20.05.13 @ 21:15</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>20.05.13 @ 10:26</td>
                                                <td><i class="fa fa-times fa-2x"></i></td>
                                                <td>dlough@logicalware.com</td>
                                                <td>20.05.13 @ 21:15</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>20.05.13 @ 10:26</td>
                                                <td><i class="fa fa-times fa-2x"></i></td>
                                                <td>dlough@logicalware.com</td>
                                                <td>20.05.13 @ 21:15</td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_input_button("footer-btn delete", "delete", "Delete"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>