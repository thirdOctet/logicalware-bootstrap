<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-reports.php'); ?>    
    <div id="content">
        <div class="default-padding">
            <div class="content-holder">
                <div class="table-container">
                    <table class="table table table-striped">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Open</th>
                                <th>Overdue</th>
                                <th>Hold</th>
                                <th>Closed</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>david</td><td>1</td><td>0</td><td>1</td><td>220</td>
                            </tr>
                            <tr>
                                <td>galtsev</td><td>0</td><td>0</td><td>0</td><td>2</td>
                            </tr>
                            <tr>
                                <td>igor</td><td>0</td><td>0</td><td>0</td><td>76</td>
                            </tr>
                            <tr>
                                <td>jonathan</td><td>0</td><td>0</td><td>0</td><td>1</td>
                            </tr>
                            <tr>
                                <td>logicalware</td><td>0</td><td>0</td><td>0</td><td>11</td>
                            </tr>
                            <tr>
                                <td>timets</td><td>0</td><td>0</td><td>0</td><td>80</td>
                            </tr>
                            <tr>
                                <td>will</td><td>0</td><td>0</td><td>1</td><td>12</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php include('views/footer-rbuilder.php'); ?>
</div>
<?php include('views/footer.php'); ?>