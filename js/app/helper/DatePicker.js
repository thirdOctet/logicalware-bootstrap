define(['jquery'], function($) {

    var DatePicker = function() {

        /**
         * [defaultclass description]
         * @type {String}
         */
        var defaultclass = "";

        /**
         * [getYearFrom description]
         * @param  {[type]} years [description]
         * @return {[type]}       [description]
         */
        this.getYearFrom = function(years) {
            return this.getCurrentYear() - years;
        }

        /**
         * [getCurrentYear description]
         * @return {[type]} [description]
         */
        this.getCurrentYear = function() {
            return new Date().getFullYear();
        }

    };

    return DatePicker;


});
