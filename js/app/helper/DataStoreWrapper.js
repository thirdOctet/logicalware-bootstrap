define(['jquery', 'underscore', 'json2', 'persistjs'], function($, _) {
    var DataStore = function() {

        //TODO  implement typeof checks for key value pairs for strings

        //preferences will be stored against different default types
        //e.g. menu, layout, defaults e.t.c
        //
        //http://stackoverflow.com/questions/191881/serializing-to-json-in-jquery

        var _dataStore;

        var setStore = function() {
            _dataStore = new Persist.Store('logicalware');
        };

        this.storeContains = function(key) {
            return _dataStore.get(key) ? true : false;
        };

        this.init = function() {
            setStore();
        }

        /**
         * Add data to store
         * Will overwrite key if set
         * @param  {[type]} key  [description]
         * @param  {[type]} data [description]
         * @return {[type]}      [description]
         */
        this.saveData = function(key, data) {
            _dataStore.set(key, data)
        }

        this.getData = function(key) {
            return _dataStore.get(key);
        };

        this.removeData = function(key) {
            _dataStore.remove(key);
        };
    };

    /**
     * Singleton set up
     */
    var datawrapperinstance = null;

    var getInstance = function() {
        if (!this.datawrapperinstance) {
            this.datawrapperinstance = new DataStore();
            this.datawrapperinstance.init();
        }
        return this.datawrapperinstance;
    };
    return getInstance();
});
