define(['jquery', 'underscore'], function($, _) {
    /**
     * A module for managing urls and interactivity with layout or components <br><br>
     * Uses OO concepts to implement data management<br>
     * The urlHelper module removes some of the complexities of :- <br>
     * <ol>
     * <li>Identifying the current page a user is on</li>
     * <li>Holds a key:value map of the url structure common to all pages</li>
     * </ol>
     */
    var UrlHelper = function() {

        'use strict';
        /**
         * Set the base url
         * @todo Change on go live
         */
        var base = window.location.protocol + '//' + window.location.host + '/';
        /**
         * source directory set based on url structure
         * In live environment this may be preconfigured
         *
         * Current system directories
         * Images       -   /mailmanager/static/images/
         * Basepath     -   https://dc2.logicalware.net/your-system-name/mail/
         * @todo Change on go live
         */
        var basedirectory = "";
        /**
         * The current url
         */
        var currenturl = window.location.href;
        /**
         * A map of the url structure
         * May be vulnerable as based on the current url structure
         * My suggestion would be to be able to place an ajax request to a sitemap of the layout as this might change
         * This structure is at the heart of a majority of the functionality and with little maintenance allows for
         * a multitude of time saving techniques
         * An example is loading modules based on url match, this can be modified for performance and ease
         * of maintainability in the future.
         * @todo Try to set up based on predefined url structure
         */
        var pages = {
            home: ["index_html"],
            login: ["login"],
            logout: ["logout"],
            queue: ["queues_list"],
            report: ["nreport", "rbuilder"],
            search: ["nsearch"],
            settings: ["accounts", "blacklist", "category", "customers", "filters", "groups", "kb",
                "queues", "settings", "std_attach", "system", "tag", "template", "token", "user_pref", "users"
            ],
            ticket: ["create_ticket", "nticket", "ticket_list"]
        };
        /**
         * Set to the key value of the pages object
         * {String}
         */
        var parent = "";
        /**
         * Tracks the active value in the pages object
         */
        var activepage = "";
        /**
         * Refers to the index of the array position within the
         * key value relationship set in the pages object
         */
        var pageindex = -1;
        /**
         * Sets the parent as the key of pages
         * Sets the active page based on the parent
         * Sets the page index based on the key of the current page
         */
        var setParent = function() {

        	//jsperf results on indexof vs mathc, exec e.t.c
        	//http://jsperf.com/exec-vs-match-vs-test-vs-search/16

            //get the pages object and loop through each element
            $.each(pages, function(val, i) {
                for (var j = 0, len = i.length; j < len; j++) {
                    //if the value is found in the url
                    if (currenturl.indexOf(i[j]) > -1) {
                        //set the parent value
                        //i.e. reference to page key
                        parent = val;
                        //set the index referencing the current page
                        pageindex = j;
                        //set the active page
                        //i.e. reference to actual value contained within url
                        activepage = i[j];
                        return false;
                    }
                }
            });

        };

        /**
         * In a live environment this will be hardcoded <br>
         * Sets the main directory for resource location <br>
         * Example:- basedirectory + 'images/myimage.png'
         */
        var setSourceFolder = function() {
            if (!basedirectory) {
                var splitarray = currenturl.split('/', 4);
                basedirectory = base + splitarray[3] + '/';
            }
        };

        /**
         * Get the current page <br>
         * This is the key of the pages object<br>
         * Allows for key to be used to reference current menu item
         * @returns {String}
         */
        this.getParent = function() {
            return parent;
        };
        /**
         * Get the active page or the current section of the system<br>
         * That is that section of the url which aids in highlighting where
         * the user is in the system
         * @returns {String}
         */
        this.getActivePage = function() {
            return activepage;
        };
        /**
         * Get the basedirectory
         * @returns {String}
         */
        this.getBaseDirectory = function() {
            return basedirectory;
        };
        /**
         * Get the current url
         * @returns {String}
         */
        this.getCurrentUrl = function() {
            return currenturl;
        };
        this.tostring = function() {
            var output = [];
            output.push("base : " + base);
            output.push("basedirectory : " + basedirectory);
            output.push("currenturl : " + currenturl);
            output.push("parent : " + parent);
            output.push("activepage : " + activepage);
            output.push("pageindex : " + pageindex);
            output.push("pages : " + JSON.stringify(pages, null, '\t'));
            return output.join('\n');
        };

        /**
         * Actions that must take place on object initialisation
         */
        var init = function() {
            setParent();
            setSourceFolder();
        };

        //default initialisation on object
        init();
    };
    //must place object name before returning singleton
    var urlhelperinstance = null;
    var getInstance = function() {
        if (!this.urlhelperinstance) {
            this.urlhelperinstance = new UrlHelper();
        }
        return this.urlhelperinstance;
    };
    return getInstance();
});
