define(['jquery'], function($) {
    var LayoutHelper = function() {
        /**
         * Key-Value pairing between id of an element and jquery object of element
         * @type {Object}
         */
        var layoutMap = {
            //default
            'body': $('body')
        };
        /**
         * Used to reference self throughout
         * @type {Object}
         */
        var $this = this;
        /**
         * Register layout elements based on id of element
         * Will overwrite key if already present
         * @param  {String} segmentname      The name of the segment being added, lowercase
         * @param  {Object} targetobject     A jquery object based on id, e.g. $('#main')
         */
        this.registerSeg = function(segmentname, targetobject) {
            layoutMap[segmentname] = targetobject;
        };
        /**
         * Map of layout elements populated by recursive layout getter
         * Can also be populated by regsterSeg method
         * @return {Object} The map of layout elements
         */
        var getMap = function() {
            return layoutMap;
        };
        /**
         * Get the value from the layout map
         * @param  {String} key The value to be retrieved
         * @return {Object}     returns a jquery object
         */
        this.getMapValue = function(key) {
            return getMap()[key];
        };
        /**
         * Check if layoutmap contains the key
         * @param  {String} name
         * @return {boolean}
         */
        this.layoutMapContains = function(name) {
            return this.getMapValue(name) ? true : false;
        };
        /**
         * Recursively populates the layout map
         * @param  {String} name  Name of layout segment
         * @param  {Number} depth The number of levels to traverse
         */
        this.recursiveLayoutBuilder = function(name, depth) {
            if (depth < 1) {
                return;
            }
            if (this.getMapValue(name)) {
                $.each(this.getMapValue(name).children(), function() {
                    if ($(this).prop('id')) {
                        $this.registerSeg($(this).prop('id'), $(this));
                    }
                    $this.recursiveLayoutBuilder($(this).prop('id'), --depth);
                });
            }
        };
        /**
         * Add Key/value pair to  layout section
         * @param  {String} name  The layout section that the data should be stored against
         * @param  {String} key
         * @param  {[type]} value
         */
        this.pushData = function(name, key, value) {
            return layoutMapContains(name) ? this.getMapValue(name).data(key, value) : false;
        };
        /**
         * Get Data stored against layout element
         * @param  {String} name The id of the element stored in the layout map
         * @param  {[type]} key  [description]
         * @return {Object}      Returns a jquery object
         */
        this.pullData = function(name, key) {
            return layoutMapContains(name) ? this.getMapValue(name).data(key) : false;
        };
        /**
         * Set height on top level elements bound by the searchbar and footer.
         * @param {Array} array Values that height is to be manually set at
         */
        this.setTopLevelHeight = function(name) {
            if (this.layoutMapContains(name)) {
                this.getMapValue(name).height($(window).height() - this.getHeight('footer') - this.getHeight('searchbar'));
            }
        };
        /**
         * Get the height of element within the layoutmap
         * @param  {String} name the id of the element
         * @return {Number}      Height of element or zero
         */
        this.getHeight = function(name) {
            return this.layoutMapContains(name) ? this.getMapValue(name).height() : 0;
        };
        /**
         * Allows for the scrollbar to be applied to multiple elements
         * Defaults to horizontal scroll
         * @param {Array} array Array of strings to target within the layoutmap to apply scrollbar element
         */
        this.setScrollbar = function(array, scrollbar) {
            //get the current representation of the object after
            //population
            $.each(array, function(index, value) {
                if ($this.layoutMapContains(value)) {
                    //default scrollbar settings
                    $this.getMapValue(value).mCustomScrollbar({
                        advanced: {
                            autoScrollOnFocus: false,
                            updateOnContentResize: true,
                            updateOnBrowserResize: true
                        },
                        scrollButtons: {
                            enable: false
                        },
                        scrollInertia: 0,
                        contentTouchScroll: true
                    });
                }
            });
        };

        this.resetHorizontalScrollbar = function(element, closestParent) {
            $(element).width($(element).closest(closestParent).width());
            $(element).closest(closestParent).mCustomScrollbar('destroy');
            $(element).closest(closestParent).mCustomScrollbar({
                advanced: {
                    updateOnBrowserResize: true
                },
                scrollButtons: {
                    enable: false
                },
                horizontalScroll: true,
                set_width: '100%',
                scrollInertia: 0
            });
        };
    };
    /**
     * Singleton set up
     */
    var layouthelperinstance = null;
    /**
     * Get instance of layouthelper if already created
     * else create a new instance
     * @return {Object} LayoutHelper
     */
    var getInstance = function() {
        if (!this.layouthelperinstance) {
            this.layouthelperinstance = new LayoutHelper();
        }
        return this.layouthelperinstance;
    };
    return getInstance();
});
