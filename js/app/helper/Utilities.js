define(['jquery'], function($) {
    var UtilitiesHelper = function() {
        /**
         * All checkboxes are wrapped with the icheck plugin
         * Depends on html structure
         * @param  {string} targetclass The class or id you are trying to target
         * @param  {string} parent      The parent container e.g. table
         */
        this.checkAll = function(targetclass, parent) {
            $(targetclass).on({
                ifChecked: function() {
                    $(this).closest(parent).find(':checkbox').each(function() {
                        $(this).iCheck('check');
                    });
                },
                ifUnchecked: function() {
                    $(this).closest(parent).find(':checkbox').each(function() {
                        $(this).iCheck('uncheck');
                    });
                }
            });
        };
        /**
         * Adds accordion functionality to containers with header
         * @param  {String} target     The class or id you are trying to target
         * @param  {String} targetnext The content
         * @param  {Number} animSpeed  The animation speed when the header closes or opens
         */
        this.accordionGlobal = function(target, targetnext, animSpeed) {
            $('body').on('click', target, function() {
                if ($(this).next(targetnext).is(':hidden')) {
                    $(this).next(targetnext).slideDown(animSpeed);
                } else {
                    $(this).next(targetnext).slideUp(animSpeed);
                }
            });
        };

        /**
         * [accordion_menu description]
         * @param  {String} target     The node in which the event must act on
         * @param  {String} targetnext The next node to target e.g. class or id of div, span e.t.c
         * @param  {Integer} animSpeed The speed of the animation
         * @param  {String} initial    class to toggle
         * @param  {String} change     class to toggle
         */
        this.accordionMenu = function(target, targetnext, animSpeed, initial, change) {
            $('body').on('click', target, function() {
                if ($(this).parent().find(targetnext).is(':hidden')) {
                    $(this).parent().find(targetnext).slideDown(animSpeed).toggleClass("hide");
                    $(this).toggleClass(initial).toggleClass(change);;
                } else {
                    $(this).parent().find(targetnext).slideUp(animSpeed);
                    $(this).toggleClass(initial).toggleClass(change);
                }
            });
        };
        /**
         * Add expansion functionality to menus containing lists
         * @param  {String} targetparent The parent element - e.g. .menu-section, #footer
         * @param  {String} target       This is the section which correlates with the <br>
         *                               activepage property within the url handler - this must be assigned <br>
         *                               as a class within the html of the menu
         */
        this.listMenuExpand = function(targetparent, target) {
            if ($(targetparent + ' ' +  target).length > 0) {
                if ($(targetparent + ' ' +  target).has("ul")) {
                    $(targetparent + ' ' +  target).children('ul').toggleClass("hide");

                    //will need consideration as classes are hard coded - may need updated for flexibility
                    $(targetparent + ' ' +  target).children('i').toggleClass("fa-plus").toggleClass("fa-minus");
                }
            }
        };

        /**
         * requirejs calls to modules
         * @param {string} parentname The name of the parent file to be called         *
         */
        this.pageLoader = function(parentname) {
            var module = 'app/pages/' + parentname;
            require([module], function() {
                //module loaded
            }, function(err) {});
        };

        /**
         * Load module based on single page
         * @param  {String} singlename The module name to be loaded
         */
        this.singleLoader = function(singlename) {
            var module = 'app/pages/single/' + singlename;
            require([module], function() {
                //module loaded
            }, function(err) {});
        };
    };
    return UtilitiesHelper;
});