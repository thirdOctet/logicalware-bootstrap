define(['jquery', 'app/helper/Layout'], function($, Layout) {

    var lh = Layout;

    //ie8 and ie7 fix
    ////source: http://docs.tinyfactory.co/jquery/2012/12/18/HTML5-Placeholder-In-ie7-and-ie8-fix.html
    $('input[placeholder]').each(function() {
        var input = $(this);
        $(input).val(input.attr('placeholder'));
        $(input).focus(function() {
            if (input.val() == input.attr('placeholder')) {
                input.val('');
            }
        });
        $(input).blur(function() {
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.val(input.attr('placeholder'));
            }
        });
    });

    //http://stackoverflow.com/questions/5284878/how-do-i-correctly-detect-orientation-change-using-javascript-and-phonegap-in-io
    // function doOnOrientationChange() {
    //     switch (window.orientation) {
    //         case 0:
    //         case 90:
    //             lh.resizeHorizontalScrollbar('.table','.table-container');
    //             break;
    //         default:
    //             lh.resetHorizontalScrollbar('.table', '.table-container');
    //             break;
    //     }
    // }

    // window.addEventListener('orientationchange', doOnOrientationChange);

});
