define(['jquery', 'underscore', 'app/helper/Url'], function($, _, Url) {
    var User = function() {
        //need to get user role status e.t.c

        var uh = Url;
        var systemname;
        var user;
        var role;

        this.getRole = function(){
            return role;
        }

        var setSystemName = function() {
            systemname = uh.getCurrentUrl().split('/', 4)[3];
        };

        this.getSystemName = function() {
            return systemname;
        }
    };
    return User;
});
