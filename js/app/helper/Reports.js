define(['jquery', 'moment'], function($, moment) {
    var ReportsHelper = function() {
        var filter = {
            snapshot: [
                "By status and accounts",
                "By status and users",
                "By status and queue",
                "By priority and account",
                "By priority and user",
                "By priority and queue"
            ],
            performance: [
                "User Events",
                "Queue Events",
                "Account Events"
            ]
        };

        /**
         * [getFilter description]
         * @param  {[type]} key [description]
         * @return {[type]}     [description]
         */
        this.getFilter = function(key) {
            return filter[key];
        };

        /**
         * Set the weekly period for reports - Sunday - Saturday
         * Plugin may allow customised start dates
         * @param {Number} interval The number of weeks to view from
         * @return {Array} dateTracker Array of strings representing date
         */
        this.setWeeklyPeriod = function(interval) {
            var weeklyhtml = "";
            for (var i = interval; i >= 0; i--) {
                weeklyhtml += "<option value='" + moment().startOf('week').add('weeks', i *-1).format('YYYY-MM-DD') + "'>";
                weeklyhtml += moment().startOf('week').add('weeks', i * -1).format('DD.MM.YYYY');
                weeklyhtml += " - ";
                weeklyhtml += moment().endOf('week').add('weeks', i * -1).format('DD.MM.YYYY');
                weeklyhtml += "</option>";
            };
            return weeklyhtml;
        };
    };
    return ReportsHelper;
});
