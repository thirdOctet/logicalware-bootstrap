require(['jquery', 'app/helper/Layout', 'app/helper/Url'], function($, Layout, Url) {
    var lh = Layout;
    var uh = Url;

    //bind login container to centre of page
    lh.getMapValue('logincontainer').css({
        top: $(document).height() / 2 - lh.getMapValue('logincontainer').height() / 2
    });

    //change position based on page resize
    $(window).on({
        resize: function(event) {
            lh.getMapValue('logincontainer').css({
                top: $(document).height() / 2 - lh.getMapValue('logincontainer').height() / 2
            });
        }
    });
});
