require(['jquery', 'app/helper/Url', 'app/helper/Layout',
        'app/helper/Utilities'
    ],
    function($, Url, Layout, Utilities) {
        //get layouthelper instance
        var lh = Layout,
            uh = Url;
        uth = new Utilities();
        uth.singleLoader(uh.getActivePage());
    });
