require(['jquery', 'underscore', 'chartjs', 'app/helper/Url',
    'app/helper/Layout', 'app/helper/Utilities', 'app/helper/Reports'
], function($, _, Chartjs, Url, Layout, Utilities, Reports) {

    var uh = Url,
        lh = Layout,
        uth = new Utilities(),
        rh = new Reports();

    //populates weekly period with up to 10 (or any number) week history
    $('#weekly select').html(rh.setWeeklyPeriod(10));

    //checks if menu section contains an unordered list
    uth.listMenuExpand('.menu-section', '.' + uh.getActivePage());

    $('#report-by').on({
        change: function() {

            //get the selected text within the report by area
            var text = $(this).val().toLowerCase().replace(/\s/gi, '');

            //output container
            var html = '<option>' + rh.getFilter(text).join('</option><option>') + '</option>';

            //remove current content
            $('#info-to-display').html(html);

            if ($(this).prop('selectedIndex') > 0) {
                if ($('.reports_date').is(':hidden'))
                    $('.reports_date').toggleClass('hide');
            } else if ($(this).prop('selectedIndex') === 0) {
                $('.reports_date').toggleClass('hide');
            }
        }
    });

    $('#period-selection').on({
        change: function() {
            //hide all elements
            $(this).parent().children('div').addClass('hide');
            //get text
            var text = $(this).val().toLowerCase().replace(/\s/gi, '');
            //show time period
            $('#' + text).removeClass('hide');
        }
    });

    //load single page
    uth.singleLoader(uh.getActivePage());
});
