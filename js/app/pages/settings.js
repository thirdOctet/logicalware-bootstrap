require(['jquery', 'app/helper/Url', 'app/helper/Layout', 'app/helper/Utilities', 'tinyMCE', 'fullcalendar'], function($, Url, Layout, Utilities, tinyMCE) {
    //initialise
    var lh = Layout,
        uh = Url;
        uth = new Utilities();

    $('#thiscalendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        firstDay: 1,
        weekMode: 'liquid'
    });

    //checks if menu section contains an unordered list
    uth.listMenuExpand('.menu-section', '.' + uh.getActivePage());

    //tinymce general initialisation
    tinyMCE.init({
        height: 300,
        browser_spellcheck: true,
        selector: "textarea#email-reply",
        toolbar1: "formatselect fontselect fontsizeselect | bold italic underline strikethrough |  \n\
        alignleft aligncenter alignright alignjustify | cut copy paste",
        toolbar2: "bullist numlist | outdent indent | undo redo |  removeformat | link unlink | forecolor backcolor | searchreplace fullscreen | ",
        plugins: ["paste wordcount searchreplace fullscreen print link table tabfocus textcolor ",
            "image media insertdatetime table contextmenu",
            "advlist autolink lists charmap preview anchor"
        ],
        paste_as_text: true,
        menubar: "file edit insert view format table"
    });

    //load module
    uth.singleLoader(uh.getActivePage());
});
