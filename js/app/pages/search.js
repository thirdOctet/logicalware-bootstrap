require(['jquery', 'underscore', 'app/helper/Url', 'app/helper/Layout', 'app/helper/Utilities', 'app/helper/DatePicker'], function($, _, Url, Layout, Utilities, DatePicker) {
    var uh = Url,
        lh = Layout,
        uth = new Utilities(),
        dp = new DatePicker();

    uth.singleLoader(uh.getActivePage());

    $('.nsearch').on('ifClicked', ':checkbox', function() {
        $(this).closest('.item-holder').find('.item-input').toggleClass('hide');
    });

    $('#settings-default').on('ifChecked', ':radio', function() {
        var text = $(this).val().toLowerCase().replace(/\s/gi, '');
        $('.content-holder').not(':first').toggleClass("hide");
    });
});
