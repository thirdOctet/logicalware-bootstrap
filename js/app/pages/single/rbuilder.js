require(['jquery', 'underscore'], function($, _) {
    $('.columnsdisplay').on('click', '#columnsadd', function() {
        $.each($('#available option:selected'), function() {
            $('#selected').append('<option value="' + $(this).attr('value') + '">' + $(this).text() + '</option>');
            $(this).remove();
        });
    });
    $('.columnsdisplay').on('click', '#columnsremove', function() {
        $.each($('#selected option:selected'), function() {
            $('#available').append('<option value="' + $(this).attr('value') + '">' + $(this).text() + '</option>');
            $(this).remove();
        });
    });

});
