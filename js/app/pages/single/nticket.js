require(['jquery', 'app/helper/Url', 'app/helper/Layout', 'tinyMCE', 'scrollto'], function($, Url, Layout, tinyMCE) {
    var uh = Url;
    var lh = Layout;
    lh.getMapValue('smartbar').on('click', '#scroll-to-top', function() {
        lh.getMapValue('content').mCustomScrollbar("scrollTo", "top");
        //        lh.getMapValue('content').scrollTop(0);
    });
    lh.getMapValue('smartbar').on('click', '#scroll-to-response', function() {
        lh.getMapValue('content').mCustomScrollbar("scrollTo", ".ticket-response");
        //        lh.getMapValue('content').scrollTo('.ticket-response');
    });
    lh.getMapValue('smartbar').on('click', '#scroll-to-last-ticket', function() {
        if ($('#last-ticket .ticket-content').is(':hidden')) {
            $('#last-ticket .ticket-content').slideDown();
        }
        lh.getMapValue('content').mCustomScrollbar("scrollTo", "#last-ticket");
    });
    lh.getMapValue('smartbar').on('click', '#expand-all-tickets', function() {
        var allContent = $('.ticket-content,.ticket-note-content').not(':last');
        if ($(this).children('i').hasClass('fa-plus')) {
            allContent.slideDown();
            $(this).children('i').toggleClass('fa-plus').toggleClass('fa-minus');
        } else {
            allContent.slideUp();
            $(this).children('i').toggleClass('fa-plus').toggleClass('fa-minus');
        }
    });

    tinyMCE.init({
        setup: function(ed) {
            ed.on('keyup', function(ed, e) {
                //if any content create array split on html tags and check length
                //usually if greater than 1 there is some content
                if (this.getContent().length >= 1) {
                    $('#ticket-display-textarea-keypress').removeClass('hide').addClass('show');
                    $('#ticket-display-default').removeClass('show').addClass('hide');
                } else {
                    $('#ticket-display-textarea-keypress').removeClass('show').addClass('hide');
                    $('#ticket-display-default').removeClass('hide').addClass('show');
                }
            });
        },
        height: 300,
        browser_spellcheck: true,
        selector: "textarea#email-reply",
        toolbar1: "formatselect fontselect fontsizeselect | bold italic underline strikethrough |  \n\
        alignleft aligncenter alignright alignjustify | cut copy paste",
        toolbar2: "bullist numlist | outdent indent | undo redo |  removeformat | link unlink | forecolor backcolor | searchreplace fullscreen | ",
        plugins: ["paste wordcount searchreplace fullscreen print link table tabfocus textcolor ",
            "image media insertdatetime table contextmenu",
            "advlist autolink lists charmap preview anchor"
        ],
        paste_as_text: true,
        menubar: "file edit insert view format table"
    });


});