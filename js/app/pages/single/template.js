require(['jquery', 'tinyMCE'], function($, tinyMCE) {

    //can be moved to a separate handler
    function newTemplateToggle() {
        $('.content-holder').eq(1).find('.ticket-content').toggle();
        $('.content-holder').eq(1).toggle();
        $('#footeralignment input').toggle();
    }

    $('#footer .template').on('click', '#add-new-template', newTemplateToggle);
    $('#footer .template').on('click', '.cancel', newTemplateToggle);

    tinyMCE.init({
        height: 300,
        browser_spellcheck: true,
        selector: "textarea#email-reply",
        toolbar1: "formatselect fontselect fontsizeselect | bold italic underline strikethrough |  \n\
        alignleft aligncenter alignright alignjustify | cut copy paste",
        toolbar2: "bullist numlist | outdent indent | undo redo |  removeformat | link unlink | forecolor backcolor | searchreplace fullscreen | ",
        plugins: ["paste wordcount searchreplace fullscreen print link table tabfocus textcolor ",
            "image media insertdatetime table contextmenu",
            "advlist autolink lists charmap preview anchor"
        ],
        paste_as_text: true,
        menubar: "file edit insert view format table"
    });
});
