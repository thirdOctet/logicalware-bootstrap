define(['jquery'], function($) {
    var emailToAdd = $('.add-email .item-holder').first().clone();
    $('.content-holder').on('click', '#add-email', function(event) {
        $(this).prev('.add-email').is(':hidden') ? $(this).prev('.add-email').removeClass('hide') : $('.add-email .item-holder').last().after(emailToAdd.clone());
    });
    $('.add-email').on('click', '.remove-email', function() {
        $('.remove-email').length > 1 ? $(this).closest('.item-holder').remove() : $('.add-email').toggleClass('hide');
    });
});
