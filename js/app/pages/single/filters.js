define(['jquery'], function($) {
    //add option
    $('.content-holder').on('click', '.new-condition', function(event) {
        $(this).closest('.item-holder').before($('.add-filter').first().clone());
    });
    //remove option
    $('.content-holder').on('click', '.remove-row', function() {
        if ($('.remove-row').length > 1) {
            if ($(this).closest('.item-holder').index() !== 0) {
                $(this).closest('.item-holder').remove();
            }
        }
    });
});
