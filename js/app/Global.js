require([
    'jquery', 'app/helper/Url', 'app/helper/Layout', 'app/helper/Utilities',
    'app/helper/DatePicker', 'app/helper/DataStoreWrapper', 'moment', 'app/helper/BrowserFix',
    'mcustomscrollbar', 'icheck', 'mousewheel', 'jqueryui'
], function($, Url, Layout, Utilities, DatePicker, DataStoreWrapper, moment) {
    $(function() {
        //performance tracking
        var start = new Date().getTime();

        var uh = Url,
            lh = Layout,
            uth = new Utilities(),
            dp = new DatePicker(),
            ds = DataStoreWrapper;

        /**************************************************************************
         * UI Configurations                                                      *
         **************************************************************************/

        //Get top level elements recursively depending on depth based on parent element
        lh.recursiveLayoutBuilder('body', 3);

        //peformance concern
        //sets the active menu based on current section
        //sidebar ids must contain corresponding category title
        var activemenu = lh.layoutMapContains('innermenu') ? 'activemenu' : 'activemenu-nm';
        // document.getElementById(uh.getParent()).classList.add('menu-active');
        $('#' + uh.getParent()).addClass('menu-active').prepend("<i class='fa fa-caret-left " + activemenu + "'></i>");
        $('#' + uh.getParent()).prepend("<i class='fa fa-caret-left " + activemenu + "'></i>");

        /**************************************************************************
         * UI - Scrollbar                                                         *
         *************************************************************************/
        //http://manos.malihu.gr/jquery-custom-content-scroller/

        //must set the height of the content,innermenu
        //before initialising scrollbar
        lh.setTopLevelHeight('content');
        lh.setTopLevelHeight('innermenu');

        //initialise vertical scrollbars
        lh.setScrollbar(['content', 'innermenu']);

        //all tables on mobile devices will stretch beyond content
        //this is a means to create scrollable tables within .content-holder
        //for mobile devices
        //tables should always be the width of the parent table-container
        lh.resetHorizontalScrollbar('.table', '.table-container');

        $('#outerbar').mCustomScrollbar({
            advanced: {
                updateOnBrowserResize: true
            },
            contentTouchScroll: true,
            scrollButtons: {
                enable: false
            },
            scrollInertia: 0
        });

        /**************************************************************************
         * UI - Checkbox & radio                                                  *
         **************************************************************************/
        //initialise plugin on checkboxes
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-grey',
            radioClass: 'iradio_square-grey',
            increaseArea: '10%',
            labelHover: false,
            cursor: true
        });


        /**************************************************************************
         * UI - General                                                           *
         **************************************************************************/

        //global initialisers
        uth.accordionGlobal('.menu-header', 'div', 10);
        uth.accordionGlobal('.ticket-header', 'div', 10);
        uth.accordionGlobal('.ticket-note-header', 'div', 10);
        uth.accordionMenu('.menu-section i', 'ul', 10, 'fa-plus', 'fa-minus');

        //applies checkbox functionality across all tables with checkbox within table head
        uth.checkAll('th :checkbox', 'table');

        //default setup for datepickers
        /**
         * @todo check if required to be global or varies among pages
         */
        $('.datepicker').datepicker({
            buttonImageOnly: false,
            buttonText: '<i class="fa fa-calendar"></i>',
            showOn: 'both', //focus, button, both
            changeYear: true,
            changeMonth: true,
            yearRange: dp.getYearFrom(10) + ":" + dp.getCurrentYear(),
            dateFormat: 'yy-mm-dd'
        });

        /**************************************************************************
         * UI -                                                      		      *
         **************************************************************************/

        //interactivity in search bar
        $('#search-form').length > 0 ? $('#search-form').data('original', $('#search-form').offset().left) : '';
        $('#search-form').length > 0 ? $('#search-form').data('collapsed', $('#search-form').data('original') - $('#outerbar').width()) : '';
        $('#searchbar').on('click', '#menucontrol', function() {
            $('#innermenu').toggle();
            $('#outerbar').toggle();
            var distance;
            if ($('#outerbar').is(':hidden')) {
                distance = 10;
                $('#search-form').css({
                    left: $('#search-form').data('collapsed')
                });
            } else {
                distance = $('#outerbar').width() + 10;
                $('#search-form').css({
                    left: $('#search-form').data('original')
                });
            }
            //ensures visibility icon matches position of other elements
            $(this).css({
                left: distance + "px"
            });
            lh.resetHorizontalScrollbar('.table', '.table-container');
        });

        $('#searchbar').on('click', '#notificationshow', function() {
            $('#notificationmain').toggle();
        });

        $('#notificationshow .badge, .notifynumber').html($('.nmclose').length);

        $('#notificationmain').on('click', '.nmclose', function() {
            $(this).closest('tr').fadeOut(100, function() {
                $(this).remove();
                $('#notificationshow .badge, .notifynumber').html($('.nmclose').length);
            });
        });

        /**
         * TODO Move functionality to separate class
         */
        $('.response-attributes').on('click', '#templateBtn,#personalTemplateBtn,#attachFilesBtn', function() {
            $('#' + $(this).prop('id').split('Btn')[0]).toggleClass('hide');
        });

        $('.response-attributes').on('click', '#ccBtn,#bccBtn', function() {
            $this = $(this);
            var selected = $(this).html().split(/\s/)[1].toLowerCase();
            var lcSelected = selected;
            $('#' + lcSelected + '-field').hasClass('hide') ? $this.html("Clear " + selected.toUpperCase()) : $this.html("Add " + selected.toUpperCase());
            $('#' + lcSelected + '-field').toggleClass('hide');
        });

        /**
         * @todo Specific to response attributes, perhaps move to separate class/module
         */
        $('#template').on('click', 'div', function() {
            var firsticon = $(this).children('i').first();
            if ($(this).next().is('ul')) {
                $(this).next().toggleClass('hide');
                firsticon.toggleClass('fa-plus').toggleClass('fa-minus');
            }
        });

        //apply
        // $('#footer .form-container').mCustomScrollbar({
        //     advanced: {
        //         updateOnBrowserResize: true
        //     },
        //     scrollButtons: {
        //         enable: false
        //     },
        //     horizontalScroll: true,
        //     set_width: '100%',
        //     contentTouchScroll: true,
        //     scrollInertia: 0
        // });

        // $('#footer .mCSB_container').width($('#footer').width() - $('#outerbar').width());
        // $('#footer .form-container').mCustomScrollbar('update');

        /**************************************************************************
         * System - Load Module                                                   *
         **************************************************************************/
        //load modules based on parent calculated within urlhelper
        //modules must be named accordingly
        //pages object within urlhelper must be updated to track
        //note: all functionality within this file is called before the execution of another require module
        uth.pageLoader(uh.getParent());

        console.log('time in ms: '+ (new Date().getTime() - start));

        $(window).on({
            resize: function() {

                //always set the height of content for ui purposes
                lh.setTopLevelHeight('content');
                lh.setTopLevelHeight('innermenu');

                //Check that scroll bar exists
                if (!$('#content .mCSB_horizontal .mCSB_container').length > 0) {

                    //add scrollbar
                    $('.table').closest('.table-container').mCustomScrollbar({
                        advanced: {
                            updateOnBrowserResize: true
                        },
                        scrollButtons: {
                            enable: false
                        },
                        horizontalScroll: true,
                        set_width: '100%',
                        scrollInertia: 0
                    });
                } else {
                    //ensure that horizontal scrollbar is present
                    if ($('#content .content-holder .mCSB_horizontal .mCSB_container').length > 0) {
                        lh.resetHorizontalScrollbar('.table', '.table-container');
                        //remove scrollbar
                        // $('.table').closest('.table-container').mCustomScrollbar("destroy");
                        // $('.table').width($('.table').closest('.table-container').width());
                    }
                }

                // $('#footer .mCSB_container').width($('#footer').width() - $('#outerbar').width());
                // $('#footer .form-container').mCustomScrollbar('update');
            }
        });
    });
});
// lh.registerSeg('footeralignment', $('#footeralignment'));
// lh.getMapValue('footeralignment').css({left: '50px'});
// lh.getMapValue('footeralignment').mCustomScrollbar({
//     advanced: {
//         updateOnBrowserResize: true
//     },
//     scrollButtons: {
//         enable: false
//     },
//     horizontalScroll: true,
//     // set_width: '100%',
//     scrollInertia: 0
// });
// lh.getMapValue('footeralignment').find(".mCSB_container").width($(window).width() - lh.getMapValue('outerbar').width());
// lh.getMapValue('footeralignment').mCustomScrollbar('update');
//
// lh.registerSeg('faform', $('#footeralignment form'));
// lh.getMapValue('faform').mCustomScrollbar({
//     advanced: {
//         updateOnBrowserResize: true
//     },
//     scrollButtons: {
//         enable: false
//     },
//     horizontalScroll: true,
//     contentTouchScroll: true,
//     scrollInertia: 0
// });

// //form-container width
// lh.getMapValue('footer').find('.form-container').width();
// lh.getMapValue('faform').find(".mCSB_container").width($(window).width() - lh.getMapValue('outerbar').width()).css({
//     left: lh.getMapValue('outerbar').width()
// });
// lh.getMapValue('faform').find(".mCustomScrollBox").width($(window).width() - lh.getMapValue('outerbar').width()).css({
//     // left: lh.getMapValue('outerbar').width()
// });
// lh.getMapValue('faform').mCustomScrollbar('update');
// lh.getMapValue('faform').find(".mCSB_container").width($(window).width() - lh.getMapValue('outerbar').width()).css({
//     left: lh.getMapValue('outerbar').width()
// });
// lh.getMapValue('faform').find(".mCustomScrollBox").width($(window).width() - lh.getMapValue('outerbar').width()).css({
//     left: lh.getMapValue('outerbar').width()
// });
// // lh.getMapValue('faform').find(".mCustomScrollBox").width($(window).width() - lh.getMapValue('outerbar').width());
// lh.getMapValue('faform').mCustomScrollbar('update');

//     //tables should always be the width of the parent content-holder
//     $('table').width($('table').closest('.table-container').width());
