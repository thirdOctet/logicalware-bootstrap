/**
 *
 * @author Jonathan Hussey - jhusseydesign
 *
 */
$.noConflict();
jQuery(document).ready(function($) {
    /**
     * A module for managing urls and interactivity with layout or components <br><br>
     * Uses OO concepts to implement data management<br>
     * The urlhandler module removes some of the complexities of :- <br>
     * <ol>
     * <li>Identifying the current page a user is on</li>
     * <li>Holds a key:value map of the url structure common to all pages</li>
     * </ol>
     */

    function UrlHandler() {
        /**
         * Set the base url
         * @todo Change on go live
         */
        var base = window.location.protocol + '//' + window.location.host + '/';
        /**
         * source directory set based on url structure
         * In live environment this may be preconfigured
         *
         * Current system directories
         * Images       -   /mailmanager/static/images/
         * Basepath     -   https://dc2.logicalware.net/your-system-name/mail/
         * @todo  Change on go live
         */
        var basedirectory = "";
        /**
         * The current url
         */
        var currenturl = window.location.href;
        /**
         * A map of the url structure
         * May be vulnerable as based on the current url structure
         */
        var pages = {
            home: ["index_html"],
            login: ["login"],
            logout: ["logout"],
            queue: ["queues_list"],
            report: ["nreport", "rbuilder"],
            search: ["nsearch"],
            settings: ["accounts", "blacklist", "category", "customers", "filters", "groups", "kb",
                "queues", "settings", "std_attach", "system", "tag", "template", "token", "user_pref", "users"
            ],
            ticket: ["create_ticket", "nticket", "ticket_list"]
        };
        /**
         * Set to the key value of the pages object
         * {String}
         */
        var currentpage = "";
        /**
         * Tracks the active value in the pages object
         * {String}
         */
        var activepage = "";
        /**
         * Refers to the index of the array position within the
         * key value relationship set in the pages object
         * {Number}
         */
        var pageindex = -1;
        /**
         * Sets the currentpage as the key of pages
         * Sets the active page based on the currentpage
         * Sets the page index based on the key of the current page
         */
        var setCurrentPage = function() {
            //get the pages object and loop through each element
            $.each(pages, function(val, i) {
                for (var j = 0, len = i.length; j < len; j++) {
                    //if the value is found in the url
                    if (currenturl.search(i[j]) > -1) {
                        //set the currentpage value
                        //i.e. reference to page key
                        //used to highlight menu
                        currentpage = val;
                        //set the index referencing the current page
                        pageindex = j;
                        //set the active page
                        //i.e. reference to actual value contained within url
                        activepage = i[j];
                        return false;
                    }
                }
            });
        };
        /**
         * In a live environment this will be hardcoded <br>
         * Sets the main directory for resource location <br>
         * Example:- basedirectory + 'images/myimage.png'
         *
         */
        var setSourceFolder = function() {
            if (!basedirectory) {
                var splitarray = currenturl.split('/', 4);
                basedirectory = base + splitarray[3] + '/';
            }
        };
        /**
         *
         *
         */
        this.init = function() {
            setCurrentPage();
            setSourceFolder();
        };
        /**
         * Get the current page <br>
         * This is the key of the pages object<br>
         * Allows for key to be used to reference current menu item
         * @returns {String}
         */
        this.getcurrentpage = function() {
            return currentpage;
        };
        /**
         * Get the active page or the current section of the system<br>
         * That is that section of the url which aids in highlighting where
         * the user is in the system
         * @returns {String}
         */
        this.getactivepage = function() {
            return activepage;
        };
        /**
         * Get the basedirectory
         * @returns {String}
         */
        this.getbasedirectory = function() {
            return basedirectory;
        };
        /**
         * Get the current url
         * @returns {String}
         */
        this.getcurrenturl = function() {
            return currenturl;
        };
    }
    /**
     * A module for managing elements on the reports page
     */

    function ReportsHandler() {
        /**
         * The filter represents the different drop down options on the reports page
         */
        var filter = {
            snapshot: [
                "By status and accounts",
                "By status and users",
                "By status and queue",
                "By priority and account",
                "By priority and user",
                "By priority and queue"
            ],
            performance: [
                "User Events",
                "Queue Events",
                "Account Events"
            ]
            //            ,accounts: [
            //                "Tickets Opened",
            //                "Closed & Unresponded",
            //                "Performance",
            //                "Targets met",
            //                "Account Events",
            //                "By View Time"
            //            ],
            //            users: [
            //                "Tickets Opened",
            //                "Tickets Closed",
            //                "Closed & Unresponded",
            //                "Performance",
            //                "Targets met",
            //                "Responses by Users",
            //                "User Events",
            //                "By View Time"
            //            ],
            //            enquirytype: [
            //                "Tickets Opened",
            //                "Tickets Closed",
            //                "Closed & Unresponded",
            //                "Performance",
            //                "Overdue",
            //                "Targets met",
            //                "By View Time"
            //            ],
            //            queue: [
            //                "Tickets Opened",
            //                "Performance",
            //                "Targets met",
            //                "User Events"
            //            ],
            //            other: [
            //                "KB Usage",
            //                "Email Address"
            //            ]
        };
        /**
         * Get the array associated with the key
         * <br>
         * This will return the drop down items associated with the selected report
         *
         * @param {String} key
         * @returns {Array}
         */
        this.getFilter = function(key) {
            return filter[key];
        };
    }
    //create objects
    var urlhandler = new UrlHandler();
    //initial set up
    urlhandler.init();
    /**
     * Set global attributes
     */
    defaults = {
        animspeed: 20
    };
    /**
     * Interactive components
     */
    components = {
        smartbar: $('#smartbar')
        //add other icon elements
    };
    /**
     * An arrow component
     * @returns {Object}
     */

    function arrow() {
        /**
         * The html output of the arrow<br>
         * Default class is left-arrow
         * @type String|String
         */
        var html = "<span class='left-arrow'></span>";
        /**
         * The color of the collapsed arrow<br>
         * @type String
         */
        var colorCollapsed = "#efefef";
        /**
         * The color of the expanded arrow
         * @type String
         */
        var colorExpanded = "#fff";
        /**
         * Get the html
         * @returns {String}
         */
        this.gethtml = function() {
            return html;
        };
        /**
         * Sets the html with the altered class
         * @param {String} theclass The class to be set for the arrow
         */
        this.setClass = function(theclass) {
            html = "<span class='" + theclass + "'></span>";
        };
    }
    /**
     * Specific to menu when highlighting active page on outerbar
     */
    menuarrow = {
        /**
         * default html for menu arrow
         */
        html: "<span class='left-arrow'></span>",
        /**
         * The color of the arrow when innermenu is collapsed
         */
        colorCollapsed: '#efefef',
        /**
         * Color of arrow when innermenu expanded
         */
        colorExpanded: '#fff',
        /**
         * Set the class for the arrow e.g. right-arrow
         * @param {String} arrowclass The name of the class
         */
        sethtml: function(arrowclass) {
            this.html = "<span class='" + arrowclass + '"></span>';
        }
    };
    /**
     * Captures main layout sections
     */
    layouthandler = {
        searchbar: $('#search-bar'),
        footer: $('#footer-dynamic'),
        innermenu: $('#inner-menu'),
        outermenu: $('#outerbar'),
        content: $('#content')
    };
    if (layouthandler.innermenu.length > 0) {
        layouthandler.innermenu.data('width', layouthandler.innermenu.width());
    } else {
        hide_element('#toggle-menu');
    }
    /**
     * Specific to interactions
     * Settings for different ui elements
     */
    uihandler = {
        screen: {
            /**
             * Area of detection for smartbar on y-axis
             * {Number}
             */
            hoverareaX: -1,
            /**
             * area of detection for smartbar on y-axis
             * {Number}
             */
            hoverareaY: -1,
            /**
             *
             *
             */
            sethoverareaX: function() {
                this.hoverareaX = layouthandler.outermenu.width() + layouthandler.innermenu.data('width');
            },
            sethoverareaY: function() {
                this.hoverareaY = $(window).height() - layouthandler.footer.height() - components.smartbar.height() * 2;
            },
            gethoverareaX: function() {
                return this.hoverareaX;
            },
            gethoverareaY: function() {
                return this.gethoverareaY;
            }
        },
        keyboard: {
            activeclass: 'keyboard-active'
        },
        scrollbar: {
            scrollinterval: 10
        }
    };

    /*******************************************************
     * FUNCTIONALITY - GLOBAL COMPONENTS                   *
     *******************************************************/

    /*******************************************************
     * ACCORDION                                            *
     *******************************************************/

    function hide_element(target) {
        $(target).hide(0);
    }

    /**
     * Accordion functionality applied to menu item
     *
     * Structurally dependent, looks for the next element and checks if
     * it is hidden, then animates the hidden element to either hide or
     * show
     *
     * @param {String} targetClass
     * @param {String} targetsection
     * @param {Number} animSpeed
     */

    function accordion_menuheader(targetClass, targetsection, animSpeed) {
        $(targetClass).on({
            click: function() {
                if ($(this).next(targetsection).is(':hidden')) {
                    $(this).next(targetsection).slideDown(animSpeed);
                } else {
                    $(this).next(targetsection).slideUp(animSpeed);
                }
            }
        });
    }

    /**
     * Accordion functionality on li and nested li
     *
     * @param {String} targetClass
     * @param {String} elementCheck
     */

    function accordion_nestedlists(targetClass, elementCheck) {
        $(targetClass).on({
            click: function() {
                //prevents nested li from automatically closing
                if ($(this).parent().parent().is(elementCheck)) {
                    //redirect to url
                    window.location.replace($(this).children('a').attr('href'));
                    return false;
                } else {
                    if ($(this).has('ul') && $(this).children('ul').hasClass('hide')) {
                        $(this).children('ul').slideDown().removeClass('hide');
                    } else {
                        $(this).children('ul').slideUp().addClass('hide');
                    }
                }
            }
        });
    }

    /**
     *
     * @param {type} target
     * @param {type} targetclass
     * @param {type} toggleclass
     * @returns {undefined}
     */

    function show_hidden_next(target, targetclass, toggleclass) {
        $(target).on({
            click: function() {
                $(this).next(targetclass).toggleClass(toggleclass);
            }
        });
    }

    /**
     * some text
     * <br>
     * Uses 'closest', 'find', 'on', 'toggleClass' jquery methods
     * <br>
     * <b>Implementation specific</b> i.e. Depends on html structure
     *
     * @param {String} target The parent container highest within the dom
     * @param {String} targetclass The class,id,pseudo selector being targeted
     * @param {String} toggleclass The class to be toggled e.g. hide
     * @param {String} targetevent The event in which the toggle is based e.g. click, mouseover, ifClicked
     * @param {String} closestparent The parent container of element and related elements
     * @param {String} findclass The class,id or element in which the toggle class should act
     *
     */

    function show_hidden_find(target, targetclass, toggleclass, targetevent, closestparent, findclass) {

        $(target).on(targetevent, targetclass, function() {
            $(this).closest(closestparent).find(findclass).toggleClass(toggleclass);
        });
    }

    show_hidden_find('.nsearch', ':checkbox', 'hide', 'ifClicked', '.item-holder', '.item-input');

    /*******************************************************
     * DATEPICKER                                          *
     *******************************************************/

    /**
     *
     * @param {type} targetclass
     * @param {type} yearRange
     * @returns {undefined}
     */

    function datepicker_init(targetclass, yearRange) {
        if ($(targetclass).length > 0) {
            //get start year
            var fromYear = get_year_from(yearRange);
            //get current year
            var toYear = get_current_year();
            //default setup for datepickers
            $(targetclass).datepicker({
                buttonImageOnly: true,
                buttonImage: urlhandler.getbasedirectory() + 'img/icon-calendar.png',
                showOn: 'both', //focus, button, both
                changeYear: true,
                changeMonth: true,
                yearRange: fromYear + ":" + toYear,
                dateFormat: 'yy-mm-dd'
            });
        }
    }

    /**
     *
     * @param {type} targetClass
     * @param {type} yearRange
     * @param {type} datepicker
     * @returns {undefined}
     */

    function datepicker_custom(targetClass, yearRange, datepicker) {

    }


    /*******************************************************
     * CHECKBOX                                            *
     *******************************************************/


    /**
     * Gets the first checkbox in a table header and highlights all checkboxes <br>
     *
     * Code closely coupled to plugin
     * @param {type} targetclass
     * @param {type} parent
     * @returns {undefined}
     */

    function checkbox_selectall(targetclass, parent) {
        $(targetclass).on({
            ifChecked: function() {
                $(this).closest(parent).find(':checkbox').each(function() {
                    $(this).iCheck('check');
                });
            },
            ifUnchecked: function() {
                $(this).closest(parent).find(':checkbox').each(function() {
                    $(this).iCheck('uncheck');
                });
            }
        });
    }


    /**
     * Initialise plugin on all checkbox and radio buttons
     * @param {type} targetclass
     * @param {type} area
     */

    function checkbox_init(targetclass, area) {
        $(targetclass).iCheck({
            checkboxClass: 'icheckbox_square-grey',
            radioClass: 'iradio_square-grey',
            increaseArea: area
        });
    }


    /*******************************************************
     * MENU                                                *
     *******************************************************/
    //expand and collapse inner menu
    $('#toggle-menu').on({
        click: function() {
            if (layouthandler.innermenu.hasClass('expanded')) {
                $('#toggle-menu span').html('expand');
                layouthandler.innermenu.addClass('collapsed');
                layouthandler.innermenu.removeClass('expanded');
                layouthandler.innermenu.animate({
                    width: 0
                }, defaults.animspeed);
                $('#main-menu .left-arrow').css({
                    borderRightColor: menuarrow.colorCollapsed
                });
            } else {
                $('#toggle-menu span').html('collapse');
                layouthandler.innermenu.addClass('expanded');
                layouthandler.innermenu.removeClass('collapsed');
                layouthandler.innermenu.animate({
                    width: layouthandler.innermenu.data('width')
                }, defaults.animspeed);
                $('#main-menu .left-arrow').css({
                    borderRightColor: menuarrow.colorExpanded
                });
            }
            if (uihandler.screen.hoverAreaX > -1) {
                if (layouthandler.innermenu.hasClass('expanded')) {
                    uihandler.screen.hoverAreaX = uihandler.screen.sethoverareaX();
                } else {
                    hoverAreaX = layouthandler.outermenu.width();
                }
            }
        }
    });


    //Arrow used to highlight current section
    if (layouthandler.outermenu.length > 0) {
        //matches the id set in the sidebar
        $('#' + urlhandler.getcurrentpage()).prepend(menuarrow.html);
        //highlights current section
        $('#' + urlhandler.getcurrentpage()).addClass('menu-active');
        //set the colour of the arrow based on innermenu
        //i.e if it is collapsed
        if (layouthandler.innermenu.length === 0) {
            $('#main-menu .left-arrow').css({
                borderRightColor: menuarrow.colorCollapsed
            });
        }
    }

    /**
     * Based on the id of the button related content is shown <br>
     * Heavily dependent on the implementation of the html structure
     * @param {String} source The target id or class
     * @param {String} txtToSplit
     */

    function show_target(source, txtToSplit) {
        $(source).on({
            click: function() {
                $('#' + $(this).prop('id').split(txtToSplit)[0]).toggleClass('hide');
            }
        });
    }

    /**
     * clear all text inputs within the parent of the button clicked
     *
     * @param {String} trigger The id of the button being clicked
     */

    function input_clear(trigger) {
        $(trigger).on({
            click: function() {
                $(trigger).closest('.item-input').find('input[type="text"]').val('');
            }
        });

    }

    /**
     *
     * @param {String} target The target id or class
     * @param {String} childTarget get the first element
     * @param {String} check Check that the arrow is within a div or span e.t.c
     */

    function arrow_toggle(target, childTarget, check) {
        $(target).on({
            click: function() {
                var firstspan = $(this).children(childTarget).first();
                if ($(this).next().is(check)) {
                    $(this).next().toggleClass('hide');
                    firstspan.toggleClass('right-arrow').toggleClass('down-arrow');
                }
            }
        });
    }

    /**
     *
     * @param {type} target
     * @param {type} parent
     * @param {type} targetElement
     * @returns {undefined}
     */

    function text_get_html(target, parent, targetElement) {
        $(target).on({
            click: function() {
                //            console.log($(this).html());
                $(this).closest(parent).find(targetElement).val($(this).html());
            }
        });
    }

    $('#report-by').on('change', function(e) {
        if ($(this).prop('selectedIndex') > 0) {
            if ($('.reports_date').is(':hidden'))
                $('.reports_date').toggleClass('hide');
        } else if ($(this).prop('selectedIndex') === 0) {
            $('.reports_date').toggleClass('hide');
        }
    });

    text_get_html('.template-response', '.item-input', 'input[type="text"]');

    /*******************************************************
     * RESPONSE BOX                                        *
     *******************************************************/
    if ($('.response-attributes').length > 0) {
        $('#ccBtn,#bccBtn').on({
            click: function() {
                $this = $(this);
                var selected = "" + $(this).html().split(/\s/)[1];
                var lcSelected = selected.toLowerCase();
                if ($('#' + lcSelected + '-field').hasClass('hide')) {
                    $this.html("Clear " + selected);
                } else {
                    $this.html("Add " + selected);
                }
                $('#' + lcSelected + '-field').toggleClass('hide');
            }
        });


        arrow_toggle('#template div', 'span', 'ul');
    }

    accordion_menuheader('.menu-header', '.menu-section', 100);
    accordion_nestedlists('#inner-menu li', 'li');
    checkbox_selectall('table th input:checkbox', 'table');
    checkbox_init('input', '10%');
    datepicker_init('.datepicker', 10);
    input_clear('#clearBtn');
    show_target('#templateBtn,#personalTemplateBtn,#attachFilesBtn', 'Btn');



    /*******************************************************
     * CONDITIONALLY LOAD FUNCTIONALITY BASED ON URL       *
     *******************************************************/
    /*******************************************************
     * LOGIN/LOGOUT                                        *
     *******************************************************/
    if (urlhandler.getactivepage().search("login") > -1 || urlhandler.getactivepage().search("logout") > -1) {
        /*
         * Primarily targeted at the login page.
         *
         * {event} focus removes the default form value
         * {event} keypress checks if an error is on the page and removes the element
         */
        $('input[type="text"],input[type="password"]').on({
            focus: function() {
                if (!$(this).is('[readonly]')) {
                    if ($(this).val() === $(this).attr('value') && !$(this).hasClass('datepicker')) {
                        $(this).val('');
                    }
                }
            },
            keypress: function() {
                if ($(this).next('div').hasClass('form-error')) {
                    $(this).next('div').remove();
                }
            }
        });
        //set the disabled input field as system name
        set_element_val('form input:first', get_system_name(urlhandler.getcurrenturl()));
        var loginContainer = $('#login-container');
        //set the position of the container to the center of the screen
        loginContainer.css({
            top: $(document).height() / 2 - loginContainer.height() / 2
        });
        //Fix container to center of screen
        $(window).on({
            resize: function() {
                loginContainer.css({
                    top: $(document).height() / 2 - loginContainer.height() / 2
                });
            }
        });
    }
    /*******************************************************
     * TICKETS                                             *
     *******************************************************/
    else if (urlhandler.getcurrentpage().search("ticket") > -1) {
        /*******************************************************
         * TICKET RESPONSE                                     *
         *******************************************************/
        if (urlhandler.getactivepage().search("nticket") > -1) {
            //show content
            $('#last-ticket .ticket-content').addClass('show');
            //apply accordion to ticket header
            accordion_global('.ticket-header', 10, 'div');
            //apply accordion to ticket note
            accordion_global('.ticket-note-header', 0, 'div');
            /**
             * area of detection for smartbar on y-axis
             * {Number}
             */
            var hoverAreaY = $(window).height() - layouthandler.footer.height() - components.smartbar.height() * 2;
            /**
             * area of detection for smartbar on x-axis
             */
            var hoverAreaX = layouthandler.outermenu.width() + layouthandler.innermenu.width();
            $('#expand-all-tickets').on({
                click: function() {
                    //get all tickets and notes
                    var allContent = $('.ticket-content,.ticket-note-content').not(':last');
                    //get vertical bar for this element
                    var vertical = $(this).find('.rect-vertical');
                    //hide all if the vertical bar is hidden
                    if (vertical.hasClass('hide')) {
                        allContent.slideUp();
                        vertical.addClass('display').removeClass('hide');
                    } else {
                        allContent.slideDown();
                        vertical.addClass('hide').removeClass('display');
                    }
                }
            });
            $('#scroll-to-top').on({
                click: function() {
                    layouthandler.content.mCustomScrollbar("scrollTo", 'top');
                }
            });
            $('#scroll-to-response').on({
                click: function() {
                    layouthandler.content.mCustomScrollbar("scrollTo", '.ticket-response');
                }
            });
            $('#scroll-to-open-ticket').on({
                click: function() {
                    //get last element relative to content within content container
                    if ($('#last-ticket .ticket-content').is(':hidden')) {
                        $('#last-ticket .ticket-content').slideDown();
                        layouthandler.content.mCustomScrollbar("scrollTo", "#last-ticket");
                    } else {
                        layouthandler.content.mCustomScrollbar("scrollTo", "#last-ticket");
                    }
                }
            });
            $('.menu-section #edit').on({
                click: function() {
                    $('#ticket_details').toggleClass('hide');
                    $('#edit-form').toggleClass('hide');
                }
            });
            $('.menu-section #cancel').on({
                click: function() {
                    $('#ticket_details').toggleClass('hide');
                    $('#edit-form').toggleClass('hide');
                }
            });
            $('.menu-section #save').on({});
            /**
             *
             */
            $(window).on({
                mousemove: function(e) {
                    var hide = 0;
                    var show = 38;
                    if (e.pageX > hoverAreaX) {
                        if (e.pageY < hoverAreaY) {
                            if (components.smartbar.hasClass('hovered')) {
                                components.smartbar.animate({
                                    bottom: hide
                                });
                                components.smartbar.removeClass('hovered');
                            }
                        } else if (e.pageY > hoverAreaY) {
                            if (!components.smartbar.hasClass('hovered')) {
                                components.smartbar.animate({
                                    bottom: show
                                });
                                components.smartbar.addClass('hovered');
                            }
                        }
                    } else if (e.pageX < hoverAreaX) {
                        if (components.smartbar.hasClass('hovered')) {
                            components.smartbar.animate({
                                bottom: hide
                            });
                            components.smartbar.removeClass('hovered');
                        }
                    }
                }
            });
        }
        /*******************************************************
         * TICKET LIST                                         *
         *******************************************************/
        else if (urlhandler.getactivepage().search("ticket_list") > -1) {}
    }
    /*******************************************************
     * REPORTS                                             *
     *******************************************************/
    else if (urlhandler.getactivepage().search("nreport") > -1) {
        var reportshandler = new ReportsHandler();
        //drop down menu for reporting options section
        $('#period-selection').on({
            change: function() {
                //hide all elements
                $(this).parent().children('div').addClass('hide');
                //get text
                var string = new String($(this).val()).toLowerCase();
                //show time period
                $('#' + string).removeClass('hide');
            }
        });
        $('#report-by').on({
            change: function() {
                //get the selected text within the report by area
                var text = new String($(this).val()).toLowerCase();
                //array for reporthandler
                var selectArray = [];
                //check if text within select box has any spaces
                if (text.search(' ') > -1) {
                    //remove spaces from selection from select box
                    var textArray = text.split(' ');
                    //setup key to access values from reporthandler filter
                    var newString = textArray.join('');
                    //get array from reporthandler
                    selectArray = reportshandler.getFilter(newString);
                } else {
                    //get array from reporthandler
                    selectArray = reportshandler.getFilter(text);
                }
                //output container
                var html = "";
                html += "<option>" + selectArray.join('</option><option>') + '</option>';
                //remove current content
                $('#info-to-display').empty();
                //add new output
                $('#info-to-display').append(html);
            }
        });
        $('#info-to-display').on({
            change: function() {}
        });
    }
    /*******************************************************
     * SETTINGS                                            *
     *******************************************************/
    else if (urlhandler.getcurrentpage().search("settings") > -1) {
        //apply accordion to ticket header
        accordion_global('.ticket-header', 10, 'div');
        accordion_global('li ul', 10, 'li');
    }


    $ticketheader = $('#ticket-filter .menu-header');
    $ticketmenu = $('#ticket-filter .menu-section');
    $ticketheader.on({
        mouseover: function() {
            if (!$ticketheader.hasClass('hovered'))
                $(this).addClass('hovered');
        },
        mouseleave: function() {
            $ticketheader.removeClass('hovered');
            //reset when the user leaves the area again
            var timeout = false;
            timeout = setTimeout(function() {
                if (!$ticketheader.hasClass('hovered') && !$ticketmenu.hasClass('hovered')) {
                    $ticketmenu.slideUp();
                }
            }, 3000);
        }
    });
    $ticketmenu.on({
        mouseenter: function() {
            //track mouse hover
            $ticketmenu.addClass('hovered');
            //waiting is added when mouse leaves the hover area
            //there is a 3 second delay
            //if the user moves back into the section to change the settings
            //remove the waiting class
            if ($ticketmenu.hasClass('waiting')) {
                $ticketmenu.removeClass('waiting');
            }
        },
        mouseleave: function() {
            $ticketmenu.addClass('waiting');
            $ticketmenu.removeClass('hovered');
            //reset when the user leaves the area again
            var timeout = false;
            timeout = setTimeout(function() {
                if (!$ticketheader.hasClass('hovered') && !$ticketmenu.hasClass('hovered')) {
                    $ticketmenu.slideUp();
                }
            }, 3000);
        }
    });

    $('#previous-page').on({
        click: function() {
            history.back();
        }
    });

    $('.ticket-content').on('click', '.remove-row', function() {
        if ($('.remove-row').length > 1) {
            if ($(this).closest('.item-holder').index() !== 0) {
                $(this).closest('.item-holder').remove();
            }
        }
    });

    if ($('.add-filter').length > 0) {
        var addFilterContainer = $('.add-filter').first();
    }

    $('.new-condition').on({
        click: function() {

            //reference to btn
            $this = $(this);

            //clone first empty condition
            var itemToAdd = addFilterContainer.clone();

            //append clone before
            $this.closest('.item-holder').before(itemToAdd);

        }
    });

    var emailToAdd = $('.add-email .item-holder').first().clone();

    $('#add-email').on({
        click: function() {

            if ($(this).prev('.add-email').is(':hidden')) {
                $(this).prev('.add-email').removeClass('hide');
            } else {
                $('.add-email .item-holder').last().after(emailToAdd.clone());
            }

        }
    });

    $('.add-email').on('click', '.remove-email', function() {
        if ($('.remove-email').length > 1) {
            $(this).closest('.item-holder').remove();
        } else {
            $('.add-email').toggleClass('hide');
        }
    });



    $('#add-new-template').on({
        click: function() {
            //get button clicked
            $this = $(this);
            //get hidden content
            var firstContent = $('.content-holder').first();
            //get firstcontent hidden content
            var contentForm = firstContent.find('.ticket-content').first();
            var isHidden;
            if (firstContent.is(':hidden')) {
                isHidden = true;
            } else {
                isHidden = false;
            }
            if (isHidden) {
                firstContent.show(0);
                contentForm.show(0);
                $this.prop('value', 'cancel').toggleClass('delete').toggleClass('save');
            } else {
                $this.prop('value', 'Add New Template').toggleClass('delete').toggleClass('save');
                firstContent.hide(0);

            }

            $(this).next('.footer-btn').toggleClass('hide');
            //
        }
    });


    show_hidden_next('.help', '.help-info', 'hide');

    function input_text_to_input(trigger) {
        $(trigger).on({
            click: function() {

            }
        });
    }

    /**
     * There are some elements that need to load after everything is initialised
     *
     * Layout: The menu and content height is dynamically set on load and window resize.
     * Scrollbar: This will need to be initialised after the content is populated
     *
     */
    $(window).on({
        load: function() {
            // 1. Set height of menu and content
            layouthandler.innermenu.height(set_content_height());
            layouthandler.content.height(set_content_height());
            // 2. Set Scrollbar now that page loaded and content height set
            //horizontalScroll: true
            $('#inner-menu').mCustomScrollbar({
                advanced: {
                    updateOnContentResize: true,
                    updateOnBrowserResize: true
                },
                scrollButtons: {
                    enable: false
                },
                scrollInertia: 0
            });
            $('#content').mCustomScrollbar({
                advanced: {
                    updateOnContentResize: true,
                    updateOnBrowserResize: true
                },
                scrollButtons: {
                    enable: false
                },
                scrollInertia: 0
            });

            // Conditionally load actions based on urlhandler
            //scroll to last ticket once layout set
            if (urlhandler.getactivepage().search('nticket') > -1) {
                layouthandler.content.mCustomScrollbar("scrollTo", '#last-ticket');
            }
            /**
             * Initialise tinymce
             */
            tinyMCE.init({
                setup: function(ed) {
                    ed.on('keyup', function(ed, e) {
                        //applicable to nticket page
                        if (urlhandler.getactivepage().search('nticket') > -1) {
                            //if any content create array split on html tags and check length
                            //usually if greater than 1 there is some content
                            if (this.getContent().split(/[<][/]?[a-zA-z]+[>]/).length > 1) {
                                $('#ticket-display-textarea-keypress').removeClass('hide').addClass('show');
                                $('#ticket-display-default').removeClass('show').addClass('hide');
                            } else {
                                $('#ticket-display-textarea-keypress').removeClass('show').addClass('hide');
                                $('#ticket-display-default').removeClass('hide').addClass('show');
                            }
                        }
                    });
                },
                height: 300,
                browser_spellcheck: true,
                selector: "textarea#email-reply,textarea#user_signature_body",
                toolbar1: "formatselect fontselect fontsizeselect | bold italic underline strikethrough |  \n\
                           alignleft aligncenter alignright alignjustify | cut copy paste",
                toolbar2: "bullist numlist | outdent indent | undo redo |  removeformat | link unlink | forecolor backcolor | searchreplace fullscreen | ",
                plugins: ["paste wordcount searchreplace fullscreen print link table tabfocus textcolor ",
                    "image media insertdatetime table contextmenu",
                    "advlist autolink lists charmap preview anchor"
                ],
                paste_as_text: true,
                menubar: "file edit insert view format table"
            });



            var scrollPos = 0;
            $('#content, #inner-menu').on({
                mouseover: function() {
                    $(this).addClass(uihandler.keyboard.activeclass);
                },
                mouseleave: function() {
                    $(this).removeClass(uihandler.keyboard.activeclass);
                }
            });
            //enable keyboard scrolling based on active area
            $(document).keydown(function(e) {

                //add logic to check for existence of mcsbdragger
                if ($(this).has('.' + uihandler.keyboard.activeclass + ' .mCSB_dragger')) {
                    var draggerOffset = $('.' + uihandler.keyboard.activeclass + ' .mCSB_dragger').position().top;
                    var draggerHeight = $('.' + uihandler.keyboard.activeclass + ' .mCSB_dragger').height();
                    var scrollContainer = $('.' + uihandler.keyboard.activeclass + ' .mCSB_draggerContainer').height();
                    scrollPos = draggerOffset;
                    //down
                    if (e.keyCode === 40) {
                        if (draggerOffset + draggerHeight < scrollContainer) {
                            scrollPos = scrollPos + uihandler.scrollbar.scrollinterval;
                            $('.' + uihandler.keyboard.activeclass).mCustomScrollbar("scrollTo", scrollPos, {
                                moveDragger: true
                            });
                        }
                        //up
                    } else if (e.keyCode === 38) {
                        if (draggerOffset >= 0) {
                            scrollPos = scrollPos - uihandler.scrollbar.scrollinterval;
                            $('.' + uihandler.keyboard.activeclass).mCustomScrollbar("scrollTo", scrollPos, {
                                moveDragger: true
                            });
                        }
                    }
                }
            });
        },
        resize: function() {
            layouthandler.innermenu.height(set_content_height());
            layouthandler.content.height(set_content_height());
        }
    });
    /*******************************************************
     * UI                                                  *
     *******************************************************/
    /**
     * Sets the height of element based window, searchbar and layouthandler.footer height
     * @returns {Number}
     */

    function set_content_height() {
        return $(window).height() - layouthandler.footer.height() - layouthandler.searchbar.height();
    }
    /**
     *
     * Apply accordion functionality to elements
     *
     * Focuses on applying the accordion to the next element <br>
     * This is wholly dependent on the html structure
     *
     * @param {String} target The id or class to target
     * @param {Number} animTime The time in milliseconds to animate
     * @param {String} targetnext The next type of element to animate e.g div, span e.t.c
     */

    function accordion_global(target, animTime, targetnext) {
        $('' + target).on({
            click: function() {
                if ($(this).next(targetnext).is(':hidden')) {
                    $(this).next(targetnext).slideDown(animTime);
                } else {
                    $(this).next(targetnext).slideUp(animTime);
                }


            }
        });
    }
    /**
     * Handles nested lists
     *
     * @param {String} target the target element to apply accordion
     * @param {String} classToCheck checks if class is applied to nested ul
     */

    function nested_accordion_handler(target, classToCheck) {
        $(target).on({
            click: function(e) {
                e.preventDefault();
                //all nested elements must have nested-default
                //since all load collapsed by default this is
                //simple to manage
                if ($(this).parent().hasClass(classToCheck)) {
                    return false;
                } else {
                    //ensures parent has nested ul and checks if class applied to child ul
                    if ($(this).has('ul') && !$(this).children('ul').hasClass('hide')) {
                        $(this).children('ul').addClass('hide').slideDown();
                    } else if ($(this).has('ul') && $(this).children('ul').hasClass('hide')) {
                        $(this).children('ul').removeClass('hide').slideUp();
                    }
                }
            }
        });
    }
    /**********************************************
     * Form  functionality                         *
     **********************************************/
    /**********************************************
     * Form  Validation                           *
     **********************************************/
    //form submissions
    $('#form-login').submit(function(e) {
        //prevent form submission
        e.preventDefault();
        //validate username and password fields
        field_validation($('#username'), 'username', 'value');
        field_validation($('#password'), 'password', 'value');
        //check if user wants username remembered
        if ($('#login-checkbox').attr('checked')) {}
        //post form through ajax
        //system will auto redirect to url or return error in html
        //        $.ajax({
        //            type: 'post',
        //            url: someurl,
        //            dataType: 'html'
        //            data: $(this).serialize(),
        //            success: function(data) {
        //
        //            },
        //            error: function(data) {
        //
        //            }
        //        });
    });
    /**********************************************
     * Error handler methods                      *
     **********************************************/
    /**
     * @description Uses the class form-error <br />
     *
     * Normally integrated with a form to handle errors
     *
     * @param {String} message The error message for user
     * @param {String} addClass Add classes to track error messages
     * @param {String} addId Adds an id to div
     * @returns {String}
     */

    function error_handler(message, addClass, addId) {
        return '<div id="' + addId + '" class="form-error ' + addClass + '"><span class="error-arrow"></span>' + message + '</div>';
    }
    /**********************************************
     * Form methods                               *
     **********************************************/
    /**
     * Validates a form element
     *
     * @param {object} formelement jquery object
     * @param {string} keyword used in conjunction with string for 'Please enter your' + keyword
     * @param {string} checkAgainst Check the value of the form element against an attribute e.g. id, value, class
     */

    function field_validation(formelement, keyword, checkAgainst) {
        if (formelement.val() === '' || formelement.val() === formelement.attr('' + checkAgainst)) {
            if (!formelement.next().hasClass('form-error')) {
                formelement.after(error_handler('Please enter your ' + keyword, '', keyword));
            }
        }
    }
    /**
     * Set the value for a form element e.g. input, textarea, select e.t.c
     *
     * @param {String} id The input field to target
     * @param {String} value the string to be set
     */

    function set_element_val(id, value) {
        $(id).val(value);
    }
});
/**
 * Get the current year
 *
 * @returns {String} The date
 */

function get_current_year() {
    return new Date().getFullYear();
}
/**
 * Get the start year
 *
 * @param {Number} years The numebr of years to subtract
 * @returns {Number}
 */

function get_year_from(years) {
    return get_current_year() - years;
}
/**
 * Takes the current href / url and returns the system name
 *
 * @param {String} currenturl The login url
 * @returns {String} The users system name
 */

function get_system_name(currenturl) {
    //only get first four backslashes
    var urlArray = currenturl.split('\/', 4);
    //return the last element in the array
    //this will typically be the system name
    return urlArray[urlArray.length - 1];
}
/**
 * Some reference notes for development choices
 *
 * string.search is used instead of string.indexOf for performance
 * more tests can be found here and comparisons are searchable
 * e.g http://jsperf.com/string-compare-perf-test
 * http://jsperf.com/regexp-test-search-vs-indexof/29
 *
 * Interestingly almost all elements created are a type of object
 * Most objects in javascript are implemented as hashtables making them fast
 * http://javascript.crockford.com/private.html
 */

//This is instead would be
