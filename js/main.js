require.config({
    baseUrl: 'js/',
    paths: {
        backbone: ['vendor/backbone'],
        buckets: ['vendor/buckets-minified'],
        chartjs: ['vendor/Chart'],
        datatables: ['vendor/datatables'],
        fullcalendar: ['vendor/fullcalendar.min'],
        icheck: ['vendor/jquery.icheck.min'],
        jquery: ['vendor/jquery.min', 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min'],
        jqueryui: ['vendor/jquery.ui', 'http://code.jquery.com/ui/1.10.3/jquery-ui'],
        json2: ['vendor/json2'],
        mcustomscrollbar: ['vendor/jquery.mCustomScrollbar'],
        moment: ['vendor/moment.min'],
        mousewheel: ['vendor/jquery.mousewheel'],
        persistjs: ['vendor/persist-min'],
        qunit: ['vendor/qunit', 'http://code.jquery.com/qunit/qunit-1.13.0'],
        scrollto: ['vendor/jquery.scrollTo.min'],
        tinyMCE: ['vendor/tinymce/tinymce.min', 'http://tinymce.cachefly.net/4.0/tinymce.min'],
        tinymceplugin: ['vendor/tinymce/plugins/table/plugin.min'],
        tinymcetheme: ['vendor/tinymce/themes/modern/theme.min'],
        underscore: ['vendor/underscore']
    },
    shim: {
        "backbone": {
            exports: 'backbone'
        },
        "fullcalendar": ['jquery'],
        "icheck": {
            deps: ['jquery'],
            exports: 'iCheck'
        },
        "jqueryui": ['jquery'],
        "mcustomscrollbar": {
            deps: ['jquery'],
            exports: 'mCustomScrollbar'
        },
        "moment": {
            exports: 'moment'
        },
        "mousewheel": ['jquery'],
        "scrollto": ['jquery'],
        "tinymceplugin": ['tinyMCE'],
        "tinymcetheme": ['tinyMCE'],
        "underscore": {
            exports: '_'
        },
        "tinyMCE": {
            exports: "tinyMCE",
            init: function() {
                //this must be set in order to work <ie9
                this.tinyMCE.DOM.events.domLoaded = true;
                return this.tinyMCE;
            }
        }
    }
    //set up for cache busting - if on chrome, use inspect element and hard reset
    // ,urlArgs: "bust=" + Math.random()
});

//main entry into application
require(['app/Global']);