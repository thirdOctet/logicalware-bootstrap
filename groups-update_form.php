<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <!-- SET MESSAGE SOURCE -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">USER GROUP</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Edit user group</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="span18">
                                <div class="item-holder">
                                    <div class="span6 item-name">User Group Name:</div>
                                    <div class="span18">
                                        <span>Billing &amp; Invoicing</span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Assigned Users:</div>
                                    <div class="span18">
                                        <div class="item-input margin-bottom-5px">
                                            <input type="checkbox" /> <span>igor</span>
                                        </div>
                                        <div class="item-input margin-bottom-5px">
                                            <input type="checkbox" /> <span>logicalware</span>
                                        </div>
                                        <div class="item-input margin-bottom-5px">
                                            <input type="checkbox" /> <span>spamCollector</span>
                                        </div>
                                        <div class="item-input margin-bottom-5px">
                                            <input type="checkbox" checked="true" /> <span>david</span>
                                        </div>
                                        <div class="item-input margin-bottom-5px">
                                            <input type="checkbox" /> <span>will</span>
                                        </div>
                                        <div class="item-input">
                                            <input type="checkbox" /> <span>galtsev</span>
                                        </div>
                                        <div class="item-input">
                                            <input type="checkbox" checked="true"/> <span>jonathan</span>
                                        </div>
                                        <div class="item-input">
                                            <input type="checkbox" /> <span>timets</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<?php

$btn_array = array();

array_push($btn_array, get_input_button("footer-btn save", "save", "Save"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>