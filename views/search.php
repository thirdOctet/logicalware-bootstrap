<div id="searchbar" class="">
    <div id="menucontrol" class="iconcontainer iconwh iconcolor">
        <i class="fa fa-bars"></i>
    </div>
    <div id="search-form" class="">
        <form accept-charset="utf-8" action="" method="post" id="getTicket">
            <input id="ticket_id" type="text" name="ticket_id" accesskey="g" value="" placeholder="Get Ticket..." class="null-margin-top null-margin-bottom">
            <button type="submit" class="">
                <i class="fa fa-search"></i>
            </button>
        </form>
    </div>
    <div id="notificationshow" class="notifications iconcontainer pull-right iconwh iconcolor hide is_admin">
        <i class="fa fa-bell"></i>
        <span class="badge"></span>
    </div>
    <div id="notificationholder" class="pull-right iconcontainer notifications margin-right hide">
        <span class="notifytext">I am some text ok</span>
    </div>
    <div class="clearfix"></div>
    <div id="notificationmain" class="default-shadow hide">
        <div class="nmcontainer">
            <div class="notifycount">You have <span class="notifynumber"></span> notifications</div>
            <table class="nmtable">
                <tbody>
                    <tr class="nmwarning">
                        <td>
                            <i class="fa fa-exclamation-triangle warning notifyicon"></i>
                        </td>
                        <td>Lorem ipsum dolor sit amet.</td>
                        <td><i class="fa fa-times nmclose"></i></td>
                    </tr>
                    <tr class="nmdanger">
                        <td>
                            <i class="fa fa-minus-circle danger notifyicon"></i>
                        </td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</td>
                        <td><i class="fa fa-times nmclose"></i></td>
                    </tr>
                    <tr class="nmsuccess">
                        <td>
                            <i class="fa fa-check-circle success notifyicon"></i>
                        </td>
                        <td>Lorem ipsum dolor sit amet</td>
                        <td><i class="fa fa-times nmclose"></i></td>
                    </tr>
                    <tr class="nminfo">
                        <td>
                            <i class="fa fa-info information notifyicon"></i>
                        </td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</td>
                        <td><i class="fa fa-times nmclose"></i></td>
                    </tr>
                </tbody>
            </table>
            <div class="notifycount notifybase"></div>
        </div>
    </div>
</div>