<?php
try {
    $dir = dirname(__DIR__);
    include($dir . '/scripts/html-helper.php');
} catch (Exception $exc) {
    die($exc->getTraceAsString());
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Site title Here</title>
        <meta name="viewport" content="width=device-width, initial-scale=0.75">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <?php
        if (strpos($_SERVER['HTTP_HOST'], 'localhost') !== false) {
            echo '<base href="http://localhost/logicalware-bootstrap/" />';
        } else {
            echo '<base href="http://' . $_SERVER['SERVER_NAME'] . '/lw-frontend-dev/" />';
        }
        ?>

         <link rel="stylesheet" type="text/css" href="css/logicalware.css"/>
        <!-- <link rel="stylesheet" type="text/css" href="css/logicalware-alt.css"/> -->
        <link rel="stylesheet" type="text/css" href="css/vendor/bootstrap.min.css">
        <!--<link rel="stylesheet" type="text/css" href="css/vendor/bootstrap-responsive.min.css"/>-->

        <link rel="stylesheet" type="text/css" href="js/vendor/tinymce/skins/lightgray/skin.min.css" />
        <link rel="stylesheet" type="text/css" href="css/vendor/jquery.mCustomScrollbar.css" />
        <link rel="stylesheet" type="text/css" href="css/vendor/square/grey.css" />
        <!-- <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"> -->
        <link href="css/vendor/fonts/fontawesome.css" rel="stylesheet" type="text/css"/>
        <link href="css/vendor/qunitjs.css" rel="stylesheet" type="text/css"/>
        <link href="css/vendor/fullcalendar.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <!--[if lt IE 8]>
            <link rel="stylesheet" type="text/css" href="css/ie.css"/>
        <![endif]-->
        <!-- IE6-8 support of HTML5 Elements -->
        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>

        <![endif]-->
        <!-- <script src="js/vendor/html5.js" type="text/javascript"></script> -->
    </head>
    <body>