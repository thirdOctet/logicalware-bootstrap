<div id="footer">
    <div id="footeralignment">
        <form id="">
            <div id="">
                <?php
                $btn_array = array();
                array_push($btn_array, get_input_button("footer-btn cancel", "cancel", "Cancel"));
                array_push($btn_array, get_input_button("footer-btn save-note", "save-note", "Save as Note"));
                array_push($btn_array, get_input_button("footer-btn save-draft", "save-draft", "Save as Draft"));
                array_push($btn_array, get_input_button("footer-btn respond", "respond", "Respond"));
                array_push($btn_array, get_input_button("footer-btn respond-close", "respond-close", "Respond & Close"));
                ?>


            </div>
        </form>
    </div>
</div>

