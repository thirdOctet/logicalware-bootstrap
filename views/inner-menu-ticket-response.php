<div id="innermenu" class="expanded">
    <div class="default-padding">
        <?php /*<div class="content-holder text-center">
            <div class="ticket-helper">
                <div id="previous">
                    <span class="ticket-left-arrow"></span>
                </div>
            </div>
            <!-- divider -->
            <span class="ui-split-tickets">
                <div class="ui-split-circle"></div>
                <div class="ui-split-circle"></div>
                <div class="ui-split-circle"></div>
                <div class="ui-split-circle"></div>
                <div class="ui-split-circle"></div>
                <div class="ui-split-circle"></div>
            </span>

            <!-- preview/list button -->
            <span id="switch-view">
                <div class="ticket-helper">
                    <div class="list-helper">
                        <div class="rect-horizontal-list"></div>
                        <div class="rect-horizontal-list"></div>
                        <div class="rect-horizontal-list"></div>
                    </div>
                </div>
            </span>

            <!-- divider -->
            <span class="ui-split-tickets">
                <div class="ui-split-circle"></div>
                <div class="ui-split-circle"></div>
                <div class="ui-split-circle"></div>
                <div class="ui-split-circle"></div>
                <div class="ui-split-circle"></div>
                <div class="ui-split-circle"></div>
            </span>
            <div class="ticket-helper">
                <div id="next">
                    <span class="ticket-right-arrow"></span>
                </div>
            </div>
        </div>
        */ ?>
        <p id="" class="menu-header null-margin-top">Ticket Details</p>
        <div class="menu-section">
            <!-- <div id="ticket_details" class="inner-menu-content">
                <div class="row-fluid">
                    <div class="item-holder">
                        <span class="item-name span11">Ticket #:</span>
                        <span class="item-input span13">012345</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="item-holder">
                        <span class="item-name span11">Account:</span>
                        <span class="item-input span13">email@something.com</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="item-holder">
                        <span class="item-name span11">Date Created:</span>
                        <span class="item-input span13">18/04/2013</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="item-holder">
                        <span class="item-name span11">Date Last Opened:</span>
                        <span class="item-input span13">19/04/2013</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="item-holder">
                        <span class="item-name span11">Subject:</span>
                        <span class="item-input span13">Some subject</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="item-holder">
                        <span class="item-name span11">Assigned:</span>
                        <span class="item-input span13">Jonathan</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="item-holder">
                        <span class="item-name span11">Status:</span>
                        <span class="item-input span13">Open</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="item-holder">
                        <span class="item-name span11">Priority:</span>
                        <span class="item-input span13">Critical</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="item-holder">
                        <span class="item-name span11">Tags:</span>
                        <span class="item-input span13">tag 1</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="item-holder">
                        <span class="item-name span11 align-top">Description:</span>
                        <span class="item-input span13">Some text with a description</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="item-holder null-margin-bottom">
                        <span class="item-input span13">
                            <?php echo get_input_button("default-btn full-width edit", "edit", "Edit"); ?>
                        </span>
                    </div>
                </div>
            </div> -->

            <div id="edit-form" class="inner-menu-content">
                <div class="row-fluid">
                    <form id="tickets" accept-charset="utf-8">

                        <!-- ticket# -->
                        <div class="item-holder">
                            <div class="item-name span11">Ticket #:</div>
                            <div class="item-input span13">012345</div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- account -->
                        <div class="item-holder">
                            <div class="item-name span11">Account:</div>
                            <div class="item-input span13">email@something.com</div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- Date Created -->
                        <div class="item-holder">
                            <div class="item-name span11">Date Created:</div>
                            <div class="item-input span13">18/04/2013</div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- Date Last Opened -->
                        <div class="item-holder">
                            <div class="item-name span11">Date Last Opened:</div>
                            <div class="item-input span13">19/04/2013</div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- subject -->
                        <div class="item-holder">
                            <div class="item-name span11">Subject:</div>
                            <div class="item-input span13">
                                <input type="text" value="Some subject"/>
                            </div>
                        </div>

                        <!-- assigned -->
                        <div class="item-holder">
                            <div class="item-name span11">Assigned:</div>
                            <div class="item-input span13">
                                <select>
                                    <option>Igor</option>
                                    <option>Jonathan</option>
                                    <option>David</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- status -->
                        <div class="item-holder">
                            <div class="item-name span11">Status:</div>
                            <div class="item-input span13">
                                <select name="state">
                                    <option value="Open" selected="">Open</option>
                                    <option value="Hold">Hold</option>
                                    <option value="Closed">Closed</option>
                                    <option value="Spam">Spam</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- priority -->
                        <div class="item-holder">
                            <div class="item-name span11">Priority:</div>
                            <div class="item-input span13">
                                <select name="priority">
                                    <option value="5">Critical</option>
                                    <option value="4">High</option>
                                    <option value="3" selected="">Normal</option>
                                    <option value="2">Low</option>
                                    <option value="1">Junk</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- Category 0 -->
                        <div class="item-holder">
                            <div class="item-name span11">Category0:</div>
                            <div class="item-input span13">
                                <select name="category0">
                                    <option value="-">-</option>
                                    <option value="Billing">Billing</option>
                                    <option value="Billing &amp; Invoicing">Billing &amp; Invoicing</option>
                                    <option value="Free Trial">Free Trial</option>
                                    <option value="Free Trial: Prospect Enquiries">Free Trial: Prospect Enquiries</option>
                                    <option value="Free Trial: Website Notification">Free Trial: Website Notification</option>
                                    <option value="Ignore">Ignore</option>
                                    <option value="Internal: Instance deletion">Internal: Instance deletion</option>
                                    <option value="Internal: Test">Internal: Test</option>
                                    <option value="Internal: User deletion" selected="">Internal: User deletion</option>
                                    <option value="Junk/Spam">Junk/Spam</option>
                                    <option value="Sales &amp; Enquiries">Sales &amp; Enquiries</option>
                                    <option value="Support">Support</option>
                                    <option value="Zuzana">Zuzana</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- Category1 -->
                        <div class="item-holder">
                            <div class="item-name span11">Category1:</div>
                            <div class="item-input span13">
                                <select name="category1">
                                    <option value=""></option>
                                    <option value="-">-</option>
                                    <option value="1: Acknowledged">1: Acknowledged</option>
                                    <option value="2: In Process">2: In Process</option>
                                    <option value="3: Complete">3: Complete</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- tags -->
                        <div class="item-holder">
                            <div class="item-name span11">Tags:</div>
                            <div class="item-input span13">
                                <select multiple="true">
                                    <option>tag 1</option>
                                    <option>tag 2</option>
                                    <option>tag 3</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- description -->
                        <div class="item-holder">
                            <div class="item-name span11">Description:</div>
                            <div class="item-input span13">
                                <textarea class="null-margin-bottom">Some text with a description</textarea>
                            </div>
                        </div>

                        <!-- cancel save -->
                        <!-- <div class="item-holder null-margin-bottom row-fluid">
                            <div class="item-input margin-bottom-5px div12">
                                <?php echo get_input_button("default-btn full-width save", "save", "Save"); ?>
                            </div>
                            <div class="item-input span13">
                                <?php echo get_input_button("default-btn full-width cancel", "cancel", "Cancel"); ?>
                            </div>
                        </div> -->

                    </form>
                </div>
            </div>
        </div>
        <p id="" class="menu-header">Ticket Options</p>
        <!--<span class="arrow-up"></span>-->
        <div class="menu-section hide">
            <ul class="">
                <li>Show headers</li>
                <li>Show html</li>
                <li>Export</li>
                <li>Forward parent message</li>
                <li>Print</li>
            </ul>
        </div>

        <p id="" class="menu-header">Related Open Tickets</p>
        <div class="menu-section hide">
            <ul class="">
                <li>Show headers</li>
                <li>Show html</li>
                <li>Export</li>
            </ul>
        </div>

        <p id="" class="menu-header">Ticket History</p>
        <div class="menu-section hide null-padding">
            <div class="ticket-history">
                <div class="history-row">
                    <span class="title">Event</span>
                    <span class="description">AutoReply</span>
                </div>
            </div>
        </div>
    </div>
</div>