<div id="innermenu" class="expanded">
    <div class="default-padding">
        <p class="menu-header">Options</p>
        <div class="menu-section">
            <ul>
                <li>
                    <a href="nreports">Reports</a></li>
                    <li class="rbuilder">Report Builder<i class="fa fa-plus pull-right"></i>
                        <ul class="hide">
                            <li><a href="rbuilder-list">List Reports</a></li>
                            <li><a href="rbuilder-add_form">Create/Add New Report</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

            <p id="" class="menu-header">Report Options</p>
            <div class="menu-section">
                <select id="report-by" class="null-margin-bottom">
                    <option>Snapshot</option>
                    <option>Performance</option>
                </select>
            </div>
            <p class="menu-header">Information To Display</p>
            <div class="menu-section">
                <select id="info-to-display" class="null-margin-bottom">
                    <option>By status and accounts</option>
                    <option>By status and users</option>
                    <option>By status and queue</option>
                    <option>By priority and account</option>
                    <option>By priority and user</option>
                    <option>By priority and queue</option>
                </select>
            </div>
            <div class="reports_date hide margin-top">
                <p id="" class="menu-header">Report Period</p>
                <div class="menu-section">
                    <select id="period-selection">
                        <option>Daily</option>
                        <option>Weekly</option>
                        <option>Monthly</option>
                        <option>User-defined</option>
                    </select>
                    <div id="daily" class="reporting-period-choice">
                        <input id="" class="datepicker date-with-img" type="text" value="<?php echo date('Y-m-d'); ?>" />
                    </div>
                    <div id="weekly" class="reporting-period-choice hide">
                        <select>
                            <option value="2013-06-03">03.06.13 - 09.06.13</option>
                            <option value="2013-06-10">10.06.13 - 16.06.13</option>
                            <option value="2013-06-17">17.06.13 - 23.06.13</option>
                            <option value="2013-06-24">24.06.13 - 30.06.13</option>
                            <option value="2013-07-01">01.07.13 - 07.07.13</option>
                            <option value="2013-07-08">08.07.13 - 14.07.13</option>
                            <option value="2013-07-15">15.07.13 - 21.07.13</option>
                            <option value="2013-07-22">22.07.13 - 28.07.13</option>
                            <option value="2013-07-29">29.07.13 - 04.08.13</option>
                            <option value="2013-08-05">05.08.13 - 11.08.13</option>
                        </select>
                    </div>
                    <div id="monthly" class="reporting-period-choice hide">
                        <div class="row-fluid">
                            <div class="span12">
                                <select>
                                    <option value="2008">2008</option>
                                    <option value="2009">2009</option>
                                    <option value="2010">2010</option>
                                    <option value="2011">2011</option>
                                    <option value="2012">2012</option>
                                    <option value="2013" selected="">2013</option>
                                </select>
                            </div>
                            <div class="span12">
                                <select>
                                    <option value="1">Jan</option>
                                    <option value="2">Feb</option>
                                    <option value="3">Mar</option>
                                    <option value="4">Apr</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8" selected="">Aug</option>
                                    <option value="9">Sep</option>
                                    <option value="10">Oct</option>
                                    <option value="11">Nov</option>
                                    <option value="12">Dec</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div id="user-defined" class="reporting-period-choice hide">
                        <input id="user-defined-from" class="datepicker date-with-img" type="text" value="Date From" />
                        <input id="user-defined-to" class="datepicker date-with-img" type="text" value="Date To" />
                    </div>
                    <input type="button" class="default-btn full-width" value="Get Report">
                </div>
            </div>

<!--        <p id="" class="menu-header">Dates</p>
<div class="menu-section">
    <input id="" class="datepicker input-sidebar-plus-img" type="text" value="Date From" />
</div>-->
</div>
</div>