<div id='outerbar' class="align-left">
    <div id='menu-icon'>
        <a href='index_html' id="logo-icon">
            <img src='img/logo-icon.png' />
        </a>
        <ul id="main-menu" class="menu-base-color">
            <li id="home">
                <a href='index_html'>
                    <!-- <i class="foundicon-globe"></i> -->
                    <i class="fa fa-home  menu-color home-size"></i>
                    <!-- <img $src='img/icon-home.png' />-->
                    <span>home</span>
                </a>
            </li>
            <li id="ticket">
                <a href='ticket_list'>
                    <i class="fa fa-tags  menu-color"></i>
                    <!-- <img src='img/icon-tickets.png' /> -->
                    <span>tickets</span>
                </a>
            </li>
            <li id="queue">
                <a href='queues_list'>
                    <i class="fa fa-tasks  menu-color"></i>
                    <!-- <img src='img/icon-queues.png' /> -->
                    <span>queues</span>
                </a>
            </li>
            <li id="search">
                <a href='nsearch'>
                    <i class="fa fa-search  menu-color"></i>
                    <!-- <img src='img/icon-search_main.png' /> -->
                    <span>search</span>
                </a>
            </li>
            <li id="report">
                <a href='nreports'>
                    <i class="fa fa-bar-chart-o  menu-color"></i>
                    <!-- <img src='img/icon-reports.png' /> -->
                    <span>reports</span>
                </a>
            </li>
            <li id="settings">
                <a href='settings'>
                    <i class="fa fa-cog  menu-color"></i>
                    <!-- <img src='img/icon-settings.png' /> -->
                    <span>settings</span>
                </a>
            </li>
        </ul>
        <ul id="utility-menu">
            <li>
                <a href='http://www.logicalware.com/support/' target="_blank">
                    <!-- <img src='img/icon-support.png' /> -->
                    <i class="fa fa-wrench menu-color"></i>

                    <span class="">support</span>
                </a>
            </li>
            <!-- <li  id="toggle-menu">
                <img src='img/icon-collapse.png' />
                <span>collapse</span>
            </li> -->
        </ul>
        <div id="log-out">
            <a href='logout'>
                <!-- <img src='img/icon-logout.png' /> -->
                <i class="fa fa-sign-out  menu-color"></i>
                <span>log out</span>
            </a>
        </div>
    </div>
</div>