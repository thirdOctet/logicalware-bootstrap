
<div id="innermenu" class="expanded">
    <div class="default-padding">
        <p id="" class="menu-header null-margin-top">Ticket to display</p>
        <div class="menu-section">
            <form id="tickets" accept-charset="utf-8">
                <select>
                    <option>From Account</option>
                    <option>next value</option>
                    <option>next value</option>
                </select>
                <select>
                    <option>Assigned to</option>
                    <option>From Account</option>
                    <option>next value</option>
                    <option>next value</option>
                </select>
                <select>
                    <option>From Queue</option>
                    <option>next value</option>
                    <option>next value</option>
                </select>
                <select>
                    <option>Category 0</option>
                    <option>From Account</option>
                    <option>next value</option>
                    <option>next value</option>
                </select>
                <select>
                    <option>Category 1</option>
                    <option>From Account</option>
                    <option>next value</option>
                    <option>next value</option>
                </select>
                <select>
                    <option>Category 2</option>
                    <option>From Account</option>
                    <option>next value</option>
                    <option>next value</option>
                </select>
                <input id="input-date-from" class="datepicker date-with-img" type="text" value="Date From" />
                <input id="input-date-to" class="datepicker date-with-img" type="text" value="Date To" />
                <select>
                    <option>Priority</option>
                    <option>From Account</option>
                    <option>next value</option>
                    <option>next value</option>
                </select>
                <select>
                    <option>Tags</option>
                    <option>From Account</option>
                    <option>next value</option>
                    <option>next value</option>
                </select>
                <input type="button" class="default-btn full-width" id="" value="Search">
            </form>
        </div>
        <p id="" class="menu-header">Full Text Search</p>
        <div class="menu-section hide">
            <form id="advanced-search" accept-charset="utf-8">
                <input id="" class="null-padding" type="text" value="Full text search" />
                <input type="button" class="default-btn full-width" id="" value="Search">
            </form>
        </div>
    </div>
</div>