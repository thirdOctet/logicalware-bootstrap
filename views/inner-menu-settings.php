<div id="innermenu" class="expanded">
    <div class="default-padding">
        <p id="" class="menu-header">User Settings</p>
        <div class="menu-section">
            <ul class="">
                <li><a href="user_pref">My Preferences</a></li>
                <li><a href="user_pref-change_password_form">Password Change</a></li>
            </ul>
        </div>

        <p id="" class="menu-header">Basic Settings</p>
        <div class="menu-section">
            <ul class="">
                <li><a href="users">Users</a></li>
                <li><a href="accounts">Accounts</a></li>
                <li><a href="groups">User Groups</a></li>
                <li><a href="queues">Queues</a></li>
            </ul>
        </div>

        <p id="" class="menu-header">Advanced Settings</p>
        <div class="menu-section">
            <ul class="">
                <li class="template"><i class="fa pull-right fa-plus"></i>
                    <a href="template">Templates</a>
                    <ul class="hide">
                        <li><a href="template_group">Template Groups</a></li>
                    </ul>
                </li>
                <li class="filters"><i class="fa pull-right fa-plus "></i>
                    <a href="filters">Filters</a>
                    <ul class="hide">
                        <li><a href="filters-groups">Filter Groups</a></li>
                        <li><a href="filters-headers">Header List</a></li>
                    </ul>
                </li>
                <li class="category">Categories <i class="fa-plus fa pull-right fa-plus"></i>
                    <ul class="hide">
                        <li><a href="category0">Category 0</a></li>
                        <li><a href="category1">Category 1</a></li>
                        <li><a href="category2">Category 2</a></li>
                    </ul>
                </li>
                <li><a href="ticket_tag">Tags</a></li>
                <li><a href="kb">Knowledge Base</a></li>
                <li><a href="customers">Self Service</a></li>
                <li><a href="token">Access Groups</a></li>
            </ul>
        </div>

        <p id="" class="menu-header">System Settings</p>
        <div class="menu-section">
            <ul class="">
                <li><a href="system-delete_form">Delete & Archive Tickets</a></li>
                <li><a href="system-datetz_form">Localisation</a></li>
                <li><a href="system-wtcal">Work Time Calendar</a></li>
                <li><a href="system-spamholder_form">Spam Holder</a></li>
                <li><a href="std_attach">Attachments</a></li>
                <li><a href="blacklist">Blacklist</a></li>
                <li><a href="system-other_form">Other</a></li>
            </ul>
        </div>
    </div>
</div>