<div id="smartbar">
    <div id="scroll-to-top"><i class="fa fa-chevron-up"></i></div>
    <!-- <div id="scroll-to-top">
        <div class="">
            <span class="ticket-top-arrow"></span>
            <span class="rect-vertical"></span>
        </div>
    </div> -->

    <span class="divider display-inline-block">
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
    </span>

    <div id="scroll-to-last-ticket"><i class="fa fa-ticket"></i></div>
    <!-- <div id="scroll-to-open-ticket">
        <div class="">
            <span class="ticket-right-arrow"></span>
            <span class="ticket-rectangle-block"></span>
        </div>
    </div> -->
    <span class="divider display-inline-block">
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
    </span>

    <div id="scroll-to-response"><i class="fa fa-comment"></i></div>
    <!-- <div id="scroll-to-response">
        <div class="">
            <span class="ticket-response-box"></span>
            <span class="ticket-arrow-base"></span>
        </div>
    </div> -->
    <span class="divider display-inline-block">
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
        <div><i class="fa fa-circle"></i></div>
    </span>
    <div id="expand-all-tickets"><i class="fa fa-plus"></i></div>
    <!-- <div id="expand-all-tickets">
        <div class="">
            <span class="rect-horizontal"></span>
            <span class="rect-vertical"></span>
        </div>
    </div> -->
</div>
<div id="footer">
    <div id="footeralignment">
        <form id="">
            <div id="ticket-display-default">
                <div class="footer-btn">
                    <input type="checkbox" class="display-inline"/><span class="margin-left display-inline">get next ticket from queue</span>
                </div>
                <?php
                echo get_input_button("footer-btn cancel", "cancel", "Cancel");
                echo get_input_button("footer-btn update", "update", "Update");
                echo get_input_button("footer-btn update-close", "update-close", "Update & Close");
                ?>

            </div>
            <div id="ticket-display-textarea-keypress" class="hide">
                <div class="footer-btn">
                    <input type="checkbox" class="display-inline"/><span class="margin-left display-inline">get next ticket from queue</span>
                </div>

                <?php
                echo get_input_button("footer-btn cancel", "cancel", "Cancel");
                echo get_input_button("footer-btn save-note", "save-note", "Save as Note");
                echo get_input_button("footer-btn save-draft", "save-draft", "Save as Draft");
                echo get_input_button("footer-btn respond", "respond", "Respond");
                echo get_input_button("footer-btn respond-close", "respond-close", "Respond & Close");
                ?>
            </div>
        </form>
    </div>
</div>
