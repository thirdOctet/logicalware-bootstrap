<div id="innermenu" class="expanded">
    <div class="default-padding">
        <p class="menu-header">Options</p>
        <div class="menu-section">
            <ul>
                <li><a href="nreports">Reports</a></li>
                <li class="rbuilder">Report builder<i class="fa fa-plus pull-right"></i>
                    <ul class="hide">
                        <li><a href="rbuilder-list">List Reports</a></li>
                        <li><a href="rbuilder-add_form">Create/Add New Report</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>