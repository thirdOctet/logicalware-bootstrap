<div id="footer">
    <div id="footeralignment">
        <form id="">
            <div class="form-container">
                <input type="button" class="footer-btn blacklist" id="blacklist" value="Add to Blacklist" />
                <select>
                    <option>Category 0</option>
                </select>
                <select>
                    <option>Category 1</option>
                </select>
                <select>
                    <option>Category 2</option>
                </select>
                <select>
                    <option>Priority</option>
                    <option value="0">No change</option>
                    <option value="5">Critical</option>
                    <option value="4">High</option>
                    <option value="3">Normal</option>
                    <option value="2">Low</option>
                    <option value="1">Junk</option>
                </select>
                <select>
                    <option>Status</option>
                    <option value="">No change</option>
                    <option value="Open">Open</option>
                    <option value="Hold">Hold</option>
                    <option value="Closed">Closed</option>
                    <option value="Spam">Spam</option>
                </select>
                <input type="button" class="footer-btn save" id="save" value="Update Selected" />
            </div>
        </form>
        <!--        <a href="" id="blacklist" class="footer-btn">Add to Blacklist</a>
                <a href="" id="respond-close" class="footer-btn">Respond and close</a>
                <a href="" id="respond" class="footer-btn">Respond</a>
                <a href="" id="save-note" class="footer-btn">Save as Note</a>
                <a href="" id="cancel" class="footer-btn">Cancel</a>
                <a href="" id="delete" class="footer-btn">Delete</a>-->
                <!--<a href="" id="save" class="footer-btn">Save</a>-->
            </div>
        </div>
