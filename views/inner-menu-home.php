<div id="innermenu" class="expanded">
    <div class="default-padding">


        <p id="" class="menu-header">Information</p>
        <div class="menu-section">
            <p>Welcome to Logicalware's email management platform.</p>
            <p>The total number of new, open and overdue tickets assigned to you are shown here.</p>
        </div>
    </div>
</div>