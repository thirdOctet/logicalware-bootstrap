<div id="innermenu" class="expanded">
    <div class="default-padding">
        <form id="tickets" accept-charset="utf-8">
            <p id="" class="menu-header null-margin-top">Required Options</p>
            <div class="menu-section">
                <div class="inner-menu-content">
                    <div class="item-holder">
                        <span class="item-name">Account:</span>
                        <span class="item-input">
                            <select>
                                <option>-</option>
                                <option>some other account</option>
                                <option>another account</option>
                            </select>
                        </span>
                    </div>
                    <div class="item-holder">
                        <span class="item-name">Assigned to::</span>
                        <span class="item-input">
                            <select>
                                <option>jonathan</option>
                                <option>david</option>
                                <option>will</option>
                            </select>
                        </span>
                    </div>
                    <div class="item-holder">
                        <span class="item-name">Priority:</span>
                        <span class="item-input">
                            <select>
                                <option>normal</option>
                                <option>medium</option>
                                <option>emergency</option>
                            </select>
                        </span>
                    </div>
                    <div class="item-holder">
                        <span class="item-name">Response Time:</span>
                        <span class="item-input">
                            <select>
                                <option>Account Default</option>
                                <option>today</option>
                                <option>tomorrow</option>
                            </select>
                        </span>
                    </div>
                </div>
            </div>
            <p id="" class="menu-header">Other Options</p>
            <div class="menu-section hide">
                <div class="item-holder">
                    <span class="item-name">Category 0:</span>
                    <span class="item-input">
                        <select>
                            <option>Igor</option>
                            <option>Jonathan</option>
                            <option>David</option>
                        </select>
                    </span>
                </div>
                <div class="item-holder">
                    <span class="item-name">Category 1:</span>
                    <span class="item-input">
                        <select>
                            <option>Igor</option>
                            <option>Jonathan</option>
                            <option>David</option>
                        </select>
                    </span>
                </div>
                <div class="item-holder">
                    <span class="item-name">Category 2:</span>
                    <span class="item-input">
                        <select>
                            <option>Critical</option>
                            <option>High</option>
                            <option>Normal</option>
                            <option>Low</option>
                            <option>Junk</option>
                        </select>
                    </span>
                </div>
                <div class="item-holder">
                    <span class="item-name">Tags:</span>
                    <span class="item-input">
                        <select>
                            <option>tag 1</option>
                            <option>tag 2</option>
                            <option>tag 3</option>
                        </select>
                    </span>
                </div>
                <div class="item-holder">
                    <span class="item-name align-top">Description:</span>
                    <span class="item-input">
                        <textarea></textarea>
                    </span>
                </div>
            </div>
        </form>
    </div>
</div>