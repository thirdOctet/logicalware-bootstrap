<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-tickets-create.php'); ?>
    <div id="content">
        <div class="default-padding">

             <!-- Ticket Response -->
            <div class="content-holder ticket-response">
                <div class="row-fluid">
                    <div class="">

                        <!-- SUBJECT field -->
                        <div id="subject-field">
                            <div class="span3">
                                <span class="subject-header">Subject:</span>
                            </div>
                            <div class="offset1 span12">
                                <input type="text" />
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- TO field -->
                        <div id="to-field">
                            <div class="span3">
                                <span class="subject-header">To:</span>
                            </div>
                            <div class="span1">
                                <img id="email-search" src="img/icon-search.png" class=""/>
                            </div>
                            <div class="span12">
                                <input type="text" />
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- CC field -->
                        <div id="cc-field" class="hide">
                            <div class="span3">
                                <span class="subject-header">CC:</span>
                            </div>
                            <div class="span1">
                                <img id="email-search" src="img/icon-search.png" class=""/>
                            </div>
                            <div class="span12">
                                <input type="text" />
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- BCC field -->
                        <div id="bcc-field" class="hide">
                            <div class="span3">
                                <span class="subject-header">BCC:</span>
                            </div>
                            <div class="span1">
                                <img id="email-search" src="img/icon-search.png" class=""/>
                            </div>
                            <div class="span12">
                                <input type="text" />
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- RESPONSE attributes -->
                        <div class="response-attributes">
                            <div class="offset4 span12">
                                <span id="ccBtn">Add CC</span>
                                <span id="bccBtn">Add BCC</span>
                                <span id="templateBtn">Use Template</span>
                                <span id="personalTemplateBtn">Use Personal Template</span>
                                <span id="attachFilesBtn">Attach files</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- Use Template -->
                        <div id="template" class="hide margin-bottom-5px">
                            <div class="offset4 span12">
                                <div><span class="right-arrow"></span><span>Auto Responses</span>
                                </div>
                                <ul class="hide list">
                                    <li><span class="block"></span>Auto response:support</li>
                                </ul>
                                <div><span class="right-arrow"></span><span>Contract Modifications</span></div>
                                <ul class="hide list">
                                    <li><span class="block"></span>Contract - Termination</li>
                                    <li><span class="block"></span>Contract - User Decrease</li>
                                    <li><span class="block"></span>Contract - User Increase</li>
                                </ul>
                                <div><span class="block"></span><span>Auto response: Old Employees</span></div>
                                <div><span class="block"></span><span>Users - Delete</span></div>
                                <div><span class="block"></span><span>Cite all previous messages</span></div>
                                <div><span class="block"></span><span>Cite last message</span></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- Use Personal Template -->
                        <div id="personalTemplate" class="hide margin-bottom-5px">
                            <div class="span4">
                                <span class="subject-header">Tags:</span>
                            </div>
                            <div class="span12">
                                <form id="" method="" action="">
                                    <div class="span20">
                                        <input type="text" name="" value="*" />
                                    </div>
                                    <div class="span4">
                                        <input type="submit" class="default-btn full-width save" value ="search" />
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- Attach files -->
                        <div id="attachFiles" class="hide">
                            <div id="fileupload" class="margin-bottom-5px">
                                <div class="span4">
                                    <span class="subject-header">File:</span>
                                </div>
                                <div class="span12">
                                    <input type="file" class=""/>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="standard_attachment margin-bottom-5px">
                                <div class="span4">
                                    <span class="subject-header">Standard Attachment:</span>
                                </div>
                                <div class="span10">
                                    <select id="standard_attach">
                                        <option value=""></option>
                                        <option value="mbox.txt">mbox.txt</option>
                                    </select>
                                </div>
                                <div class="span2">
                                    <?php echo get_input_button("default-btn full-width save", "attach", "Attach") ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div>
                    <textarea id="email-reply"></textarea>
                </div>
            </div>
        </div>
    </div>
    <?php
    $btn_array = array();
    array_push($btn_array, get_input_button("footer-btn cancel", "cancel", "Cancel"));
    array_push($btn_array, get_input_button("footer-btn save-note", "save-note", "Save as Note"));
    array_push($btn_array, get_input_button("footer-btn save-draft", "save-draft", "Save as Draft"));
    array_push($btn_array, get_input_button("footer-btn respond", "respond", "Respond"));
    array_push($btn_array, get_input_button("footer-btn respond-close", "respond-close", "Respond & Close"));

    echo get_footer($btn_array);
    ?>

</div>
<?php include('views/footer.php'); ?>