<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="table-container">
                </div>
                <!-- ticket data -->
                <div class="content-holder">
                    <div class="table-container">
                        <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                            <table id="ticketlist" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>QUEUE</th>
                                        <th>GROUP</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            Billing &amp; Invoicing
                                        </td>
                                        <td>
                                            Billing &amp; Invoicing
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "queues-update_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Free Trials</td>
                                        <td>Free Trials</td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "queues-update_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Sales &amp; Enquiries</td>
                                        <td>Sales &amp; Enquiries</td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "queues-update_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Support</td>
                                        <td>Support</td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "queues-update_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>TRIALS</td>
                                        <td>Support</td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "queues-update_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            6
                                        </td>
                                        <td>
                                            Wrong email address
                                        </td>
                                        <td>
                                            Support
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "queues-update_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_button('footer-btn create', 'new-queue', "Add New Queue", "queues-add_form"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>