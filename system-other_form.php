<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">OTHER SETTINGS</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="table-container">
                            <form accept-charset="utf-8" class="blockform" method="post" action="system/other" enctype="multipart/form-data">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Set Value/Activate</th>
                                            <th>Caption</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <tr><td>Formatting - Always paste as plain text </td><td><input type="hidden" name="paste_plain_text_chk" value="1"> <input type="checkbox" name="paste_plain_text" value="1" checked=""> </td><td>When pasting text from other sources, choose to always paste without formatting </td></tr>
                                       <tr><td>Formatting - Set default to HTML </td><td><input type="hidden" name="default_format_html_chk" value="1"> <input type="checkbox" name="default_format_html" value="1" checked=""> </td><td>Choose to use HTML instead of plain text when responding and within templates/signatures </td></tr>
                                       <tr><td>General - Accept bounce back emails</td><td><input type="hidden" name="accept_bounceback_chk" value="1"> <input type="checkbox" name="accept_bounceback" value="1"> </td><td>Choose to receive bounce back emails instead of having them marked as SPAM </td></tr>
                                       <tr><td>Knowledge Base - Default search tag </td><td><input type="text" name="kb_default_tag" value="*"> </td><td>When using the Knowledge Base, choose your default search tag </td></tr>
                                       <tr><td>Overview - Show account statistics </td><td><input type="hidden" name="overview_account_chk" value="1"> <input type="checkbox" name="overview_account" value="1" checked=""> </td><td>On the tickets overview page, choose to display account statistics </td></tr>
                                       <tr><td>Overview - Show queue statistics </td><td><input type="hidden" name="overview_queue_chk" value="1"> <input type="checkbox" name="overview_queue" value="1" checked=""> </td><td>On the tickets overview page, choose to display queue statistics </td></tr>
                                       <tr><td>Reports - Do not list users with 0 values </td><td><input type="hidden" name="hide_user_zero_chk" value="1"> <input type="checkbox" name="hide_user_zero" value="1"> </td><td>When generating reports, choose to not list users with 0 values </td></tr>
                                       <tr><td>Responding - Display signatures in response box </td><td><input type="hidden" name="signature_in_response_chk" value="1"> <input type="checkbox" name="signature_in_response" value="1" checked=""> </td><td>When responding, choose to display the USER and ACCOUNT signatures in the response box </td></tr>
                                       <tr><td>Responding - Remove spacing between signatures </td><td><input type="hidden" name="no_signature_separator_chk" value="1"> <input type="checkbox" name="no_signature_separator" value="1" checked=""> </td><td>When responding, choose to remove the standard spacing between the user and account signatures </td></tr>
                                       <tr><td>Responding - Show list of OPEN tickets from same customer </td><td><input type="hidden" name="show_other_customer_tickets_chk" value="1"> <input type="checkbox" name="show_other_customer_tickets" value="1" checked=""> </td><td>When responding, choose to display a list of OPEN tickets from the same customer </td></tr>
                                       <tr><td>Response Fields - Set BCC as read only </td><td><input type="hidden" name="bcc_readonly_chk" value="1"> <input type="checkbox" name="bcc_readonly" value="1"> </td><td>Set the BCC field to read only </td></tr>
                                       <tr><td>Response Fields - Set default BCC email address </td><td><input type="text" name="bcc_default" value=""> </td><td>Set an email address for all outgoing emails to be BCC'd to </td></tr>
                                       <tr><td>Response Fields - Set subject as read only </td><td><input type="hidden" name="subject_readonly_chk" value="1"> <input type="checkbox" name="subject_readonly" value="1"> </td><td>Set the subject field to read only </td></tr>
                                       <tr><td>Response Fields - Show full name in FROM field </td><td><input type="hidden" name="usernamefrom_chk" value="1"> <input type="checkbox" name="usernamefrom" value="1"> </td><td>Show the full name of the sender in the FROM field </td></tr>
                                       <tr><td>Response Target Time - Set 1st indicator time </td><td><input type="text" name="limit_response_target3" value="00:45"> </td><td>On ticket lists, set the time limit for when the 1st indicator colour (Blue) should display </td></tr>
                                       <tr><td>Response Target Time - Set 2nd indicator time </td><td><input type="text" name="limit_response_target2" value="00:30"> </td><td>On ticket lists, set the time limit for when the 2rd indicator colour (Green) should display </td></tr>
                                       <tr><td>Response Target Time - Set 3rd indicator time </td><td><input type="text" name="limit_response_target1" value="00:15"> </td><td>On ticket lists, set the time limit for when the 3rd indicator colour (Magenta) should display </td></tr>
                                       <tr><td>Search - Include message body in export </td><td><input type="hidden" name="csv_with_msg_body_chk" value="1"> <input type="checkbox" name="csv_with_msg_body" value="1"> </td><td>When exporting search results, choose to also include the body of the first message </td></tr>
                                       <tr><td>Tickets - Allow changing a tickets original queue </td><td><input type="hidden" name="reassign_queue_chk" value="1"> <input type="checkbox" name="reassign_queue" value="1" checked=""> </td><td>Choose to allow users to change the tickets original queue </td></tr>
                                       <tr><td>Tickets - Prevent anassigned users from processing tickets </td><td><input type="hidden" name="unassigned_user_process_ticket_chk" value="1"> <input type="checkbox" name="unassigned_user_process_ticket" value="1" checked=""> </td><td>Choose to prevent users from replying to a ticket unless it has been assigned to them </td></tr>
                                       <tr><td>Tickets - Prevent tickets being edited whilst queued </td><td><input type="hidden" name="noeditinqueue_chk" value="1"> <input type="checkbox" name="noeditinqueue" value="1" checked=""> </td><td>Choose to prevent users from modifying tickets whilst they are still in a queue </td></tr>
                                       <tr><td>Tickets - Restrict access to queues to user groups </td><td><input type="hidden" name="restrict_queue_by_group_chk" value="1"> <input type="checkbox" name="restrict_queue_by_group" value="1" checked=""> </td><td>Choose to restrict access to queues to assigned user groups only </td></tr>
                                       <tr><td>Tickets - Restrict access to tickets based on category </td><td><input type="hidden" name="viewbycat_chk" value="1"> <input type="checkbox" name="viewbycat" value="1"> </td><td>Choose to restrict access to tickets based on category selection </td></tr>

                                   </tbody>
                               </table>
                           </form>
                       </div>
                   </div>
               </div>
               <div class="clearfix"></div>
           </div>
       </div>
   </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_input_button("footer-btn save", "system-other", "Apply Changes"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>