<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">

                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">ATTACHMENT</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Add standard attachments to your library</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="item-holder">
                            <div class="span4 item-name">Choose File:</div>
                            <div class="span8">
                                <div class="item-input">
                                    <input type="file"/>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="item-holder">
                            <div class="offset4 span2">
                                <?php echo get_input_button("default-btn full-width save", "add-attachment", "Add") ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">EXISTING ATTACHMENTS</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Your current attachments</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="item-holder">
                            <div class="table-container">
                                <div class="table-container">
                                    <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                                        <table id="ticketlist" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ATTACHMENT NAME</th>
                                                    <th>ACTION</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>mbox.txt</td>
                                                    <td><?php echo get_button("default-btn delete", "", "Delete", ""); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>mbox.txt</td>
                                                    <td><?php echo get_button("default-btn delete", "", "Delete", ""); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>mbox.txt</td>
                                                    <td><?php echo get_button("default-btn delete", "", "Delete", ""); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>mbox.txt</td>
                                                    <td><?php echo get_button("default-btn delete", "", "Delete", ""); ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();

//array_push($btn_array, get_input_button("default-btn save", "apply-spam", "Apply"));

echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>