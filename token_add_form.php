<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <!-- ADD CHOICE -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">ACCESS GROUPS</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Name:</span>
                            </div>
                            <div class="span14">
                                <input type="text" value=""/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>                        
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $btn_array = array();
    array_push($btn_array, get_input_button("default-btn save", "save-token", "Save"));
    echo get_footer($btn_array);
    ?>
</div>
<?php include('views/footer.php'); ?>