<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content" class="">
        <div class="default-padding">
            <form id="" method="post" accept-charset="utf-8">

                <!-- SET MESSAGE SOURCE -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">INFORMATION</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Set the users basic information</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="item-holder">
                                    <div class="span6 item-name">Full Name:</div>
                                    <div class="span18">
                                        <input type="text" value="" />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Email Address:</div>
                                    <div class="span18">
                                        <input type="text" value="" />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">User Name: <span>*</span></div>
                                    <div class="span18">
                                        <input type="text" value="usernamePreset" disabled=""/>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="span12">
                                <div class="item-holder">
                                    <div class="span6 item-name">Substitute User:</div>
                                    <div class="span18">
                                        <select name="substitute_user" id="substitute_user">
                                            <option value=""selected="">Not assigned</option>
                                            <option value="david">david</option>
                                            <option value="galtsev">galtsev</option>
                                            <option value="igor">igor</option>
                                            <option value="logicalware">logicalware</option>
                                            <option value="spamCollector">spamCollector</option>
                                            <option value="timets">timets</option>
                                            <option value="will">will</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Password: 
                                        <span class="ticket-helper info-btn-small margin-left">
                                            <div class="display-inline-block">
                                                <span class="info">i</span>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="span18">
                                        <input type="password" value="defaultpass" />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Confirm Password: <span>*</span></div>
                                    <div class="span18">
                                        <input type="password" value="" />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <!-- ASSIGN MESSAGES TO -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">ASSIGNMENT</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Assign the user to a user group</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="item-holder">
                                    <div class="span6 item-name">Assign to:</div>
                                    <div class="span18">
                                        <select id="something" class="nothing" name="somenothing">                            
                                            <option>-</option>
                                            <option>Another one</option>
                                            <option>ok another one</option>
                                            <option>Another one</option>
                                            <option>ok another one</option>
                                            <option>Another one</option>
                                            <option>ok another one</option>
                                            <option>Another one</option>
                                            <option>ok another one</option>
                                            <option>Another one</option>
                                            <option>ok another one</option>
                                            <option>Another one</option>
                                            <option>ok another one</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <!-- SET PERMISSIONS -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">PERMISSIONS</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left  padding-top-bottom">Set the users access permissions</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="item-holder">
                                    <div class="span6 item-name">Admin:</div>
                                    <div class="span18">
                                        <input type="checkbox" name="tickets" value="tickets"/>
                                        <span>Allow unrestricted access</span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">View:</div>
                                    <div class="span18">
                                        <input type="checkbox" value="tickets"/>
                                        <span>Queues</span>
                                        <input type="checkbox" value="reports"/>
                                        <span>Reports</span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Access Group:</div>
                                    <div class="span18">
                                        <select multiple="true">
                                            <option>Category 0</option>                                                    
                                        </select> 
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Member of Groups</div>
                                    <div class="span18">
                                        <div class="item-input margin-bottom-5px">                                        
                                            <input type="checkbox" /> <span> Auto response: Old Employees</span>
                                        </div>
                                        <div class="item-input margin-bottom-5px">                                        
                                            <input type="checkbox" /> <span>Auto response: Support</span>
                                        </div>
                                        <div class="item-input margin-bottom-5px">                                        
                                            <input type="checkbox" /> <span>Contract - Termination</span>
                                        </div>
                                        <div class="item-input margin-bottom-5px">                                        
                                            <input type="checkbox" /> <span>Contract - User Decrease</span>
                                        </div>
                                        <div class="item-input margin-bottom-5px">                                        
                                            <input type="checkbox" /> <span>Contract - User Increase</span>
                                        </div>
                                        <div class="item-input">                                        
                                            <input type="checkbox" /> <span>Users - Delete</span>
                                        </div>
                                    </div>                                
                                    <div class="clearfix"></div>
                                </div> 
                            </div>                    
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <!-- SET TEMPLATE RESPONSE FOR USERS -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">SIGNATURE</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left  padding-top-bottom">Set the users email signature</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="item-holder">
                                <textarea id="email-reply"></textarea>                        
                            </div>
                        </div>
                    </div>
                </div>        
            </form>            
        </div>

    </div>


    <?php
    $btn_array = array();

    array_push($btn_array, get_input_button("footer-btn cancel", "cancel", "Cancel"));
    array_push($btn_array, get_input_button("footer-btn delete", "delete", "Delete"));
    array_push($btn_array, get_input_button("footer-btn save", "save", "Save"));
    echo get_footer($btn_array);
    ?>

</div>
<?php include('views/footer.php'); ?>