<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">PREFERENCES</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Set your preferences</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show contentlineheight">
                        <form accept-charset="utf-8" action="user_pref/save_prefs" method="post">
                            <div class="item-holder">
                                <div class="span4 item-name">Columns to display on ticket lists:</div>
                                <div class="span8">
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>ticket_id</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>state</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>subject</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>from_name</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>assigned</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>category0</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>category1</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>category2</span>
                                    </div>
                                </div>
                                <div class="span8">
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>date</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>time</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>priority</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>note</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>date_closed</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>response_target</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_display_columns" /> <span>access_group</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span4 item-name">Categories available when batch updating tickets:</div>
                                <div class="span16">
                                    <div class="item-input">
                                        <input type="checkbox" name="value_batch_update" /> <span>category0</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_batch_update" /> <span>category1</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_batch_update" /> <span>category2</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span4 item-name">Categories to display on ticket filter list:</div>
                                <div class="span16">
                                    <div class="item-input">
                                        <input type="checkbox" name="value_filter" /> <span>category0</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_filter" /> <span>category1</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" name="value_filter" /> <span>category2</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span4 item-name">Message preview mode. How to display tickets within ticket lists:</div>
                                <div class="span16">
                                    <div class="item-input">
                                        <input type="radio" name="value_preview_by_click" checked="" value="0"/><span>Compact</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="radio" name="value_preview_by_click" value="1"/> <span>Full</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span4 item-name">Number of tickets to display per page on ticket lists(Max: 15)</div>
                                <div class="span14">
                                    <div class="item-input">
                                        <input type="text" name="value_batch_size" value="15"/>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="item-holder">
                                <div class="span4 item-name">Show html message as default:</div>
                                <div class="span16">
                                    <div class="item-input">
                                        <input type="checkbox" name="value_show_html" /> <span>yes</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span4 item-name">Ticket list refresh interval (minimum:5mins):</div>
                                <div class="span14">
                                    <div class="item-input">
                                        <input type="text" value="10" name="value_refresh_rate"/>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_input_button("footer-btn delete", "reset", "Reset to defaults", "reset", "submit"));
array_push($btn_array, get_input_button("footer-btn save", "apply-change", "Apply changes", "apply", "submit"));
echo get_footer($btn_array);
?>
</form>
</div>
<?php include('views/footer.php'); ?>