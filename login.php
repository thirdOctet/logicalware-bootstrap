<?php include('views/header.php'); ?>
<div id='login-background'>
    <div id='logincontainer'>
        <div id='login-top'>            
            <img id="logo" src='img/logo-logicalware.png' />            
        </div>
        <div id='login-form-container'>
            <form id='form-login' method="POST" accept-charset="utf-8" action="">
                <input class="disabled-input" type='text' value="your.login.com" disabled/>
                <input id="username" class='' type='text' value='Username...'/>
                <input id="password" class='' type='password' value=''/>
                <input id="submit-button" type="submit" value="log in">                
                <!--<input type="submit" value='log in'/>-->
                <div class="login-area">
                    <input id="login-checkbox"type="checkbox" value=""/> <span class="login-remember">Remember my username</span>
                </div>                
            </form>
        </div>
    </div>
</div>
<?php include('views/footer.php'); ?>