<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <!-- settings info -->
                <div class="content-holder">
                    <div class="span2">
                        <div class="settings-default">
                            <div class="ticket-helper info-btn">
                                <div class="display-inline-block">
                                    <span class="info">i</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span22">
                        You specify a name of the user whom all spam messages (messages, that SpamAssasin filter marks as Spam) would be assigned to.
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">SPAM HOLDER</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Select who would be assigned spam tickets</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">

                            <div class="item-holder">
                                <div class="span4 item-name">
                                    <span>Tag Name:</span>
                                </div>
                                <div class="span4">
                                    <select id="spamholder" name="spamholder">
                                        <option value="david">david</option>
                                        <option value="galtsev">galtsev</option>
                                        <option value="igor">igor</option>
                                        <option value="jonathan">jonathan</option>
                                        <option value="logicalware">logicalware</option>
                                        <option selected="selected" value="spamCollector">spamCollector</option>
                                        <option value="timets">timets</option>
                                        <option value="will">will</option></select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();

array_push($btn_array, get_input_button("default-btn save", "apply-spam", "Apply"));

echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>