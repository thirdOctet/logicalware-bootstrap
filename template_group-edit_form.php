<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="content-holder">
                    <div class="ticket-holder">
                        <div class="ticket-header">
                            <span class="assigned align-left default-padding padding-top-bottom">TEMPLATES GROUP</span>
                            <span class="ticket-arrow-assigned align-left"></span>
                            <div class="clearfix"></div>
                        </div>
                        <div class="ticket-content default-padding show">
                            <div class="item-holder">
                                <div class="span4">
                                    <span class="item-name">Group Name:</span>
                                </div>
                                <div class="span20">
                                    <span class="item-input">
                                        Auto Responses
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span4">
                                    <span class="item-name">Set as private:</span>
                                </div>
                                <div class="span20">
                                    <span class="item-input">
                                        <input type="checkbox" /> <span>(Used for the appointment of private user templates</span>
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span4">
                                    <span class="item-name">Templates in group:</span>
                                </div>
                                <div class="span20">
                                    <div class="item-input margin-bottom-5px">
                                        <input type="checkbox" /> <span> Auto response: Old Employees</span>
                                    </div>
                                    <div class="item-input margin-bottom-5px">
                                        <input type="checkbox" /> <span>Auto response: Support</span>
                                    </div>
                                    <div class="item-input margin-bottom-5px">
                                        <input type="checkbox" /> <span>Contract - Termination</span>
                                    </div>
                                    <div class="item-input margin-bottom-5px">
                                        <input type="checkbox" /> <span>Contract - User Decrease</span>
                                    </div>
                                    <div class="item-input margin-bottom-5px">
                                        <input type="checkbox" /> <span>Contract - User Increase</span>
                                    </div>
                                    <div class="item-input">
                                        <input type="checkbox" /> <span>Users - Delete</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_input_button("footer-btn save", "save", "Save"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>