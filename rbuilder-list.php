<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-rbuilder.php'); ?>    
    <div id="content">
        <div class="default-padding">
            <div class="content-holder">
                <div class="table-container">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Report Title</th>
                                <th>Action</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>2</td>
                                <td>test</td>
                                <td>
                                    <span><a href="rbuilder" class="action-button action-edit">Edit</a></span>
                                    <span><a href="" class="action-button action-prepare">Prepare</a></span>
                                    <span><a href="rbuilder"class="action-button action-delete">Delete</a></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php include('views/footer-rbuilder.php'); ?>
</div>
<?php include('views/footer.php'); ?>