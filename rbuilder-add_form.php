<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-rbuilder.php'); ?>
    <div id="content">
        <div class="default-padding">

            <!-- Add Report -->
            <div class="content-holder">
                <div class="ticket-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">ADD REPORT</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="item-holder">
                                <div class="span4">
                                    <span class="item-name">Report Title:</span>
                                </div>
                                <div class="span14">
                                    <span class="item-input">
                                        <input type="text"/>
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <!-- Reporting Groups -->
            <div class="content-holder">
                <div class="ticket-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">REPORTING GROUPS</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="item-holder">
                                    <div class="span4">
                                        <span class="item-name">Main Group:</span>
                                    </div>
                                    <div class="span18">
                                        <span class="item-input">
                                            <select size="4">
                                                <option>User</option>
                                                <option>Queue</option>
                                                <option>Account</option>
                                                <option>Enquiry Type</option>
                                            </select>
                                        </span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="span12">
                                <div class="item-holder">
                                    <div class="span4">
                                        <span class="item-name">Sub-Group:</span>
                                    </div>
                                    <div class="span18">
                                        <span class="item-input">
                                            <select size="4">
                                                <option>User</option>
                                                <option>Queue</option>
                                                <option>Account</option>
                                                <option>Enquiry Type</option>
                                            </select>
                                        </span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <!-- Columns to Display -->
            <div class="content-holder">
                <div class="ticket-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">COLUMNS TO DISPLAY</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <!-- <table>
                            <tbody>
                                <tr>
                                    <td>
                                        Available:
                                        <select size="16" multiple="multiple">
                                            <option value="event_type:assign">Tickets assigned</option>
                                            <option value="event_type:close">Tickets closed</option>
                                            <option value="event_type:create">Tickets created</option>
                                            <option value="event_type:manual">Tickets created manually</option>
                                            <option value="event_type:receive">Messages received</option>
                                            <option value="event_type:reopen">Tickets reopened</option>
                                            <option value="event_type:respond">Messages responded</option>
                                            <option value="priority:1">With priority Junk</option>
                                            <option value="priority:2">With priority Low</option>
                                            <option value="priority:3">With priority Normal</option>
                                            <option value="priority:4">With priority High</option>
                                            <option value="priority:5">With priority Critical</option>
                                            <option value="status:Closed">With status Closed</option>
                                            <option value="status:Hold">With status Hold</option>
                                            <option value="status:Open">With status Open</option>
                                            <option value="status:Spam">With status Spam</option>
                                        </select>
                                    </td>
                                    <td class="default-padding">
                                        <div id="columnsadd" class="closed default-btn">
                                            <i class="fa fa-chevron-right"></i>
                                            <i class="fa fa-chevron-right"></i>
                                            <i class="fa fa-chevron-right"></i>
                                        </div>
                                        <br>
                                        <br>
                                        <div id="columnsremove" class="closed default-btn">
                                            <i class="fa fa-chevron-left"></i>
                                            <i class="fa fa-chevron-left"></i>
                                            <i class="fa fa-chevron-left"></i>
                                        </div>
                                    </td>
                                    <td>
                                        Selected:
                                        <select size="16" multiple="multiple" class="full-width"></select>
                                    </td>
                                </tr>
                            </tbody>
                        </table> -->
                        <div class="row-fluid">
                            <div class="span10">
                                <div class="item-holder">
                                    <span class="item-name">Available:</span>
                                    <span class="item-input">
                                        <select id="available" size="16" multiple="multiple">
                                            <option value="event_type:assign">Tickets assigned</option>
                                            <option value="event_type:close">Tickets closed</option>
                                            <option value="event_type:create">Tickets created</option>
                                            <option value="event_type:manual">Tickets created manually</option>
                                            <option value="event_type:receive">Messages received</option>
                                            <option value="event_type:reopen">Tickets reopened</option>
                                            <option value="event_type:respond">Messages responded</option>
                                            <option value="priority:1">With priority Junk</option>
                                            <option value="priority:2">With priority Low</option>
                                            <option value="priority:3">With priority Normal</option>
                                            <option value="priority:4">With priority High</option>
                                            <option value="priority:5">With priority Critical</option>
                                            <option value="status:Closed">With status Closed</option>
                                            <option value="status:Hold">With status Hold</option>
                                            <option value="status:Open">With status Open</option>
                                            <option value="status:Spam">With status Spam</option>
                                        </select>
                                    </span>
                                </div>
                            </div>
                            <div class="span2 offset1">
                                <div class="columnsdisplay text-center">
                                    <div id="columnsadd" class="">
                                        <i class="fa fa-chevron-right"></i>
                                        <i class="fa fa-chevron-right"></i>
                                        <i class="fa fa-chevron-right"></i>
                                    </div>
                                    <div id="columnsremove" class="">
                                        <i class="fa fa-chevron-left"></i>
                                        <i class="fa fa-chevron-left"></i>
                                        <i class="fa fa-chevron-left"></i>
                                    </div>
                                </div>

                            </div>
                            <div class="span10 offset1">
                                <div class="item-holder">
                                    <span class="item-name">Selected:</span>
                                    <span class="item-input">
                                        <select id="selected" size="16" multiple="multiple">

                                        </select>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('views/footer-rbuilder.php'); ?>
</div>
<?php include('views/footer.php'); ?>