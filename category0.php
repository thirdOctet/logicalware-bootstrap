<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <!-- CATEGORY NAME HEADER -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">CATEGORY NAME</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Set the name of the category</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="item-holder">
                                <div class="span4 item-name">
                                    <span>Category Name:</span>
                                </div>
                                <div class="span4">
                                    <input type="text" value="Enquiry Type"/>
                                </div>
                                <div class=" span2">
                                    <span class="margin-right">On</span>
                                    <input type="radio" name="status" checked=""/>
                                </div>
                                <div class="span2">
                                    <span class="margin-right">Off</span>
                                    <input type="radio" name="status" />
                                </div>
                                <div class="span2"><?php echo get_input_button("default-btn save", "change-category", "Change") ?></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- ADD CHOICE -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">CHOICES</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Add / Remove category choices</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="item-holder">
                                <div class="span4 item-name">
                                    <span>Choice:</span>
                                </div>
                                <div class="span8">
                                    <input type="text" value=""/>
                                </div>
                                <div class="span2"><?php echo get_input_button("default-btn save", "add-choice", "Add") ?></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- Choices -->
                <div class="content-holder">
                    <div class="table-container">
                        <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                            <table id="ticketlist" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>CHOICES</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Billing</td>
                                        <td>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Billing & Invoicing</td>
                                        <td>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Free Trial</td>
                                        <td>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Free Trial: Prospect Enquiries</td>
                                        <td>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Free Trial: Website Notification</td>
                                        <td>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Ignore</td>
                                        <td>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>Internal: Instance deletion</td>
                                        <td>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
//array_push($btn_array, get_button("footer-btn save", "save", "Add New Filter", "filters-add_form"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>