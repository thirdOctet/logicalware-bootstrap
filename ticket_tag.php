<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">TAGS</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Add / Remove tags</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="item-holder">
                                <div class="span4 item-name">
                                    <span>Tag Name:</span>
                                </div>
                                <div class="span4">
                                    <input type="text" value=""/>
                                </div>
                                <div class="span2"><?php echo get_input_button("default-btn save", "add-tag", "Add") ?></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- Choices -->
                <div class="content-holder">
                    <div class="table-container">
                        <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                            <table id="ticketlist" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>TAGS</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>test</td>
                                        <td>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
//array_push($btn_array, get_button("footer-btn save", "save", "Add New Filter", "filters-add_form"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>