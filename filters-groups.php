<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content" class="">
        <div class="default-padding">
            <form id="" method="post" accept-charset="utf-8">
                <!-- NEW FILTERS GROUP -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">FILTERS GROUP</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="item-holder">
                                <div class="span4 item-name">Group Name:</div>
                                <div class="span14 child">
                                    <input type="text" value="" />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="span4 item-name">Priority:</div>
                                <div class="span3 child">
                                    <input type="text" value="" />
                                </div>
                                <div class="span3 text-center">(0 is highest priority)</div>
                                <div class="span2"><?php echo get_input_button("default-btn save full-width", "append", "Append"); ?></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- Filters table -->
                <div class="content-holder">
                    <div class="table-container">
                        <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                            <table id="ticketlist" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>GROUP NAME</th>
                                        <th>PRIORITY</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead
                                <tbody>
                                    <tr>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            Spam Capture
                                        </td>
                                        <td>
                                            <span>0</span>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "filters-groupform"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr><tr>
                                        <td>
                                            2
                                        </td>
                                        <td>
                                            System
                                        </td>
                                        <td>
                                            <span>0</span>
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "filters-groupform"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php
    $btn_array = array();
    array_push($btn_array, get_input_button("footer-btn save", "save", "Save"));
    echo get_footer($btn_array);
    ?>
</div>
<?php include('views/footer.php'); ?>