<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>    
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                
                <!-- ticket data -->
                <div class="content-holder">
                    <div class="table-container">
                        <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                            <table id="ticketlist" class="table table-striped">                                
                                <thead>
                                    <tr>                                        
                                        <th>#</th>
                                        <th>GROUP</th>
                                        <th>USERS</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead
                                <tbody>                                    
                                    <tr>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            <strong>Billing &amp; Invoicing</strong>
                                        </td>
                                        <td>
                                            <span>david</span>
                                            
                                            <span>jonathan</span>
                                            
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "groups-update_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr><tr>
                                        <td>
                                            2
                                        </td>
                                        <td>
                                            <strong>Free Trials</strong>
                                        </td>
                                        <td>
                                            <span>david</span>
                                            
                                            <span>igor</span>
                                            
                                            <span>jonathan</span>
                                            
                                            <span>will</span>
                                            
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "groups-update_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr><tr>
                                        <td>
                                            3
                                        </td>
                                        <td>
                                            <strong>Sales &amp; Enquiries</strong>
                                        </td>
                                        <td>
                                            <span>david</span>
                                            
                                            <span>jonathan</span>
                                            
                                            <span>will</span>
                                            
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "groups-update_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr><tr>
                                        <td>
                                            4
                                        </td>
                                        <td>
                                            <strong>Support</strong>
                                        </td>
                                        <td>
                                            <span>david</span>
                                            
                                            <span>igor</span>
                                            
                                            <span>jonathan</span>
                                            
                                        </td>
                                        <td>
                                            <?php echo get_button("default-btn", "", "Edit", "groups-update_form"); ?>
                                            <?php echo get_button("default-btn delete", "", "Delete", ""); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>           
        </div>            
    </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_button('footer-btn create', 'new-group', "Add New Group", "groups-add_form"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>