<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="content-holder">
                    <div class="layout-helper">
                        <div class="span6 settings-title">Templates</div>
                        <div class="span18">Set up templates for your users to use when replying to emails</div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="content-holder hide">
                    <div class="ticket-holder">
                        <div class="ticket-header">
                            <span class="assigned align-left default-padding padding-top-bottom">NEW TEMPLATE</span>
                            <span class="ticket-arrow-assigned align-left"></span>
                            <div class="clearfix"></div>
                        </div>
                        <div class="ticket-content default-padding">
                            <!-- template name -->
                            <div class="item-holder">
                                <div class="span4">
                                    <span class="item-name">Template Name:</span>
                                </div>
                                <div class="span12">
                                    <span class="item-input">
                                        <input type="text" value="">
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!-- Textarea -->
                            <div class="item-holder">
                                <div class="span4">
                                    <span class="item-name">Template Body:</span>
                                </div>
                                <div class="span20">
                                    <span class="item-input">
                                        <textarea id="email-reply"></textarea>
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-holder">
                    <div class="table-container">
                        <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                            <table id="ticketlist" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ACTIVE TEMPLATES</th>
                                        <th>TEXT</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Auto response: Old Employees</td>
                                        <td>Attention:  You are receiving this email as this person you ....</td>                                                                                     <td>
                                            <?php
                                            echo get_button("default-btn", "edit", "Edit", "template-edit_form");
                                            echo get_button("default-btn delete", "delete", "Delete", "index_html");
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Auto response: Old Employees</td>
                                        <td>Attention:  You are receiving this email as this person you ....</td>                                                                                     <td>
                                            <?php
                                            echo get_button("default-btn", "edit", "Edit", "template-edit_form");
                                            echo get_button("default-btn delete", "delete", "Delete", "index_html");
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Auto response: Old Employees</td>
                                        <td>Attention:  You are receiving this email as this person you ....</td>                                                                                     <td>
                                            <?php
                                            echo get_button("default-btn", "edit", "Edit", "template-edit_form");
                                            echo get_button("default-btn delete", "delete", "Delete", "index_html");
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Auto response: Old Employees</td>
                                        <td>Attention:  You are receiving this email as this person you ....</td>                                                                                     <td>
                                            <?php
                                            echo get_button("default-btn", "edit", "Edit", "template-edit_form");
                                            echo get_button("default-btn delete", "delete", "Delete", "index_html");
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Auto response: Old Employees</td>
                                        <td>Attention:  You are receiving this email as this person you ....</td>                                                                                     <td>
                                            <?php
                                            echo get_button("default-btn", "edit", "Edit", "template-edit_form");
                                            echo get_button("default-btn delete", "delete", "Delete", "index_html");
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Auto response: Old Employees</td>
                                        <td>Attention:  You are receiving this email as this person you ....</td>                                                                                     <td>
                                            <?php
                                            echo get_button("default-btn", "edit", "Edit", "template-edit_form");
                                            echo get_button("default-btn delete", "delete", "Delete", "index_html");
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>Auto response: Old Employees</td>
                                        <td>Attention:  You are receiving this email as this person you ....</td>                                                                                     <td>
                                            <?php
                                            echo get_button("default-btn", "edit", "Edit", "template-edit_form");
                                            echo get_button("default-btn delete", "delete", "Delete", "index_html");
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>Auto response: Old Employees</td>
                                        <td>Attention:  You are receiving this email as this person you ....</td>                                                                                     <td>
                                            <?php
                                            echo get_button("default-btn", "edit", "Edit", "template-edit_form");
                                            echo get_button("default-btn delete", "delete", "Delete", "index_html");
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>Auto response: Old Employees</td>
                                        <td>Attention:  You are receiving this email as this person you ....</td>                                                                                     <td>
                                            <?php
                                            echo get_button("default-btn", "edit", "Edit", "template-edit_form");
                                            echo get_button("default-btn delete", "delete", "Delete", "index_html");
                                            ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_input_button("footer-btn save", "add-new-template", "Add New Template"));
array_push($btn_array, get_input_button("footer-btn cancel hide", "cancel-template", "Cancel"));
array_push($btn_array, get_input_button("footer-btn save hide", "save-template", "Save"));
echo get_footer($btn_array,'template');
?>
</div>
<?php include('views/footer.php'); ?>