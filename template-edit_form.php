<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="content-holder">
                    <div class="ticket-holder">
                        <div class="ticket-header">
                            <span class="assigned align-left default-padding padding-top-bottom">TEMPLATE</span>
                            <span class="ticket-arrow-assigned align-left"></span>
                            <div class="clearfix"></div>
                        </div>
                        <div class="ticket-content default-padding show">
                            <!-- template name -->
                            <div class="item-holder">
                                <div class="span4">
                                    <span class="item-name">Name:</span>
                                </div>
                                <div class="span12">
                                    <span class="item-input">
                                        <input type="text" value="Some template title">
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!-- Textarea -->
                            <div class="item-holder">
                                <div class="span4">
                                    <span class="item-name">Body:</span>
                                </div>
                                <div class="span20">
                                    <span class="item-input">
                                        <textarea id="email-reply"> some template content</textarea>
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_input_button("footer-btn delete", "cancel", "cancel"));
array_push($btn_array, get_input_button("footer-btn save", "new-template", "save"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>