<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php //include('views/inner-menu-tickets.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="content-holder">
                <div class="row-fluid">
                    <div class="filterview display-inline-block">
                        <div class="display-inline-block">
                            <div id="ticket-filter">
                                <p id="" class="menu-header null-margin-top null-margin-bottom">Queue Filter List</p>
                                <div class="menu-section default-shadow">
                                    <!--<span class="up-arrow"></span>-->
                                    <form id="" accept-charset="utf-8">
                                        <span class="display-inline-block">Queues</span>
                                        <select id="header_account_name" name="value">
                                            <option value="">Billing & Invoicing</option>
                                            <option value="billing@free trials.com">Free Trials</option>
                                            <option value="enquiries@free trials.com">Sales & Enquiries</option>
                                            <option value="lwtest@my.free trials.net">Support</option>
                                            <option value="sales@free trials.com">TRIALS</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="divider display-inline-block">
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                        </div>
                        <!-- next previous buttons -->
                        <div class="display-inline-block">
                            <!-- previous page -->
                            <div id="previous-page" class="display-inline-block">
                                <?php
                                if (!empty($_SERVER['HTTP_REFERER'])) {
                                    echo "<a href='" . $_SERVER['HTTP_REFERER'] . "'>";
                                }
                                ?>
                                <div class="ticket-helper">
                                    <span class="back-arrow"></span>
                                    <span class="back_rectangle"></span>
                                </div>
                                <?php
                                if (!empty($_SERVER['HTTP_REFERER'])) {
                                    echo "</a>";
                                }
                                ?>
                            </div>
                            <div class="display-inline-block">
                                <div class="icon_container" id="viewswitch">
                                    <i class="fa fa-bars"></i>
                                </div>
                            </div>
                            <div class="divider display-inline-block">
                                <div><i class="fa fa-circle"></i></div>
                                <div><i class="fa fa-circle"></i></div>
                                <div><i class="fa fa-circle"></i></div>
                                <div><i class="fa fa-circle"></i></div>
                                <div><i class="fa fa-circle"></i></div>
                                <div><i class="fa fa-circle"></i></div>
                            </div>
                            <div class="ticket-helper">
                                <div id="first">
                                    <span class="ticket-rectangle-block"></span>
                                    <span class="ticket-left-arrow"></span>
                                </div>
                            </div>
                            <div class="ticket-helper">
                                <div id="previous">
                                    <span class="ticket-left-arrow"></span>
                                </div>
                            </div>
                            <div class="ticket-helper">
                                <div id="next">
                                    <span class="ticket-right-arrow"></span>
                                </div>
                            </div>
                            <div class="ticket-helper">
                                <div id="last">
                                    <span class="ticket-right-arrow"></span>
                                    <span class="ticket-rectangle-block"></span>
                                </div>
                            </div>
                        </div>
                        <!-- divider -->
                        <div class="divider display-inline-block">
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                        </div>
                        <!-- pagination -->
                        <div id="pagination" class="display-inline-block">
                            <div class="ticket-helper ticket-list-active">
                                <div class="number">1</div>
                            </div>
                            <div class="ticket-helper">
                                <div class="number">2</div>
                            </div>
                            <div class="ticket-helper">
                                <div class="number">3</div>
                            </div>
                            <div class="ticket-helper">
                                <div class="number">.</div>
                            </div>
                            <div class="ticket-helper">
                                <div class="number">.</div>
                            </div>
                            <div class="ticket-helper">
                                <div class="number">23</div>
                            </div>
                            <div class="ticket-helper">
                                <div class="number">24</div>
                            </div>
                            <div class="ticket-helper">
                                <div class="number">25</div>
                            </div>
                        </div>
                        <!-- divider -->
                        <div class="divider display-inline-block">
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                        </div>
                        <!-- number of tickets -->
                        <div class="display-inline-block">Tickets:
                            <span id="ticket-number">100</span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- ticket data -->
            <div class="content-holder">
                <div class="table-container">
                    <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                        <table id="ticketlist" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" id="selectall">
                                    </th>
                                    <th>
                                        <a href="nticket">Ticket</a>
                                        <span id="arrow-down" class="down-arrow"></span>
                                    </th>
                                    <th>
                                        <a href="nticket">Status</a>
                                    </th>
                                    <th>
                                        <a href="nticket">Target</a>
                                    </th>
                                    <th>
                                        <a href="nticket">Queue</a>
                                    </th>
                                    <th>
                                        <a href="nticket">Subject</a>
                                    </th>
                                    <th>
                                        <a href="nticket">From</a>
                                    </th>
                                    <th>
                                        <a href="nticket">Account</a>
                                    </th>
                                    <th>
                                        <a href="nticket">Time</a>
                                    </th>
                                    <th>
                                        <a href="nticket">Date</a>
                                    </th>
                                    <th>
                                        <a href="nticket">Date closed</a>
                                    </th>
                                    <th>
                                        <a href="nticket">Priority</a>
                                    </th>
                                    <th>
                                        <a href="nticket">Note</a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3769">
                                    </td>
                                    <td><a href="nticket">003769</a></td>
                                    <td class="status-width"><a class="status open" href="nticket">Open</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">Billing & Invoicing</a></td>
                                    <td><a href="nticket">Payment for Logicalware Invoice # 446</a></td>
                                    <td><a href="nticket">BigBadToyStore via PayPal</a></td>
                                    <td><a href="nticket">billing@free trials.com</a></td>
                                    <td><a href="nticket">01:27</a></td>
                                    <td><a href="nticket">04.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3769">
                                    </td>
                                    <td><a href="nticket">003769</a></td>
                                    <td class="status-width"><a class="status archived" href="nticket">Archived</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">Billing & Invoicing</a></td>
                                    <td><a href="nticket">Payment for Logicalware Invoice # 446</a></td>
                                    <td><a href="nticket">BigBadToyStore via PayPal</a></td>
                                    <td><a href="nticket">billing@free trials.com</a></td>
                                    <td><a href="nticket">01:27</a></td>
                                    <td><a href="nticket">04.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3769">
                                    </td>
                                    <td><a href="nticket">003769</a></td>
                                    <td class="status-width"><a class="status spam" href="nticket">Spam</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">Billing & Invoicing</a></td>
                                    <td><a href="nticket">Payment for Logicalware Invoice # 446</a></td>
                                    <td><a href="nticket">BigBadToyStore via PayPal</a></td>
                                    <td><a href="nticket">billing@free trials.com</a></td>
                                    <td><a href="nticket">01:27</a></td>
                                    <td><a href="nticket">04.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3769">
                                    </td>
                                    <td><a href="nticket">003769</a></td>
                                    <td class="status-width"><a class="status overdue" href="nticket">Overdue</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">Billing & Invoicing</a></td>
                                    <td><a href="nticket">Payment for Logicalware Invoice # 446</a></td>
                                    <td><a href="nticket">BigBadToyStore via PayPal</a></td>
                                    <td><a href="nticket">billing@free trials.com</a></td>
                                    <td><a href="nticket">01:27</a></td>
                                    <td><a href="nticket">04.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3769">
                                    </td>
                                    <td><a href="nticket">003769</a></td>
                                    <td class="status-width"><a class="status hold" href="nticket">Hold</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">Billing & Invoicing</a></td>
                                    <td><a href="nticket">Payment for Logicalware Invoice # 446</a></td>
                                    <td><a href="nticket">BigBadToyStore via PayPal</a></td>
                                    <td><a href="nticket">billing@free trials.com</a></td>
                                    <td><a href="nticket">01:27</a></td>
                                    <td><a href="nticket">04.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3767">
                                    </td>
                                    <td><a href="nticket">003767</a></td>
                                    <td class="status-width"><a class="status closed"  href="nticket">Closed</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">free trials</a></td>
                                    <td><a href="nticket">Action for Children Remittance Advice</a></td>
                                    <td><a href="nticket">Lana Tavares</a></td>
                                    <td><a href="nticket">unknown@free trials.com</a></td>
                                    <td><a href="nticket">16:46</a></td>
                                    <td><a href="nticket">03.07.13</a></td>
                                    <td><a href="nticket">03.07.13 16:46</a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3766">
                                    </td>
                                    <td><a href="nticket">003766</a></td>
                                    <td class="status-width"><a class="status closed"  href="nticket">Closed</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">Sales & Enquiries</a></td>
                                    <td><a href="nticket">Quote</a></td>
                                    <td><a href="nticket">Michael Hutcheson</a></td>
                                    <td><a href="nticket">sales@free trials.com</a></td>
                                    <td><a href="nticket">13:55</a></td>
                                    <td><a href="nticket">03.07.13</a></td>
                                    <td><a href="nticket">03.07.13 14:56</a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr class="">
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3765">
                                    </td>
                                    <td><a href="nticket">003765</a></td>
                                    <td class="status-width"><a class="status open" href="nticket">Open</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">Billing & Invoicing</a></td>
                                    <td><a href="nticket">Remittance</a></td>
                                    <td><a href="nticket">Melanie Foster</a></td>
                                    <td><a href="nticket">billing@free trials.com</a></td>
                                    <td><a href="nticket">13:44</a></td>
                                    <td><a href="nticket">03.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3764">
                                    </td>
                                    <td><a href="nticket">003764</a></td>
                                    <td class="status-width"><a class="status closed"  href="nticket">Closed</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">6:12</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">Support</a></td>
                                    <td><a href="nticket">@deleteuser: request for delete user</a></td>
                                    <td><a href="nticket">customerservicesteammanagers1@purecollection.com</a></td>
                                    <td><a href="nticket">support@free trials.com</a></td>
                                    <td><a href="nticket">13:27</a></td>
                                    <td><a href="nticket">03.07.13</a></td>
                                    <td><a href="nticket">03.07.13 13:30</a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr class="">
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3763">
                                    </td>
                                    <td><a href="nticket">003763</a></td>
                                    <td class="status-width"><a class="status open" href="nticket">Open</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">Billing & Invoicing</a></td>
                                    <td><a href="nticket">RE: Logicalware Ltd - Invoice #542 for services ...</a></td>
                                    <td><a href="nticket">Robert Sawicki</a></td>
                                    <td><a href="nticket">billing@free trials.com</a></td>
                                    <td><a href="nticket">12:13</a></td>
                                    <td><a href="nticket">03.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php include('views/footer-queues_list-single.php'); ?>
</div>
<?php include('views/footer.php'); ?>