<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-ticket-response.php'); ?>
    <div id="content">
        <div class="default-padding">

            <!-- Ticket -->
            <div class="content-holder">
                <div class="ticket-holder">
                    <div class="ticket-header">
                        <span class="assigned pull-left default-padding padding-top-bottom">user@emailaddress.com</span><span class="ticket-arrow-assigned pull-left"></span>
                        <span class="subject pull-left padding-top-bottom">TID 003780 Re: @deleteuser: request for delete user</span><span class="ticket-arrow-subject pull-left"></span>
                        <span class="date-time align-right default-padding padding-top-bottom">09/04/13 @ 11:00</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding">
                        <div class="ticket-subject">
                            <span class="subject-header">Subject:</span>
                            <span class="subject-text">TID 003780 Re: @deleteuser: request for delete user</span>
                        </div>
                        <div class="ticket-from">
                            <span class="subject-header">From:</span>
                            <span class="subject-text">support@logicalware.com</span>
                        </div>
                        <div class="ticket-to">
                            <span class="subject-header">To:</span>
                            <span class="subject-text">support@logicalware.com</span>
                        </div>
                        <div class="ticket-message">
                            <span class="subject-header">Message:</span>
                            <span class="subject-text">
                                <p>To whom it may concern: </p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    Duis aute irure dolor in reprehenderit in voluptate velit
                                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                                    occaecat cupidatat non proident, sunt in culpa qui officia
                                    deserunt mollit anim id est laborum.
                                </p>
                                <p>Kind Regards</p>
                                <p>Customer Support</p>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Ticket Note -->
            <div class='content-holder'>
                <div class='ticket-note-holder'>
                    <div class="ticket-note-header default-padding padding-top-bottom">
                        <span class="note pull-left">Note:</span>
                        <span class="note-user pull-left">Jonathan</span>
                        <span class="note-text pull-left">The Note information</span>
                        <span class="note-time align-right"> 15/12/2003 @ 15:30</span>
                        <div class='clearfix'></div>
                    </div>
                    <div class="ticket-note-content default-padding">
                        <div class="ticket-message">
                            <span class="subject-text">
                                <p>To whom it may concern:</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    Duis aute irure dolor in reprehenderit in voluptate velit
                                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                                    occaecat cupidatat non proident, sunt in culpa qui officia
                                    deserunt mollit anim id est laborum.
                                </p>
                                <p>Kind Regards</p>
                                <p>End of note</p>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Ticket -->
            <div class="content-holder">
                <div class="ticket-holder">
                    <div class="ticket-header">
                        <span class="assigned pull-left default-padding padding-top-bottom">Timets</span><span class="ticket-arrow-assigned pull-left"></span>
                        <span class="subject pull-left  padding-top-bottom">Some subject header</span>
                        <span class="ticket-arrow-subject pull-left"></span>
                        <span class="date-time align-right default-padding padding-top-bottom">09/04/13 @ 11:00</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding">
                        <div class="ticket-subject">
                            <span class="subject-header">Subject:</span>
                            <span class="subject-text">TID 003780 Re: @deleteuser: request for delete user</span>
                        </div>
                        <div class="ticket-from">
                            <span class="subject-header">From:</span>
                            <span class="subject-text">support@logicalware.com</span>
                        </div>
                        <div class="ticket-to">
                            <span class="subject-header">To:</span>
                            <span class="subject-text">support@logicalware.com</span>
                        </div>
                        <div class="ticket-message">
                            <span class="subject-header">Message:</span>
                            <span class="subject-text">
                                <p>To whom it may concern: </p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    Duis aute irure dolor in reprehenderit in voluptate velit
                                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                                    occaecat cupidatat non proident, sunt in culpa qui officia
                                    deserunt mollit anim id est laborum.
                                </p>
                                <p>Kind Regards</p>
                                <p>Customer Support</p>
                            </span>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Ticket Note -->
            <div class='content-holder'>
                <div class='ticket-note-holder'>
                    <div class="ticket-note-header default-padding padding-top-bottom">
                        <span class="note pull-left">Note:</span>
                        <span class="note-user pull-left">Jonathan</span>
                        <span class="note-text pull-left">The Note information</span>
                        <span class="note-time align-right"> 15/12/2003 @ 15:30</span>
                        <div class='clearfix'></div>
                    </div>
                    <div class="ticket-note-content default-padding">
                        <div class="ticket-message">
                            <span class="subject-text">
                                <p>To whom it may concern: </p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    Duis aute irure dolor in reprehenderit in voluptate velit
                                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                                    occaecat cupidatat non proident, sunt in culpa qui officia
                                    deserunt mollit anim id est laborum.
                                </p>
                                <p>Kind Regards</p>
                                <p>Customer Support</p>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Ticket -->
            <div class="content-holder">
                <div class="ticket-holder">
                    <div class="ticket-header">
                        <span class="assigned pull-left default-padding padding-top-bottom">Timets</span><span class="ticket-arrow-assigned pull-left"></span>
                        <span class="subject pull-left  padding-top-bottom">Some subject header</span>
                        <span class="ticket-arrow-subject pull-left"></span>
                        <span class="date-time align-right default-padding padding-top-bottom">09/04/13 @ 11:00</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding">
                        <div class="ticket-subject">
                            <span class="subject-header">Subject:</span>
                            <span class="subject-text">TID 003780 Re: @deleteuser: request for delete user</span>
                        </div>
                        <div class="ticket-from">
                            <span class="subject-header">From:</span>
                            <span class="subject-text">support@logicalware.com</span>
                        </div>
                        <div class="ticket-to">
                            <span class="subject-header">To:</span>
                            <span class="subject-text">support@logicalware.com</span>
                        </div>
                        <div class="ticket-message">
                            <span class="subject-header">Message:</span>
                            <span class="subject-text">
                                <p>To whom it may concern: </p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    Duis aute irure dolor in reprehenderit in voluptate velit
                                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                                    occaecat cupidatat non proident, sunt in culpa qui officia
                                    deserunt mollit anim id est laborum.
                                </p>
                                <p>Kind Regards</p>
                                <p>Customer Support</p>
                            </span>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Ticket Note -->
            <div class='content-holder'>
                <div class='ticket-note-holder'>
                    <div class="ticket-note-header default-padding padding-top-bottom">
                        <span class="note pull-left">Note:</span>
                        <span class="note-user pull-left">Jonathan</span>
                        <span class="note-text pull-left">The Note information</span>
                        <span class="note-time align-right"> 15/12/2003 @ 15:30</span>
                        <div class='clearfix'></div>
                    </div>
                    <div class="ticket-note-content default-padding">
                        <div class="ticket-message">
                            <span class="subject-text">
                                <p>To whom it may concern: </p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    Duis aute irure dolor in reprehenderit in voluptate velit
                                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                                    occaecat cupidatat non proident, sunt in culpa qui officia
                                    deserunt mollit anim id est laborum.
                                </p>
                                <p>Kind Regards</p>
                                <p>Customer Support</p>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Ticket -->
            <div id="last-ticket" class="content-holder">
                <div class="ticket-holder">
                    <div class="ticket-header">
                        <span class="assigned pull-left default-padding padding-top-bottom">user@emailaddress.com</span><span class="ticket-arrow-assigned pull-left"></span>
                        <span class="subject pull-left  padding-top-bottom">TID 003780 Re: @deleteuser: request for delete user</span><span class="ticket-arrow-subject pull-left"></span>
                        <span class="date-time align-right default-padding padding-top-bottom">09/04/13 @ 11:00</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="ticket-subject">
                            <span class="subject-header">Subject:</span>
                            <span class="subject-text">TID 003780 Re: @deleteuser: request for delete user</span>
                        </div>
                        <div class="ticket-from">
                            <span class="subject-header">From:</span>
                            <span class="subject-text">support@logicalware.com</span>
                        </div>
                        <div class="ticket-to">
                            <span class="subject-header">To:</span>
                            <span class="subject-text">support@logicalware.com</span>
                        </div>
                        <div class="ticket-message">
                            <span class="subject-header">Message:</span>
                            <span class="subject-text">
                                <p>To whom it may concern: </p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    Duis aute irure dolor in reprehenderit in voluptate velit
                                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                                    occaecat cupidatat non proident, sunt in culpa qui officia
                                    deserunt mollit anim id est laborum.
                                </p>
                                <p>Kind Regards</p>
                                <p>Customer Support</p>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Ticket Response -->
            <div class="content-holder ticket-response">
                <div class="row-fluid">

                    <!-- SUBJECT field -->
                    <div id="subject-field">
                        <div class="span3">
                            <span class="subject-header">Subject:</span>
                        </div>
                        <div class="offset1 span12">
                            <input type="text" />
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <!-- TO field -->
                    <div id="to-field">
                        <div class="span3">
                            <span class="subject-header">To:</span>
                        </div>
                        <div class="span1">
                            <img id="email-search" src="img/icon-search.png" class=""/>
                        </div>
                        <div class="span12">
                            <input type="text" />
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <!-- CC field -->
                    <div id="cc-field" class="hide">
                        <div class="span3">
                            <span class="subject-header">CC:</span>
                        </div>
                        <div class="span1">
                            <img id="email-search" src="img/icon-search.png" class=""/>
                        </div>
                        <div class="span12">
                            <input type="text" />
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <!-- BCC field -->
                    <div id="bcc-field" class="hide">
                        <div class="span3">
                            <span class="subject-header">BCC:</span>
                        </div>
                        <div class="span1">
                            <img id="email-search" src="img/icon-search.png" class=""/>
                        </div>
                        <div class="span12">
                            <input type="text" />
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <!-- RESPONSE attributes -->
                    <div class="response-attributes">
                        <div class="offset4 span12">
                            <span id="ccBtn">Add CC</span>
                            <span id="bccBtn">Add BCC</span>
                            <span id="templateBtn">Use Template</span>
                            <span id="personalTemplateBtn">Use Personal Template</span>
                            <span id="attachFilesBtn">Attach files</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <!-- Use Template -->
                    <div id="template" class="hide margin-bottom-5px">
                        <div class="offset4 span12">
                            <div>
                                <i class="fa fa-plus"></i><span>Auto Responses</span>
                            </div>
                            <ul class="hide list">
                                <li><i class="fa fa-minus"></i>Auto response:support</li>
                            </ul>
                            <div><i class="fa fa-plus"></i><span>Contract Modifications</span></div>
                            <ul class="hide list">
                                <li><i class="fa fa-minus"></i>Contract - Termination</li>
                                <li><i class="fa fa-minus"></i>Contract - User Decrease</li>
                                <li><i class="fa fa-minus"></i>Contract - User Increase</li>
                            </ul>
                            <div><i class="fa fa-minus"></i><span>Auto response: Old Employees</span></div>
                            <div><i class="fa fa-minus"></i><span>Users - Delete</span></div>
                            <div><i class="fa fa-minus"></i><span>Cite all previous messages</span></div>
                            <div><i class="fa fa-minus"></i><span>Cite last message</span></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <!-- Use Personal Template -->
                    <div id="personalTemplate" class="hide margin-bottom-5px">
                        <div class="span4">
                            <span class="subject-header">Tags:</span>
                        </div>
                        <div class="span12">
                            <form id="" method="" action="">
                                <div class="span20">
                                    <input type="text" name="" value="*" />
                                </div>
                                <div class="span4">
                                    <input type="submit" class="default-btn full-width save" value ="search" />
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <!-- Attach files -->
                    <div id="attachFiles" class="hide">
                        <div id="fileupload" class="margin-bottom-5px">
                            <div class="span4">
                                <span class="subject-header">File:</span>
                            </div>
                            <div class="span12">
                                <input type="file" class=""/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="standard_attachment margin-bottom-5px">
                            <div class="span4">
                                <span class="subject-header">Standard Attachment:</span>
                            </div>
                            <div class="span10">
                                <select id="standard_attach">
                                    <option value=""></option>
                                    <option value="mbox.txt">mbox.txt</option>
                                </select>
                            </div>
                            <div class="span2">
                                <?php echo get_input_button("default-btn full-width save", "attach", "Attach") ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="tinymce-response">
                        <textarea name="" id="email-reply"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('views/footer-tickets-response.php'); ?>
</div>
<?php include('views/footer.php'); ?>
