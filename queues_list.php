<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php // include('views/inner-menu-tickets.php'); ?>    
    <div id="content">
        <div class="default-padding">
            <div class='row-fluid'>
                <div>
                    <div class="content-holder">
                        <div class="table-container">                    
                            <table id="" class="table table-striped">
                                <thead>
                                    <tr>                               
                                        <th>QUEUE NAME</th>
                                        <th>TICKET COUNT</th>
                                        <th>OLDEST TICKET</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>                       
                                    <tr>
                                        <td><a href="queues_list-single">Billing & Invoicing</a></td>
                                        <td><a href="queues_list-single">30</a></td>
                                        <td><a href="queues_list-single">12:05 - 19.08.13</a></td>
                                        <td><a class="status default-btn" id="" href="nticket">Oldest Ticket</a></td>
                                    </tr> 
                                    <tr>
                                        <td><a href="queues_list-single">Support</a></td>
                                        <td><a href="queues_list-single">20</a></td>
                                        <td><a href="queues_list-single">11:08 - 11.07.12</a></td>
                                        <td><a class="status default-btn" id="" href="nticket">Oldest Ticket</a></td>
                                    </tr>  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
<!--                <div class='span6'>
                    <div class="content-holder">
                        <div class="table-container">                    
                            <table id="" class="table table-striped">
                                <thead>
                                    <tr>                               
                                        <th>QUEUES FOR JONATHAN</th>
                                        <th>OPEN</th>
                                        <th></th>                                
                                    </tr>
                                </thead>
                                <tbody>                                
                                    <?php
                                    for ($i = 0; $i < 1; $i++) {
                                        echo '<tr>';
                                        echo '<td><a href="queues_list-single">billing@logicalware.com</a></td>';
                                        echo '<td><a href="queues_list-single">1</a></td>';
                                        echo '<td><a class="status default-btn" id="">Oldest Ticket</a></td>';
                                        echo '</tr >';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>             
                    </div>
                </div>-->
                <div class='clearfix'></div>
            </div>
        </div>
    </div>
    <?php include('views/footer-blank.php'); ?>
</div>
<?php include('views/footer.php'); ?>