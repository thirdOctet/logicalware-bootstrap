<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <!-- SET MESSAGE SOURCE -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">QUEUE</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Edit Queue</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="item-holder">
                                    <div class="span6 item-name">Queue Name: *</div>
                                    <div class="span18">
                                        <input type="text" value="Billing &amp; Invoicing" />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="item-holder">
                                    <div class="span6 item-name">Assigned User Group:</div>
                                    <div class="span18">
                                        <select id="assign_group" name="assign_group">
                                            <option value="Billing &amp; Invoicing" selected="">Billing &amp; Invoicing</option>
                                            <option value="Free Trials">Free Trials</option>
                                            <option value="Sales &amp; Enquiries">Sales &amp; Enquiries</option>
                                            <option value="Support">Support</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<?php

$btn_array = array();

array_push($btn_array, get_input_button("footer-btn save", "save", "Save"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>