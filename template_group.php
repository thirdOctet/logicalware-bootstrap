<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <div class="content-holder">
                    <div class="ticket-holder">
                        <div class="ticket-header">
                            <span class="assigned align-left default-padding padding-top-bottom">TEMPLATES GROUP</span>
                            <span class="ticket-arrow-assigned align-left"></span>
                            <div class="clearfix"></div>
                        </div>
                        <div class="ticket-content default-padding show">
                            <!-- template name -->
                            <div class="item-holder">
                                <div class="span4">
                                    <span class="item-name">Group Name:</span>
                                </div>
                                <div class="span12">
                                    <span class="item-input">
                                        <input type="text" value="">
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="item-holder">
                                <div class="offset4 span1">
                                    <a href="template_group-edit_form"><?php echo get_input_button("footer-btn save", "append", "Add"); ?></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-holder">
                    <div class="table-container">
                        <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                            <table id="ticketlist" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>GROUP NAME</th>
                                        <th>PRIVATE</th>
                                        <th>TEMPLATES IN GROUP</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Auto responses</td>
                                        <td>
                                            <i class="fa fa-times fa-2x"></i>
                                        </td>
                                        <td>
                                            <ul class="null-margin">
                                                <li>
                                                    Auto response: Support
                                                </li>
                                            </ul>
                                        </td>
                                        <td>
                                            <?php
                                            echo get_button("default-btn", "edit", "Edit", "template_group-edit_form");
                                            echo get_button("default-btn delete", "delete", "Delete", "index_html");
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Contract modifications</td>
                                        <td>
                                            <i class="fa fa-times fa-2x"></i>
                                        </td>
                                        <td>
                                            <ul class="null-margin">
                                                <li>Contract - Termination</li>
                                                <li>Contract - User Decrease</li>
                                                <li>Contract - User Increase</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <?php
                                            echo get_button("default-btn", "edit", "Edit", "template_group-edit_form");
                                            echo get_button("default-btn delete", "delete", "Delete", "index_html");
                                            ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo get_footer(); ?>
</div>
<?php include('views/footer.php'); ?>