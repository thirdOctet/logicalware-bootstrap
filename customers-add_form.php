<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <?php include('views/inner-menu-settings.php'); ?>
    <div id="content">
        <div class="default-padding">
            <div class="row-fluid">
                <!-- ADD CHOICE -->
                <div class="content-holder">
                    <div class="ticket-header">
                        <span class="assigned align-left default-padding padding-top-bottom">ADD NEW CUSTOMER</span>
                        <span class="ticket-arrow-assigned align-left"></span>
                        <span class="subject align-left padding-top-bottom">Set customers details</span>
                        <span class="ticket-arrow-subject align-left"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ticket-content default-padding show">
                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Full Name:</span>
                            </div>
                            <div class="span14">
                                <input type="text" value=""/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Username: *</span>
                            </div>
                            <div class="span14">
                                <input type="text" value="jonathan"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Password:</span>
                            </div>
                            <div class="span14">
                                <input type="password"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Confirm Password:</span>
                            </div>
                            <div class="span14">
                                <input type="password" value=""/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Allow access to tickets from:</span>
                            </div>
                            <div class="span14">
                                <div class="hide add-email">
                                    <div class="item-holder">
                                        <div class="span10">
                                            <input type="text" value="" placeholder="email"/>
                                        </div>
                                        <div class="span10">
                                            <input type="text" value="" placeholder="comment"/>
                                        </div>
                                        <div class="span4">
                                            <?php echo get_input_button("default-btn delete full-width remove-email", "delete", "Delete"); ?>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <?php echo get_input_button("default-btn", "add-email", "Add Email"); ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="item-holder">
                            <div class="span4 item-name">
                                <span>Notes:</span>
                            </div>
                            <div class="span14">
                                <textarea rows="6"></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php
$btn_array = array();
array_push($btn_array, get_input_button("footer-btn cancel", "cancel", "Cancel"));
array_push($btn_array, get_input_button("footer-btn save", "save", "Save"));
echo get_footer($btn_array);
?>
</div>
<?php include('views/footer.php'); ?>