<?php include('views/header.php'); ?>
<?php include('views/sidebar.php'); ?>
<div id="content-container" class="">
    <?php include('views/search.php'); ?>
    <div id="content">
        <div class="default-padding">
            <!-- ticket menu and navigation holder -->
            <div class="content-holder">
                <div class="row-fluid">
                    <div class="filterview display-inline-block">
                        <div id="ticket-filter">
                            <p id="" class="menu-header null-margin-top null-margin-bottom">Ticket Filter List</p>
                            <div class="menu-section default-shadow">
                                <!-- <i class="fa fa-caret-up"></i> -->
                                <!-- <span class="up-arrow"></span> -->
                                <form id="" accept-charset="utf-8">
                                    <span class="display-inline-block">Account</span>
                                    <select id="header_account_name" name="value">
                                        <option value="">Show All</option>
                                        <option value="billing@logicalware.com">billing@logicalware.com</option>
                                        <option value="enquiries@logicalware.com">enquiries@logicalware.com</option>
                                        <option value="lwtest@my.logicalware.net">lwtest@my.logicalware.net</option>
                                        <option value="sales@logicalware.com">sales@logicalware.com</option>
                                        <option value="support@logicalware.com">support@logicalware.com</option>
                                        <option value="trials@logicalware.com">trials@logicalware.com</option>
                                        <option value="unknown@logicalware.com">unknown@logicalware.com</option>
                                    </select>
                                    <span class="display-inline-block">User</span>
                                    <select id="header_username_dropdown" name="value">
                                        <option value="">Show All</option>
                                        <option value="david">david</option>
                                        <option value="galtsev">galtsev</option>
                                        <option value="igor">igor</option>
                                        <option value="jonathan">jonathan</option>
                                        <option value="logicalware">logicalware</option>
                                        <option value="spamCollector">spamCollector</option>
                                        <option value="timets">timets</option>
                                        <option value="will">will</option>
                                    </select>
                                    <span class="display-inline-block">Status</span>
                                    <select name="value">
                                        <option value="" selected="">Show all</option>
                                        <option value="-">-</option>
                                        <option value="1: Acknowledged">1: Acknowledged</option>
                                        <option value="2: In Process">2: In Process</option>
                                        <option value="3: Complete">3: Complete</option>
                                    </select>
                                    <span class="display-inline-block">Enquiry Type</span>
                                    <select name="value">
                                        <option value="" selected="">Show all</option>
                                        <option value="-">-</option>
                                        <option value="Billing">Billing</option>
                                        <option value="Billing &amp; Invoicing">Billing &amp; Invoicing</option>
                                        <option value="Free Trial">Free Trial</option>
                                        <option value="Free Trial: Prospect Enquiries">Free Trial: Prospect Enquiries</option>
                                        <option value="Free Trial: Website Notification">Free Trial: Website Notification</option>
                                        <option value="Ignore">Ignore</option>
                                        <option value="Internal: Instance deletion">Internal: Instance deletion</option>
                                        <option value="Internal: Test">Internal: Test</option>
                                        <option value="Internal: User deletion">Internal: User deletion</option>
                                        <option value="Junk/Spam">Junk/Spam</option>
                                        <option value="Sales &amp; Enquiries">Sales &amp; Enquiries</option>
                                        <option value="Support">Support</option>
                                        <option value="Zuzana">Zuzana</option>
                                    </select>
                                </form>
                            </div>
                        </div>
                        <!-- divider -->

                        <div class="divider display-inline-block">
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                        </div>

                        <!-- next previous buttons -->
                        <div class="display-inline-block">
                            <div class="display-inline-block">
                            <div class="icon_container" id="viewswitch">
                                <i class="fa fa-bars"></i>
                            </div>
                        </div>
                            <div class="ticket-helper">
                                <div id="first">
                                    <span class="ticket-rectangle-block"></span>
                                    <span class="ticket-left-arrow"></span>
                                </div>
                            </div>
                            <div class="ticket-helper">
                                <div id="previous">
                                    <span class="ticket-left-arrow"></span>
                                </div>
                            </div>
                            <div class="ticket-helper">
                                <div id="next">
                                    <span class="ticket-right-arrow"></span>
                                </div>
                            </div>
                            <div class="ticket-helper">
                                <div id="last">
                                    <span class="ticket-right-arrow"></span>
                                    <span class="ticket-rectangle-block"></span>
                                </div>
                            </div>
                        </div>

                        <!-- divider -->
                        <div class="divider display-inline-block">
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                        </div>

                        <!-- pagination -->
                        <div id="pagination" class="display-inline-block">
                            <div class="ticket-helper ticket-list-active">
                                <div class="number">1</div>
                            </div>
                            <div class="ticket-helper">
                                <div class="number">2</div>
                            </div>
                            <div class="ticket-helper">
                                <div class="number">3</div>
                            </div>
                            <div class="ticket-helper">
                                <div class="number">.</div>
                            </div>
                            <div class="ticket-helper">
                                <div class="number">.</div>
                            </div>
                            <div class="ticket-helper">
                                <div class="number">23</div>
                            </div>
                            <div class="ticket-helper">
                                <div class="number">24</div>
                            </div>
                            <div class="ticket-helper">
                                <div class="number">25</div>
                            </div>
                        </div>
                        <!-- divider -->
                        <div class="divider display-inline-block">
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                            <div><i class="fa fa-circle"></i></div>
                        </div>


                        <!-- number of tickets -->
                        <div class="display-inline-block">Tickets:
                            <span id="ticket-number">100</span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <!-- ticket data -->
            <div class="content-holder">
                <div class="table-container">
                    <form accept-charset="utf-8" action="ticket_list/change_selected" method="post">
                        <table id="ticketlist" class="table table-striped">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="selectall"></th>
                                    <th>Ticket<span id="arrow-down" class="down-arrow"></span></th>
                                    <th>Status</th>
                                    <th>Target</th>
                                    <th>Assigned</th>
                                    <th>Subject</th>
                                    <th>From</th>
                                    <th>Account</th>
                                    <th>Time</th>
                                    <th>Date</th>
                                    <th>Date closed</th>
                                    <th>Priority</th>
                                    <th>Note</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3769">
                                    </td>
                                    <td><a href="nticket">003769</a></td>
                                    <td class="status-width">
                                        <a class="status open" href="nticket">Open</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">david</a></td>
                                    <td><a href="nticket">Payment for Logicalware Invoice # 446</a></td>
                                    <td><a href="nticket">BigBadToyStore via PayPal</a></td>
                                    <td><a href="nticket">billing@logicalware.com</a></td>
                                    <td><a href="nticket">01:27</a></td>
                                    <td><a href="nticket">04.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3769">
                                    </td>
                                    <td><a href="nticket">003769</a></td>
                                    <td class="status-width"><a class="status archived" href="nticket">Archived</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">david</a></td>
                                    <td><a href="nticket">Payment for Logicalware Invoice # 446</a></td>
                                    <td><a href="nticket">BigBadToyStore via PayPal</a></td>
                                    <td><a href="nticket">billing@logicalware.com</a></td>
                                    <td><a href="nticket">01:27</a></td>
                                    <td><a href="nticket">04.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3769">
                                    </td>
                                    <td><a href="nticket">003769</a></td>
                                    <td class="status-width"><a class="status spam" href="nticket">Spam</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">david</a></td>
                                    <td><a href="nticket">Payment for Logicalware Invoice # 446</a></td>
                                    <td><a href="nticket">BigBadToyStore via PayPal</a></td>
                                    <td><a href="nticket">billing@logicalware.com</a></td>
                                    <td><a href="nticket">01:27</a></td>
                                    <td><a href="nticket">04.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3769">
                                    </td>
                                    <td><a href="nticket">003769</a></td>
                                    <td class="status-width"><a class="status overdue" href="nticket">Overdue</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">david</a></td>
                                    <td><a href="nticket">Payment for Logicalware Invoice # 446</a></td>
                                    <td><a href="nticket">BigBadToyStore via PayPal</a></td>
                                    <td><a href="nticket">billing@logicalware.com</a></td>
                                    <td><a href="nticket">01:27</a></td>
                                    <td><a href="nticket">04.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3769">
                                    </td>
                                    <td><a href="nticket">003769</a></td>
                                    <td class="status-width"><a class="status hold" href="nticket">Hold</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">david</a></td>
                                    <td><a href="nticket">Payment for Logicalware Invoice # 446</a></td>
                                    <td><a href="nticket">BigBadToyStore via PayPal</a></td>
                                    <td><a href="nticket">billing@logicalware.com</a></td>
                                    <td><a href="nticket">01:27</a></td>
                                    <td><a href="nticket">04.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3767">
                                    </td>
                                    <td><a href="nticket">003767</a></td>
                                    <td class="status-width"><a class="status closed"  href="nticket">Closed</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">logicalware</a></td>
                                    <td><a href="nticket">Action for Children Remittance Advice</a></td>
                                    <td><a href="nticket">Lana Tavares</a></td>
                                    <td><a href="nticket">unknown@logicalware.com</a></td>
                                    <td><a href="nticket">16:46</a></td>
                                    <td><a href="nticket">03.07.13</a></td>
                                    <td><a href="nticket">03.07.13 16:46</a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3766">
                                    </td>
                                    <td><a href="nticket">003766</a></td>
                                    <td class="status-width"><a class="status closed"  href="nticket">Closed</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">igor</a></td>
                                    <td><a href="nticket">Quote</a></td>
                                    <td><a href="nticket">Michael Hutcheson</a></td>
                                    <td><a href="nticket">sales@logicalware.com</a></td>
                                    <td><a href="nticket">13:55</a></td>
                                    <td><a href="nticket">03.07.13</a></td>
                                    <td><a href="nticket">03.07.13 14:56</a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr class="">
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3765">
                                    </td>
                                    <td><a href="nticket">003765</a></td>
                                    <td class="status-width"><a class="status open" href="nticket">Open</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">david</a></td>
                                    <td><a href="nticket">Remittance</a></td>
                                    <td><a href="nticket">Melanie Foster</a></td>
                                    <td><a href="nticket">billing@logicalware.com</a></td>
                                    <td><a href="nticket">13:44</a></td>
                                    <td><a href="nticket">03.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3764">
                                    </td>
                                    <td><a href="nticket">003764</a></td>
                                    <td class="status-width"><a class="status closed"  href="nticket">Closed</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">6:12</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">timets</a></td>
                                    <td><a href="nticket">@deleteuser: request for delete user</a></td>
                                    <td><a href="nticket">customerservicesteammanagers1@purecollection.com</a></td>
                                    <td><a href="nticket">support@logicalware.com</a></td>
                                    <td><a href="nticket">13:27</a></td>
                                    <td><a href="nticket">03.07.13</a></td>
                                    <td><a href="nticket">03.07.13 13:30</a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr class="">
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3763">
                                    </td>
                                    <td><a href="nticket">003763</a></td>
                                    <td class="status-width"><a class="status open" href="nticket">Open</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">david</a></td>
                                    <td><a href="nticket">RE: Logicalware Ltd - Invoice #542 for services ...</a></td>
                                    <td><a href="nticket">Robert Sawicki</a></td>
                                    <td><a href="nticket">billing@logicalware.com</a></td>
                                    <td><a href="nticket">12:13</a></td>
                                    <td><a href="nticket">03.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3762">
                                    </td>
                                    <td><a href="nticket">003762</a></td>
                                    <td class="status-width"><a class="status open" href="nticket">Open</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">david</a></td>
                                    <td><a href="nticket">Invoices (Brightside Group PLC)</a></td>
                                    <td><a href="nticket">Hayley Mclay</a></td>
                                    <td><a href="nticket">billing@logicalware.com</a></td>
                                    <td><a href="nticket">12:08</a></td>
                                    <td><a href="nticket">03.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr class="">
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3759">
                                    </td>
                                    <td><a href="nticket">003759</a></td>
                                    <td class="status-width"><a class="status closed"  href="nticket">Closed</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">2:45</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">timets</a></td>
                                    <td><a href="nticket">@deleteuser: request for delete user</a></td>
                                    <td><a href="nticket">icarlotta@gardensalive.com</a></td>
                                    <td><a href="nticket">support@logicalware.com</a></td>
                                    <td><a href="nticket">18:05</a></td>
                                    <td><a href="nticket">02.07.13</a></td>
                                    <td><a href="nticket">02.07.13 18:22</a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3757">
                                    </td>
                                    <td><a href="nticket">003757</a></td>
                                    <td class="status-width"><a class="status closed"  href="nticket">Closed</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">david</a></td>
                                    <td><a href="nticket">Web Design Proposal!</a></td>
                                    <td><a href="nticket">Vinay</a></td>
                                    <td><a href="nticket">enquiries@logicalware.com</a></td>
                                    <td><a href="nticket">10:09</a></td>
                                    <td><a href="nticket">02.07.13</a></td>
                                    <td><a href="nticket">02.07.13 10:49</a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr class="">
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3755">
                                    </td>
                                    <td><a href="nticket">003755</a></td>
                                    <td class="status-width"><a class="status open" href="nticket">Open</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">david</a></td>
                                    <td><a href="nticket">RE: Logicalware Ltd - Invoice #511 for services ...</a></td>
                                    <td><a href="nticket">Lisa Dreyer</a></td>
                                    <td><a href="nticket">billing@logicalware.com</a></td>
                                    <td><a href="nticket">14:08</a></td>
                                    <td><a href="nticket">01.07.13</a></td>
                                    <td><a href="nticket"><none></none></a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3754">
                                    </td>
                                    <td><a href="nticket">003754</a></td>
                                    <td class="status-width"><a class="status closed"  href="nticket">Closed</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">0:00</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">timets</a></td>
                                    <td><a href="nticket">@deleteuser: request for delete user</a></td>
                                    <td><a href="nticket">oliver.greenslade@brightsidegroup.co.uk</a></td>
                                    <td><a href="nticket">support@logicalware.com</a></td>
                                    <td><a href="nticket">12:23</a></td>
                                    <td><a href="nticket">01.07.13</a></td>
                                    <td><a href="nticket">01.07.13 12:23</a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr class="">
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3753">
                                    </td>
                                    <td><a href="nticket">003753</a></td>
                                    <td class="status-width"><a class="status closed" href="nticket">Closed</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">0:00</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">timets</a></td>
                                    <td><a href="nticket">@deleteuser: request for delete user</a></td>
                                    <td><a href="nticket">mpickup@hallmarkconsumer.co.uk</a></td>
                                    <td><a href="nticket">support@logicalware.com</a></td>
                                    <td><a href="nticket">10:16</a></td>
                                    <td><a href="nticket">01.07.13</a></td>
                                    <td><a href="nticket">01.07.13 10:17</a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3752">
                                    </td>
                                    <td><a href="nticket">003752</a></td>
                                    <td class="status-width"><a class="status closed"  href="nticket">Closed</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">timets</a></td>
                                    <td><a href="nticket">test</a></td>
                                    <td><a href="nticket">Denis Timets</a></td>
                                    <td><a href="nticket">billing@logicalware.com</a></td>
                                    <td><a href="nticket">09:49</a></td>
                                    <td><a href="nticket">01.07.13</a></td>
                                    <td><a href="nticket">01.07.13 09:50</a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr class="">
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3751">
                                    </td>
                                    <td><a href="nticket">003751</a></td>
                                    <td class="status-width"><a class="status closed"  href="nticket">Closed</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">timets</a></td>
                                    <td><a href="nticket">test 2</a></td>
                                    <td><a href="nticket">Denis Timets</a></td>
                                    <td><a href="nticket">enquiries@logicalware.com</a></td>
                                    <td><a href="nticket">09:37</a></td>
                                    <td><a href="nticket">01.07.13</a></td>
                                    <td><a href="nticket">01.07.13 09:37</a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="ticket_ids" value="3750">
                                    </td>
                                    <td><a href="nticket">003750</a></td>
                                    <td class="status-width"><a class="status closed"  href="nticket">Closed</a></td>
                                    <td>
                                        <div>
                                            <a href="nticket">N/A</a>
                                        </div>
                                    </td>
                                    <td><a href="nticket">timets</a></td>
                                    <td><a href="nticket">test</a></td>
                                    <td><a href="nticket">Denis Timets</a></td>
                                    <td><a href="nticket">sales@logicalware.com</a></td>
                                    <td><a href="nticket">09:36</a></td>
                                    <td><a href="nticket">01.07.13</a></td>
                                    <td><a href="nticket">01.07.13 09:36</a></td>
                                    <td><a href="nticket">
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                        <div class="priority-mark"></div>
                                    </a></td>
                                    <td><a href="nticket"></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php include('views/footer-tickets.php'); ?>
</div>
<?php include('views/footer.php'); ?>